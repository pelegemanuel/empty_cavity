We order spherical harmonic modes in the program in a lexicographic manner, i.e., order modes first according to l, then according to m, starting from the lower:

```math
(1, -1), (1, 0), (1, 1), (2, -2), (2, -1), (2, 0), .. , (l_{max}, l_{max})
```

For every $`l`$, $`m`$ can take all $`2l+1`$ integer values in the range $`[-l,l]`$. Therefore the index of a mode $`(l, m)`$ is given by:

```math
index = l + m + \sum_{j=1}^{l-1} (2j+1) =  l^2 + l - 1 + m
```

Therefore
```math
(l+1)^2 > index + 1 \geq l^2
```

We now conclude that:

```math
l = \lfloor \sqrt{index + 1} \rfloor \\
m = index + 1 - l - l^2
```

