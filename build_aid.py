import os
import pathlib

PROJECT_PATH = str(pathlib.Path().absolute()) + "/"

BUILD_DIRECTORY_NAME = "build"
RESULTS_DIRECTORY_NAME = "results"
ANALYZED_RESULTS_DIRECTORY_NAME = "analyzed_results"
ANALYSIS_DIRECTORY_NAME = "analysis"
PROFILER_RESULTS = "analysis.txt"

BUILD_DIRECTORY = BUILD_DIRECTORY_NAME + "/"
RESULTS_DIRECTORY = RESULTS_DIRECTORY_NAME + "/"
ANALYZED_RESULTS_DIRECTORY = ANALYZED_RESULTS_DIRECTORY_NAME + "/"
ANALYSIS_DIRECTORY = ANALYSIS_DIRECTORY_NAME + "/"

def run_job(file):
    command = "sbatch " + file
    os.system(command)

def del_file(file):
    command = "rm " + file
    os.system(command)

def forceCreateDirectory(checkPath, directoryName):
    if(not os.path.exists(checkPath  + directoryName)):
        os.mkdir(checkPath + directoryName)
    return checkPath + directoryName + "/"