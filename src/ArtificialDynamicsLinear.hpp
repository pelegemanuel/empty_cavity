#ifndef ARTIFICIAL_DYNAMICS_LINEAR_H
#define ARTIFICIAL_DYNAMICS_LINEAR_H

#include <armadillo>
#include "dynamic_state.hpp"
#include "ArtificialDynamics.hpp"
#include "aid_arma_templates.hpp"

class ArtificialDynamicsLinear: public ArtificialDynamics
{
    private:
        arma::cx_vec const modeVelocityCoeffs;
        double const radius_velocity_coef;

    public:
        ArtificialDynamicsLinear(ArtificialDynamicsLinear const &origin)
        : ArtificialDynamics(origin.get_num_of_modes()),
        modeVelocityCoeffs(origin.get_modeVelocityCoeffs()),
        radius_velocity_coef(origin.get_radius_velocity_coef()) {}

        ArtificialDynamicsLinear(arma::cx_vec const &init_modeVelocityCoeffs, double const init_radius_velocity_coef)
        : ArtificialDynamics(init_modeVelocityCoeffs.n_rows),
        modeVelocityCoeffs(init_modeVelocityCoeffs),
        radius_velocity_coef(init_radius_velocity_coef) {}

        inline arma::cx_vec pumpAndDissipateModeVelocities(SystemState const &state, double const dt) const {
            return arma::exp(dt * modeVelocityCoeffs) % state.get_mode_vels();
        }
        inline double pumpAndDissipateRadiusVelocity(double const input_R_velocity, double const dt) const {
            return std::exp(dt * radius_velocity_coef) * input_R_velocity;
        }
        
        inline void printData() const {
            std::cout << "Linear pumping / dissipation coefs: " << std::endl
                        << "R_velocity: " << radius_velocity_coef << std::endl;
            aid::printVectorOfL(modeVelocityCoeffs);
        }

        inline arma::cx_vec get_modeVelocityCoeffs() const { return modeVelocityCoeffs; }
        inline double get_radius_velocity_coef() const { return radius_velocity_coef; }   
};

#endif