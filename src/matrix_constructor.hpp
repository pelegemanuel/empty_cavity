#ifndef MATRICES_H
#define MATRICES_H

#include <armadillo>

//IGNORING INTERACTIONS OF THE (0,0) TERM

//total interaction matrix for given mode (l_base, m_base). Reduced by scaling if shouldReduce
arma::mat total_gaunt_mat(int const N_indexes, int const l_base, int const m_base, bool const shouldReduce);

//interaction mat where components with different frequencies (different l's) have been canceled out
arma::mat total_gaunt_mat_same_l_only(int const N_indexes, int const l_base, int const m_base, bool const shouldReduce);

// C1, C2 matrix for l_base,m_base,m1,m2. 
// for proper definition of C1, C2: see gaunt_spectrum.lyx 
// basic l_indices of C are saved to base_indices pointer if given as input
arma::mat get_Ci_matrix(int const l_base, int const m_base, int const m1, int const m2, int const l_max, int const l1_parity, bool const shouldReduce, std::pair<int,int> &base_indices);
arma::mat get_Ci_matrix(int const l_base, int const m_base, int const m1, int const m2, int const l_max, int const l1_parity, bool const shouldReduce);

// C matrix for the symmetric m1=m2 case. Must have even m_base.
// for more information see gaunt_spectrum.lyx
arma::mat get_symmetric_C_mat(int const l_base, int const m_base, int const l_max, bool const shouldReduce);

// Returns matrix for second order interations. Matrix type is set by given function.
// for more information see empty_cavity.lyx
arma::mat get_sec_order_mat(int const N_indexes, int const l_base, int const m_base, bool const shouldReduce, double (*f)(int,int,int,int,int,int,bool));

//same as previous, uses known sizes knowing that f is proportional to the Gaunt matrices of (-m_base, m1, m2)
arma::mat get_sec_order_mat_smart(int const N_indexes, int const  l_base, int const m_base, bool const shouldReduce, double (*f)(int,int,int,int,int,int,bool));

#endif