#ifndef STD_VECTOR_AID_H
#define STD_VECTOR_AID_H

#include <vector>
#include <string>
#include "aid.hpp"

namespace stdVecAid
{    
    void print_vector(std::vector<double> const &vec);

    void vector_to_file(std::vector<double> const &vec, std::string const &filename);

    void clean_and_sort(std::vector<double> &vec, double const eps = aid::EPS);
}

#endif