#include <armadillo>
#include <complex>
#include <cassert>

namespace seriesappx
{
    //recieves two series approximations and returns the series approximation of their multiplication.
    //Can get two different parameter types as long as Data2 can be interpreted as Data1.
    template <typename Data1, typename Data2>
    arma::Col<Data1> multiply_series(const arma::Col<Data1> &series1, const arma::Col<Data2> &series2){
        assert(series1.n_rows == series2.n_rows); //technically not that criticall, though not intended
        int multiplied_series_length = std::min(series1.n_rows, series2.n_rows);
        arma::Col<Data1> multiplied_series(multiplied_series_length, arma::fill::zeros);
        for(int idx_mul = 0; idx_mul < multiplied_series_length; ++idx_mul){
            for(int idx1 = 0; idx1 <= idx_mul; ++idx1){
                int idx2 = idx_mul - idx1;
                multiplied_series(idx_mul) += series1(idx1) * series2(idx2);
            }
        }
        return multiplied_series;
    }

    //recieves a series and returns its positive integer power (of the same order)
    //does not change original coef series and returns a copy
    template <typename Data>
    arma::Col<Data> series_power(const arma::Col<Data> &series, const int pow){
        assert(pow > 0);
        int num_of_orders = series.n_rows;
        if (pow == 1){
            arma::vec unity_series(num_of_orders, arma::fill::zeros);
            unity_series(0) = 1.0;
            return multiply_series(series, unity_series);
        } else if (pow % 2 == 0){
            arma::Col<Data> half_power_series = series_power(series, pow / 2);
            return multiply_series(half_power_series, half_power_series);
        } else {
            arma::Col<Data> minus_one_series = series_power(series, pow - 1);
            return multiply_series(series, minus_one_series);
        }
    }

    //receives a matrix containing a series and returns the equivalent matrix of the same size
    template<typename Data>
    arma::mat multiplication_matrix(const arma::Col<Data> &multiplying_series){
        int len = multiplying_series.n_rows;
        arma::Mat<Data> mul_mat(len, len, arma::fill::zeros);
        for(int idx1 = 0; idx1 < len; ++idx1){
            for(int idx2 = 0; idx2 <=idx1; ++idx2){
                mul_mat(idx1, idx2) = multiplying_series(idx1 - idx2); //output order in first index, input order in the second
            }
        }
        return mul_mat;
    }
}