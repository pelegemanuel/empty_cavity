#include "dynamic_state.hpp"

void SystemState::update_R_acceleration(double const artificial_acceleration){
    if(shouldKeepRadiusConstant){
        R_acceleration = 0;
        return;
    }

    double const main_term = -1.5 * std::pow(R_velocity, 2.0) - 2.0 * (std::pow(R, -1.0) * physicalAttributes.get_alpha()) - SystemState::get_Xi_with_perturbations();
    double const pert_vv = arma::dot(radiusCoefs.get_vv(), arma::pow(arma::abs(mode_vels), 2.0));
    double const pert_va = SystemState::get_Rvel_over_R() * std::real(arma::cdot(mode_vels, radiusCoefs.get_va() % mode_amps)); // mode_vels is conjugated in cdot
    arma::vec const top_aa_coef = std::pow(SystemState::get_Rvel_over_R(), 2.0) * radiusCoefs.get_aa_velocity() + physicalAttributes.get_alpha() * std::pow(R, -3.0) * radiusCoefs.get_aa_surf();
    double const pert_aa_top = arma::dot(top_aa_coef, arma::pow(arma::abs(mode_amps), 2.0));
    
    double pert_term = pert_vv + pert_va + pert_aa_top;
    double bottom = 1.0 - std::pow(R, -2.0) * arma::dot(radiusCoefs.get_aa_accel(), arma::pow(arma::abs(mode_amps), 2.0));
    if(bottom < MIN_BOTTOM_VALUE_FOR_RACCEL){
        pert_term += (1.0 - bottom) * R * R_acceleration;
        bottom = 1.0;
    }

    R_acceleration = std::pow(R, -1.0) * (main_term + pert_term) / bottom;
    R_acceleration += artificial_acceleration;
}

void SystemState::set_prerun_physical_quantities(double const artificial_acceleration){
    update_R_acceleration(artificial_acceleration);
    nonlinPertTerms.set_initial_extrapolation_points(*this);
}

void SystemState::update_all_physical_aid_quantities(double const artificial_acceleration, bool const shouldUpdateNonlinTerms){
    update_R_acceleration(artificial_acceleration);
    if(shouldUpdateNonlinTerms) { nonlinPertTerms.update_rightHandSide(*this); }
}

void SystemState::perform_dissipationAndPumping(ArtificialDynamics const &artificialDynamicsRunner, double const dt){
    assert(artificialDynamicsRunner.get_num_of_modes() == num_of_modes);
    mode_vels = artificialDynamicsRunner.pumpAndDissipateModeVelocities(*this, dt);
    R_velocity = artificialDynamicsRunner.pumpAndDissipateRadiusVelocity(R_velocity, dt);
}

//lowers all mode which are dissipated mid-dynamics
void SystemState::perform_precomputation_dissipation(ArtificialDynamics const &artificialDissipator, double const dt){
    arma::cx_vec const mode_vels_possible_new = artificialDissipator.pumpAndDissipateModeVelocities(*this, dt);
    double const R_velocity_possible_new = artificialDissipator.pumpAndDissipateRadiusVelocity(R_velocity, dt);

    R_velocity = std::min(R_velocity, R_velocity_possible_new);
    for(int mode_index = 0; mode_index < num_of_modes; ++mode_index){
        double const disp_ratio = std::abs(mode_vels_possible_new(mode_index)) / (std::abs(mode_vels(mode_index)) + aid::EPS);
        if(disp_ratio < 1.0){
            mode_vels(mode_index) = mode_vels_possible_new(mode_index);
            mode_amps(mode_index) *= disp_ratio;
        }
    }
    set_prerun_physical_quantities();
}