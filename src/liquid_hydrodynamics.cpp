#include "liquid_hydrodynamics.hpp"
#include "aid_lm_indexing.hpp"

namespace hydro
{
    arma::vec get_liquid_local_velocity_direct(SystemState const &state, Point const &point){
        if(hydro::is_point_in_bubble(state, point)){ return {0.0, 0.0, 0.0}; }

        double const radii_ratio = point.get_r() / state.get_R();
        arma::cx_vec b_coefs = state.get_b_coefs_first_order();

        arma::cx_vec perturbation_part(3, arma::fill::zeros);
        for(int mode_index = 0; mode_index < state.get_num_of_modes(); ++mode_index){
            auto const [l, m] = aid::lm_from_index(mode_index);
            perturbation_part += hydro::velocity_radial_mode_scaling(radii_ratio, l) 
                                    * b_coefs(mode_index) 
                                    * hydro::liquid_velocity_pert_term(l, m, point);
        }

        if(!aid::is_eff_real(perturbation_part)) { std::cout << "Complex values in liquid velocity: " << std::endl; }
        return state.get_R_velocity() * hydro::velocity_radial_mode_scaling(radii_ratio, 0) * point.get_r_hat() + arma::real(perturbation_part);
    }
        
    double hydro_potential(SystemState const &state, Point const &point){
        if(hydro::is_point_in_bubble(state, point)){ return 0.0; }

        double const radii_ratio = point.get_r() / state.get_R();
        arma::cx_vec b_coefs = state.get_b_coefs_first_order();

        arma::cx_double perturbation_part = 0.0;
        for(int mode_index = 0; mode_index < state.get_num_of_modes(); ++mode_index){
            auto const [l, m] = aid::lm_from_index(mode_index);
            perturbation_part += hydro::hydro_radial_mode_scaling(radii_ratio, l) 
                                    * b_coefs(mode_index) 
                                    * hydro::hydro_potential_pert_term(l, m, point);
        }

        if(!aid::is_eff_real(perturbation_part)) { std::cout << "Complex values in hydrodynamic potential: " << std::endl; }
        return state.get_R() * (state.get_R_velocity() * hydro::hydro_radial_mode_scaling(radii_ratio, 0) + std::real(perturbation_part));
    }
}
        
