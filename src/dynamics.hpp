#ifndef DYNAMICS_H
#define DYNAMICS_H

#include "Output.hpp"
#include "ArtificialDynamicsMultiple.hpp"
#include "dynamic_state.hpp"
#include "time_manager.hpp"
#include "ParticleDynamics.hpp"

void run_dynamics(SystemState &state,  
                    TimeManager &timeManager, 
                    Output &output,
                    ParticleDynamics &particleDynamicsRunner,
                    ArtificialDynamics const &artificialDynamicsRunner = ArtificialDynamicsMultiple());

inline void run_dynamics(SystemState &state,  
                    TimeManager &timeManager, 
                    Output &output,
                    ArtificialDynamics const &artificialDynamicsRunner = ArtificialDynamicsMultiple()){
                        ParticleDynamics emptyParticleDynamics;
                        run_dynamics(state, timeManager, output, emptyParticleDynamics, artificialDynamicsRunner);
                    }

#endif