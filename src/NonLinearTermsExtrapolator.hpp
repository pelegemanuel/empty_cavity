#ifndef NON_LINEAR_TERMS_EXTRAPOLATOR_H
#define NON_LINEAR_TERMS_EXTRAPOLATOR_H

#include "SecondOrderTermsCalculator.hpp"
class SystemState;

class NonLinearTermsExtrapolator {
    private:
        int const num_of_modes;
        bool shouldConsiderRadiusVelocity;

        arma::cx_mat rightHandSideSaved;
        arma::vec rhsSaveTimes;

        SecondOrderTermsCalculator const secondOrderCalc;

    public:
        static int constexpr RHS_INTERPOLATION_POINT_NUM = 2;
        NonLinearTermsExtrapolator(int const init_num_of_modes);

        inline void set_shouldConsiderRadiusVelocity(bool const input){ shouldConsiderRadiusVelocity = input; }
        inline bool get_shouldConsiderRadiusVelocity() const { return shouldConsiderRadiusVelocity; }

        void set_initial_extrapolation_points(SystemState const &state);
        void update_rightHandSide(SystemState const &state);
        arma::cx_vec getRightHandSideByExtrapolation(double const current_time) const;
};

#endif