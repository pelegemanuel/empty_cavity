#ifndef STEPS_TEMPLATE_AID
#define STEPS_TEMPLATE_AID

template<typename realData> 
inline realData getDivisorForImplicitScheme(double const dt, realData const &A, double const Rvel_over_R){
    return 1.0 + 1.5 * dt * Rvel_over_R - 0.25 * dt * dt * A;
}

template<typename realData>
inline realData getVelocityMultiplierForImplicitScheme(realData const & divisorForImplicitScheme){
    return 2.0 - divisorForImplicitScheme;
}

#endif