#ifndef POINT_H
#define POINT_H

#include "aid_spherical.hpp"
#include "dynamic_state.hpp"

class SystemState;

class Point{
    private:
        arma::vec coordinates;

    public:
        Point(double const x, double const y, double const z) : coordinates{x, y, z} {}
        Point(Point const &origin) : coordinates(origin.get_coordinates()) {}
        Point(arma::vec const &init_coor) : coordinates{init_coor(aidsph::X_INDEX), init_coor(aidsph::Y_INDEX), init_coor(aidsph::Z_INDEX)} {}
        Point() : coordinates(3, arma::fill::zeros) {}
        Point(double const r) : coordinates(r * aid::get_random_coordinates_on_unit_sphere()) {}

        inline double get_x() const { return coordinates(aidsph::X_INDEX); }
        inline double get_y() const { return coordinates(aidsph::Y_INDEX); }
        inline double get_z() const { return coordinates(aidsph::Z_INDEX); }
        inline arma::vec get_coordinates() const { return coordinates; }
        inline double get_r() const { return arma::norm(coordinates); }
        inline double get_theta() const {return std::acos(get_z() / get_r()); }
        double get_phi() const { return std::atan(get_y() / get_x()) + 0.5 * double(1 - aid::sign(get_x())) * aid::PI; }
        inline arma::vec get_r_hat() const { return aidsph::r_hat(get_theta(), get_phi()); }
        inline arma::vec get_theta_hat() const { return aidsph::theta_hat(get_theta(), get_phi()); }
        inline arma::vec get_phi_hat() const { return aidsph::phi_hat(get_phi()); }

        inline arma::cx_double spherical_harmonic(int const l, int const m) const { return aidsph::spherical_harmonic(l, m, get_theta(), get_phi()); }
        inline arma::cx_vec spherical_harmonic_angular_gradient(int const l, int const m) const { return aidsph::spherical_harmonic_angular_gradient(l, m, get_theta(), get_phi());}

        inline double get_distance(Point const &other) const { return arma::norm(get_coordinates() - other.get_coordinates()); }
        inline Point nudged_a_bit(double const dx, double const dy, double const dz) const {return Point(get_x() + dx, get_y() + dy, get_z() + dz); }
        inline void move(arma::vec const dx_vec) { coordinates += dx_vec; }
};

#endif