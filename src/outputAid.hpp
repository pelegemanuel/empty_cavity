#ifndef OUTPUT_AID_H
#define OUTPUT_AID_H

#include <armadillo>
#include <fstream>
#include <sstream>
#include <filesystem>
#include "dynamic_state.hpp"

namespace outputAid
{

    static constexpr int floatPrecision = 10;

    static std::string complex_to_string(arma::cx_double cx_num){
        std::stringstream ss;
        if(std::imag(cx_num) < 0.0){
            ss << std::fixed << std::setprecision(floatPrecision) << std::real(cx_num) << std::imag(cx_num) << "i";
        } else { //plus sign needed
            ss << std::fixed << std::setprecision(floatPrecision) << std::real(cx_num) << "+" << std::imag(cx_num) << "i";
        }
        return ss.str();
    }

    static void write_complex_line(std::ofstream &file, arma::cx_vec const &vec){
        for(int index = 0; index < vec.n_rows; ++index){
            file << complex_to_string(vec(index)) << " ";  
        }
        file << std::endl << std::flush;
    }

    static void write_real_line(std::ofstream &file, arma::vec const &vec){
        for(int index = 0; index < vec.n_rows; ++index){
            file << vec(index) << " ";  
        }
        file << std::endl << std::flush;
    }

    inline static void write_main_state_terms(std::ofstream &file, SystemState const &state){
        file << state.get_time() << " "
             << state.get_R() << " "
             << state.get_R_velocity() << " "
             << state.get_perturbation_norm() << " ";
    }

    static inline std::string get_save_path(std::string const &folder_name){
        return std::filesystem::current_path().parent_path().parent_path().u8string() + "/results/" + folder_name + "/";
    }
}

#endif