
#include "wigner_coefs.hpp"
#include "aid.hpp"
#include "aid_lm_indexing.hpp"

namespace coefs{

    double gaunt_coef(const int l1, const int l2, const int l3, const int m1, const int m2, const int m3, const bool shouldReduce){
        if (shouldReduce){
            return coefs::reduced_gaunt_coef(l1, l2, l3, m1, m2, m3);
        } else {
            return coefs::gaunt_coef(l1, l2, l3, m1, m2, m3);
        }
    }

    double K_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce){
        double const coef = 3.0 + double(l1 * (l + 1)) - double(l1 - 3) * g_coef(l, l1, l2);
        return aid::get_parity(m) * coefs::gaunt_coef(l, l1, l2, -m, m1, m2, shouldReduce) * coef;
    }

    double C_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce){
        double const coef = double((l + 1) * l1 * (3 - l1 * l1)) + g_coef(l, l1, l2) * q_coef(l1);
        return aid::get_parity(m) * coefs::gaunt_coef(l, l1, l2, -m, m1, m2, shouldReduce) * coef;
    }

    double X_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce){
        double const coef = 2.0 * g_coef(l, l1, l2) - double(l + 4) + 2.0 * coefs::angular_velocity_coef_renormed(l, l1, l2);
        return aid::get_parity(m) * coefs::gaunt_coef(l, l1, l2, -m, m1, m2, shouldReduce) * coef;
    }

    double Z_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce){
        double const coef = g_coef(l, l1, l2) - 2.0 * g_coef(l, l2, l1) + 5.0 - double(l) + 2.0 * angular_velocity_coef_renormed(l, l1, l2);
        return aid::get_parity(m) * coefs::gaunt_coef(l, l1, l2, -m, m1, m2, shouldReduce) * coef;
    }

    double D_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce){
        double const coef = 0.5 * double(l + 1) + 0.5 * angular_velocity_coef_renormed(l, l1, l2) - g_coef(l, l1, l2);
        return aid::get_parity(m) * coefs::gaunt_coef(l, l1, l2, -m, m1, m2, shouldReduce) * coef;
    }

    //get the overlap of four spherical harmonics
    double four_spherical_harmonics_overlap(const int l1, const int l2, const int l3, const int l4, const int m1, const int m2, const int m3, const int m4){ 
        if(m1 + m2 + m3 + m4 != 0){
            return 0;
        } else if ((l1 + l2) % 2 != (l3 + l4) % 2){
            return 0;
        }
    
        double ret_sum = 0.0;
        const int k = m3 + m4;
        const int l_max = 80; //maximal mode to check
        const int l_max_eff = std::min(l_max, std::min(l1 + l2, l3 + l4));

        int l_min = std::max(std::abs(k), std::max(std::abs(l1 - l2), std::abs(l3 - l4)));
        if( l_min % 2 != (l3 + l4) % 2){
            l_min = l_min + 1;
        }

        for(int n = l_min; n <= l_max_eff; n += 2){
            ret_sum += coefs::gaunt_coef(n, l1, l2, k, m1, m2) * coefs::gaunt_coef(n, l3, l4, -k, m3, m4);
        }
        return ret_sum * aid::get_parity(k);
    }

}