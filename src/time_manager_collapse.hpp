#ifndef COLLAPSE_TIME_MANAGER
#define COLLAPSE_TIME_MANAGER

#include "time_manager.hpp"

class TimeManagerCollapse : public TimeManager 
{
    private:
        //collapse params
        double const theta_param;
        double const t_collapse_appx;
        double const t_final_appx;

        void finishPrintMessage(SystemState const &state) const;

    public:

        TimeManagerCollapse(double const init_max_amp_to_R, 
                            double const init_max_norm_to_R,
                            double const init_dt_over_typical_time,
                            double const init_max_time, 
                            int const init_max_num_steps,
                            SystemState const &initial_state);
};

#endif