#ifndef OUTPUT_SPARSE_MANAGER_H
#define OUTPUT_SPARSE_MANAGER_H

class OutputSparseManager{
    private:
        int const max_steps_between_saves;
        double const max_time_between_saves;

        int last_saved_step;
        double last_saved_time;

        inline bool step_condition(int const current_step){ return (current_step - last_saved_step >= max_steps_between_saves); }
        inline bool time_condition(double const current_time){ return (current_time - last_saved_time >= max_time_between_saves); }
        inline bool first_step_condition(int const current_step, double const current_time){ return ((current_step == 0) || (current_time == 0.0)); }
        inline bool any_condition(int const current_step, double const current_time){
            return (step_condition(current_step) || time_condition(current_time) || first_step_condition(current_step, current_time));
        }
        inline void update_last_saved_vars(int const current_step, double const current_time){
            last_saved_step = current_step;
            last_saved_time = current_time;
        }

    public:
        OutputSparseManager(int const init_max_steps_between_saves, double const init_max_time_between_saves) :
        max_steps_between_saves(init_max_steps_between_saves),
        max_time_between_saves(init_max_time_between_saves),
        last_saved_step(0),
        last_saved_time(0.0) {}

        inline bool shouldSave(int const current_step, double const current_time){
            if(any_condition(current_step, current_time)){
                update_last_saved_vars(current_step, current_time);
                return true;
            }
            return false;
        }
       
        inline int get_max_steps_between_saves() const { return max_steps_between_saves; }
        inline double get_max_time_between_saves() const { return max_time_between_saves; }
        inline int get_last_saved_step() const { return last_saved_step; }
        inline double get_last_saved_time() const { return last_saved_time; }
};

#endif