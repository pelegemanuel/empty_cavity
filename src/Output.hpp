#ifndef OUTPUT_H
#define OUTPUT_H

#include "outputAid.hpp"
#include "dynamic_state.hpp"
#include "output_filenames.hpp"

class Output{
    protected:
        int const num_of_modes;
        std::string const run_folder;
        std::string const save_path;

        static inline double l_as_l_func(int const l){ return double(l); }

    public:
        Output(int const init_num_of_modes, std::string const &init_run_folder) :
        num_of_modes(init_num_of_modes), run_folder(init_run_folder), save_path(outputAid::get_save_path(run_folder)) {}

        virtual void push_data(SystemState const &state, int const step, double const dt) = 0;
        virtual void close_all() = 0;

        inline int get_num_of_modes() const {return num_of_modes; }
        inline std::string get_run_folder() const {return run_folder; }
        inline std::string get_save_path() const {return save_path; }

        template <typename ArmaDataForm> 
        void armadillo_saver(ArmaDataForm const &data, std::string const &filename) const {
            std::ofstream lstfile;
            std::string const total_filename = save_path + filename;
            lstfile.open(total_filename);
            bool success = data.save(total_filename, arma::csv_ascii);
            lstfile.close();
            if (!success) { std::cout << "Error saving file: " + filename << std::endl;}
        }

        inline void save_l_number_vector() const { armadillo_saver(aid::vector_of_l_func(num_of_modes, &Output::l_as_l_func), filenames::l_number); }  
};

#endif