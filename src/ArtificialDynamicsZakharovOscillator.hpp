#ifndef ARTIFICIAL_DYNAMICS_ZAKHAROV_OSCILLATOR_H
#define ARTIFICIAL_DYNAMICS_ZAKHAROV_OSCILLATOR_H

#include <armadillo>
#include <random>
#include "ArtificialDynamics.hpp"
#include "dynamic_state.hpp"

class ArtificialDynamicsZakharovOscillator: public ArtificialDynamics
{
    private:
        arma::cx_vec const modeVelocityCoeffs;
        double const random_frequency_multiplier_std;

        static inline double getNormalRandomNumber(){
            arma::vec normal_random(1, arma::fill::randn);
            return normal_random(0);
        }

        inline double getRandomFrequencyMultiplier() const {
            return 1.0 + random_frequency_multiplier_std * getNormalRandomNumber();
        }
        
    public:
        ArtificialDynamicsZakharovOscillator(ArtificialDynamicsZakharovOscillator const &origin)
        : ArtificialDynamics(origin.get_num_of_modes()),
        modeVelocityCoeffs(origin.get_modeVelocityCoeffs()),
        random_frequency_multiplier_std(origin.get_random_frequency_multiplier_std())
        {
            arma::arma_rng::set_seed_random();
        }

        ArtificialDynamicsZakharovOscillator(arma::cx_vec const &init_modeVelocityCoeffs, double const init_random_frequency_multiplier_std)
        : ArtificialDynamics(init_modeVelocityCoeffs.n_rows),
        modeVelocityCoeffs(init_modeVelocityCoeffs),
        random_frequency_multiplier_std(init_random_frequency_multiplier_std)
        {
            arma::arma_rng::set_seed_random();
        }

        inline arma::cx_vec pumpAndDissipateModeVelocities(SystemState const &state, double const dt) const {
            return state.get_mode_vels() + dt * modeVelocityCoeffs % arma::exp(1.0j 
                                                                * getRandomFrequencyMultiplier() 
                                                                * state.get_time() 
                                                                * state.get_oscillationFrequencies());
}
        inline double pumpAndDissipateRadiusVelocity(double const input_R_velocity, double const dt) const { 
            return input_R_velocity; 
        }
        inline void printData() const {
            std::cout << "Random frequecncy multiplier standard deviation: " << random_frequency_multiplier_std << std::endl
                        << "Zakharov Oscillator pumping / dissipation coefs: " << std::endl;
            aid::printVectorOfL(modeVelocityCoeffs);
        }

        inline arma::cx_vec get_modeVelocityCoeffs() const { return modeVelocityCoeffs; } 
        inline double get_random_frequency_multiplier_std() const {return random_frequency_multiplier_std; }
};

#endif