#ifndef MOVING_PARTICLES_H 
#define MOVING_PARTICLES_H

#include <armadillo>
#include "dynamic_state.hpp"
#include "Particle.hpp"
#include "ParticleForce.hpp"

class ParticleDynamics{
    private:
        std::vector<Particle> particleVector;
        std::vector<double> entryTimeVector;
        ParticleForceViscosity viscosityForce;
        ParticleForceRandom randomForce;

        void remove_entering_particles(SystemState const &state);

    public:
        ParticleDynamics(int const num_of_particles, 
                        double const viscosity_coef,
                        double const random_force_std, 
                        double const initial_radius, 
                        double const initial_velocity = 0);
        ParticleDynamics() : particleVector(), entryTimeVector(), viscosityForce(0.0), randomForce(0.0) {}

        inline int get_remaining_num_of_particles() const { return particleVector.size(); }

        void step_euler(SystemState const &state, double const dt);
};

#endif