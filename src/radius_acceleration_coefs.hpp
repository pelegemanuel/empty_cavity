#ifndef ACCELERATION_H
#define ACCELERATION_H

#include <armadillo>
#include "aid.hpp"
#include "wigner_coefs.hpp"

inline double adotadot_Racc_coef(const int l){ return (0.125 / aid::PI) * double(2 * l + 1) / double(l + 1); }
inline double adota_Racc_coef(const int l){ return (0.25 / aid::PI) * double(l - 1) / double(l + 1); }
inline double aa_velocity_Racc_coef(const int l){ return (0.25 / aid::PI) * double(l - 1) / double(l + 1); }
inline double aa_surf_Racc_coef(const int l){ return (0.25 / aid::PI) * double(3 * l - l * l *l); }
inline double aa_accel_Racc_coef(const int l){ return (0.25 / aid::PI) * double(l); }

class RAccCoef{
    private:
        arma::vec const vv;
        arma::vec const va;
        arma::vec const aa_velocity;
        arma::vec const aa_surf;
        arma::vec const aa_accel;
    public:
        RAccCoef(int const num_of_modes) :
        vv(aid::vector_of_l_func(num_of_modes, &adotadot_Racc_coef)), 
        va(aid::vector_of_l_func(num_of_modes, &adota_Racc_coef)),
        aa_velocity(aid::vector_of_l_func(num_of_modes, &aa_velocity_Racc_coef)),
        aa_surf(aid::vector_of_l_func(num_of_modes, &aa_surf_Racc_coef)),
        aa_accel(aid::vector_of_l_func(num_of_modes, &aa_accel_Racc_coef)){}

        inline arma::vec get_vv() const { return vv; }
        inline arma::vec get_va() const { return va; }
        inline arma::vec get_aa_velocity() const { return aa_velocity; }
        inline arma::vec get_aa_surf() const { return aa_surf; }
        inline arma::vec get_aa_accel() const { return aa_accel; }

};

#endif
