#include "std_vector_aid.hpp"
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cassert>

namespace stdVecAid
{
    void print_vector(std::vector<double> const &vec){
        for (auto const &v : vec){
            std::cout << v << " ";
        }
    }

    void vector_to_file(std::vector<double> const &vec, std::string const &filename){
        std::ofstream lstfile;
        std::filesystem::path const cm_path = std::filesystem::current_path();
        std::filesystem::path const project_path = cm_path.parent_path().parent_path();
        std::string const total_filename = project_path.u8string() + "/results/" + filename;
	    lstfile.open(total_filename);
        for (auto const &v : vec){
            lstfile << v << std::endl;
        }
	    lstfile.close();
    }

    void clean_and_sort(std::vector<double> &vec, double const eps){
        assert(eps > 0);
        vec.erase(std::remove_if(vec.begin(), vec.end(), [](const double& x){return aid::is_eff_zero(x);}), vec.end());
        std::sort(vec.begin(), vec.end());
    }
}