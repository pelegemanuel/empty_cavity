#ifndef DYN_STATE_H
#define DYN_STATE_H

#include <armadillo>
#include "aid_spherical.hpp"
#include "FirstOrderCoefficients.hpp"
#include "NonLinearTermsExtrapolator.hpp"
#include "radius_acceleration_coefs.hpp"
#include "PhysicalAttributes.hpp"
#include "initial_conditions_data.hpp"
#include "ArtificialDynamics.hpp"

class ArtificialDynamics;
class SystemState{
    private:
        int const num_of_modes;
        PhysicalAttributes const physicalAttributes;
        FirstOrderCoefficients const firstOrderCoeffs;
        RAccCoef const radiusCoefs;
        NonLinearTermsExtrapolator nonlinPertTerms;

        //dynamic vars
        double time, R, R_velocity;
        arma::cx_vec mode_amps, mode_vels;
        double R_acceleration;

        //options
        bool shouldKeepRadiusConstant;

        //aid
        arma::vec const aid_zeros;

        void set_prerun_physical_quantities(double const artificial_acceleration = 0); //Numerical Recipes - aid
        void step_half_implicit_inner_recursive(double const dt, int const recursion_depth, double const artificial_acceleration = 0); // Numerical Recipes - Steps

    public:
        static double constexpr MIN_DIVISION_VAL_FOR_IMPLICIT_SCHEME = 0.7;
        static double constexpr MIN_BOTTOM_VALUE_FOR_RACCEL = 0.05;
        static int constexpr MAX_STEP_RECURSION_DEPTH = 3;

        // Misc //
        SystemState(int const init_num_of_modes,
                    PhysicalAttributes const &initPhysicalAttributes,
                    InitialConditionsData const &initialConditionsData);

        void setRadiusConstant();
        void setRadiusMoving();
        inline void set_shouldConsiderRadiusVelocityInSecondOrder(bool const input) { nonlinPertTerms.set_shouldConsiderRadiusVelocity(input); }

        inline bool shouldIgnoreRadiusVelocity() const { 
            return (shouldKeepRadiusConstant || !nonlinPertTerms.get_shouldConsiderRadiusVelocity()); 
        }

        inline bool get_shouldKeepRadiusConstant() const {return shouldKeepRadiusConstant; }
        inline int get_num_of_modes() const { return num_of_modes; }
        inline arma::vec get_aid_zeros() const {return aid_zeros; }

        inline double get_time() const { return time; }
        inline double get_R() const { return R; }
        inline double get_R_velocity() const { return R_velocity; }
        inline double get_R_acceleration() const {return R_acceleration; }
        inline arma::cx_vec get_mode_amps() const { return mode_amps; }
        inline arma::cx_vec get_mode_vels() const { return mode_vels; }

        inline arma::cx_double get_mode_amps_at_index(int const index) const { return mode_amps.at(index); }
        inline arma::cx_double get_mode_vels_at_index(int const index) const { return mode_vels.at(index); }

        // Numerical Recipes - Aid //
        void update_R_acceleration(double const artificial_acceleration = 0);
        void update_all_physical_aid_quantities(double const artificial_acceleration, bool const shouldUpdateNonlinTerms);

        // Numerical Recipes - Steps // 
        void step_euler(double const dt);
        inline void step_half_implicit(double const dt, double const artificial_acceleration = 0){ SystemState::step_half_implicit_inner_recursive(dt, 0, artificial_acceleration); }

        // Numerical Recipes - Artificial //
        void perform_dissipationAndPumping(ArtificialDynamics const &ArtificialDynamicsRunner, double const dt);
        void perform_precomputation_dissipation(ArtificialDynamics const &artificialDissipator, double const dt); //lowers all mode which are dissipated mid-dynamics

        // Physical Aid //
        inline double get_Rvel_over_R() const { return R_velocity / (R + aid::EPS); }
        inline arma::cx_double get_avg_rhs() const { return arma::mean(nonlinPertTerms.getRightHandSideByExtrapolation(time)); }

        inline double get_nonlin_ratio() const { 
            return arma::norm(nonlinPertTerms.getRightHandSideByExtrapolation(time)) / arma::norm(get_first_order_mode_acceleration()); 
        }
        
        inline arma::vec get_A() const { return firstOrderCoeffs.get_A(R_acceleration, R); }
        inline arma::vec get_oscillationFrequencies() const { return arma::sqrt(arma::max(-get_A(), aid_zeros)); }
        inline double get_last_A() const { return firstOrderCoeffs.get_A_at_index(R_acceleration, R, num_of_modes - 1); }
        inline double get_max_A() const { return firstOrderCoeffs.get_max_A(R_acceleration, R); }
        inline arma::cx_vec get_first_order_mode_acceleration() const {return mode_amps % get_A() - 3.0 * get_Rvel_over_R() * mode_vels; }
        inline arma::cx_vec get_mode_acceleration() const { return get_first_order_mode_acceleration() + nonlinPertTerms.getRightHandSideByExtrapolation(get_time()); }
        inline arma::cx_vec get_b_coefs_first_order() const { return mode_vels + 2.0 * get_Rvel_over_R() * mode_amps; }

        // Physical Evaluations //
        inline double get_alpha() const { return physicalAttributes.get_alpha(); }
        inline double get_Xi_with_perturbations() const { return physicalAttributes.get_Xi_from_volume(get_volume_with_perturbations()); }
        inline double get_perturbation_norm() const { return arma::norm(mode_amps); }
        inline double get_volume_with_perturbations() const { return aid::sphere_volume(R) + R * std::pow(get_perturbation_norm(), 2.0); }

        inline double get_local_radius(double const theta, double const phi) const {
            return R + std::real(aidsph::evaluate_vector_in_physical_space(mode_amps, theta, phi));
        }        
};

#endif