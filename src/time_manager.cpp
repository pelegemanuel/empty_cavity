#include "time_manager.hpp"
#include "aid_lm_indexing.hpp"

TimeManager::TimeManager(int const l_max,
                        double const init_max_amp_to_R,
                        double const init_max_norm_to_R,
                        double const init_dt_over_typical_time,
                        double const init_max_time,
                        int const init_max_num_steps) :
timescaleCalc(l_max),
max_amp_to_R(init_max_amp_to_R),
max_norm_to_R(init_max_norm_to_R),
dt_over_typical_time(init_dt_over_typical_time),
max_time(init_max_time),
max_num_of_steps(init_max_num_steps),
step(0){
    std::cout << "TimeManager object created" << std::endl;
}

bool TimeManager::shouldContinue(SystemState const &state, double const dt) const {
    double const max_mode_amps = arma::max(arma::abs(state.get_mode_amps()));
    bool const amp_condition = (max_mode_amps / state.get_R() < max_amp_to_R);
    bool const norm_condition = (state.get_perturbation_norm() / state.get_R() < max_norm_to_R);
    bool const step_condition = (step < max_num_of_steps);
    bool const time_condition = (state.get_time() < max_time);
    bool const positive_R_condition (state.get_R() + dt * state.get_R_velocity() > 0.0);

    if(amp_condition && norm_condition && step_condition && time_condition && positive_R_condition){
        return true;
    }

    if(!amp_condition){
        std::cout << "Run finished - premature growth of a/R" << std::endl;
        int const max_value_index = arma::abs(state.get_mode_amps()).index_max();
        auto const[l_max_value, m_max_value] = aid::lm_from_index(max_value_index);
        std::cout << "Perturbation too large at: l=" << l_max_value << " m=" << m_max_value << std::endl;
        std::cout << "Perturbation: " << max_mode_amps << std::endl;
    }

    if(!norm_condition){
        std::cout << "Run finished - perturbation norm too large" << std::endl;
    }

    if(!step_condition){
        std::cout << "Run finished - num of steps exceeded" << std::endl; 
    }

    if(!time_condition){
        std::cout << "Run finished - max time reached" << std::endl;
    }

    if(!positive_R_condition){
        std::cout << "Run finished - got too close to collapse" << std::endl;
    }

    finishPrintMessage(state);
    return false;
}

void TimeManager::printTimeData(SystemState const &state) const {
    if((step % STEPS_BETWEEN_PRINTS == 0) && (step > 0)){
        std::cout << "step: " << step << std::endl;
        std::cout << "t: " << state.get_time() 
                  << " | R: " << state.get_R() 
                  << " | Volume: " << state.get_volume_with_perturbations()
                  << " | R_vel: " << state.get_R_velocity()
                  << " | accel: " << state.get_R_acceleration()
                  << std::endl;

        std::cout<< "dt: " << get_dt(state) 
                 << " | non-linear ratio: " << state.get_nonlin_ratio()
                 << " | <rhs>: " << state.get_avg_rhs()
                 << " | norm: " << state.get_perturbation_norm()
                 << std::endl;
    }
}