#include <cassert>
#include "initial_conditions.hpp"
#include "aid_lm_indexing.hpp"

namespace initcond{

    std::pair<arma::cx_vec,arma::cx_vec> get_initial_perturbations(int const key, SystemState const &state){
        switch(key){
            case SURFACE_OSCILLATIONS_NORMAL: { 
                std::cout << "initial conditions: Surface Oscillations" << std::endl;
                return surfaceWavesInitialConditions(state);
                break;
            }
            default: {
                std::cout << "Index unknown, defaulting to surface oscillations" << std::endl;
                return surfaceWavesInitialConditions(state);
                break;
            }
        }
    }

    //see initial conditions file
    std::pair<arma::cx_vec,arma::cx_vec> surfaceWavesInitialConditions(SystemState const &state){
        arma::cx_vec const modeAmpsPlus = (state.get_R() / NUM_OF_STANDARD_DEV_LIMITER)
                                    * aid::normal_random_vector_with_limited_std<arma::cx_vec>(state.get_num_of_modes(), NUM_OF_STANDARD_DEV_LIMITER);
        arma::cx_vec const modeAmpsMinus = get_modeAmpsMinus_from_modeAmpsPlus(modeAmpsPlus);
        arma::cx_vec const mode_vels = 1.0j * state.get_oscillationFrequencies() % (modeAmpsPlus - modeAmpsMinus);
        arma::cx_vec const mode_amps = modeAmpsPlus + modeAmpsMinus;
        return std::make_pair(mode_amps, mode_vels);
    }

    arma::cx_vec get_modeAmpsMinus_from_modeAmpsPlus(arma::cx_vec const &modeAmpsPlus){
        arma::cx_vec modeAmpsMinus(modeAmpsPlus.n_rows, arma::fill::zeros);
        for(int mode_index = 0; mode_index < modeAmpsPlus.n_rows; ++mode_index){
            modeAmpsMinus(mode_index) = double(aid::get_parity(aid::m_from_index(mode_index))) 
                                        * std::conj(modeAmpsPlus(aid::conjugate_index(mode_index)));
        }
        return modeAmpsMinus;
    }
}

