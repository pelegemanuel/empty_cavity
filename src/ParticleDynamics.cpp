#include "ParticleDynamics.hpp"
#include "aid.hpp"
#include "Point.hpp"
#include "liquid_hydrodynamics.hpp"

ParticleDynamics::ParticleDynamics(int const num_of_particles, 
                        double const viscosity_coef,
                        double const random_force_std, 
                        double const initial_radius, 
                        double const initial_velocity) : 
particleVector(num_of_particles), 
entryTimeVector(),
viscosityForce(viscosity_coef),
randomForce(random_force_std) 
{
    #pragma omp parallel for
    for(int particle_index = 0; particle_index < num_of_particles; ++particle_index){
        particleVector[particle_index] = Particle(initial_radius, initial_velocity);
    }
}

void ParticleDynamics::remove_entering_particles(SystemState const &state){
    std::vector<int> particle_indices_to_delete;

    for(int particle_index = 0; particle_index < get_remaining_num_of_particles(); ++particle_index){
        if(!hydro::is_point_in_bubble(state, particleVector[particle_index].get_location())){ continue; }
        entryTimeVector.push_back(state.get_time());
        particle_indices_to_delete.push_back(particle_index);
        std::cout << "Particle entered at: " << state.get_time() << std::endl;
    }

    for(int particle_to_delete_index : particle_indices_to_delete){
        particleVector.erase(particleVector.begin() + particle_to_delete_index);
    }
}

void ParticleDynamics::step_euler(SystemState const &state, double const dt){
    #pragma omp parallel for
    for(int particle_index = 0; particle_index < get_remaining_num_of_particles(); ++particle_index){
        arma::vec const acceleration = randomForce.get_acceleration(particleVector[particle_index], state)
                                        + viscosityForce.get_acceleration(particleVector[particle_index], state);
        particleVector[particle_index].step_euler(dt, acceleration);
    }
    remove_entering_particles(state);
}
