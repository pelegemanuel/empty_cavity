#ifndef LIQUID_HYDRODYNAMICS_H
#define LIQUID_HYDRODYNAMICS_H

#include "dynamic_state.hpp"
#include "point_function_gradient.hpp"

namespace hydro
{
    inline bool is_point_in_bubble(SystemState const &state, Point const &point){
        return (point.get_r() < state.get_local_radius(point.get_theta(), point.get_phi())); 
    }

    inline arma::cx_vec liquid_velocity_pert_term(int const l, int const m, Point const &point){
        return point.spherical_harmonic(l, m) * point.get_r_hat() - point.spherical_harmonic_angular_gradient(l, m) / double(l + 1);
    }

    inline arma::cx_double hydro_potential_pert_term(int const l, int const m, Point const &point){
        return point.spherical_harmonic(l, m) / double(l + 1);
    }

    template<int BASE_SCALE>
    inline double radial_mode_scaling(double const radii_ratio, int const l){ return std::pow(radii_ratio, -(l + BASE_SCALE)); }
    inline double velocity_radial_mode_scaling(double const radii_ratio, int const l){ return hydro::radial_mode_scaling<2>(radii_ratio, l); }
    inline double hydro_radial_mode_scaling(double const radii_ratio, int const l){ return hydro::radial_mode_scaling<1>(radii_ratio, l); }

    double hydro_potential(SystemState const &state, Point const &point);

    arma::vec get_liquid_local_velocity_direct(SystemState const &state, Point const &point);

    inline arma::vec get_liquid_local_velocity_grad(SystemState const &state, Point const &point){
        return -class_outer_function_gradient(point, &hydro::hydro_potential, state); 
    }
}

#endif