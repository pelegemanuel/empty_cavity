#include "series_appx_plesset.hpp"
#include "series_appx_arma_template.cpp"
#include "aid.hpp"
#include "aid_lm_indexing.hpp"

namespace seriesappx
{
    //return perturbation series for modes with l=1
    arma::cx_vec a_1_series_first_order(const int max_order, const arma::cx_double a0, const arma::cx_double adot0, const double t0){
        assert(max_order >= 0);
        assert(t0 > 0);
        arma::cx_vec coef_series = 5.0 * t0 * adot0 * one_minus_t_series(max_order, -0.2);
        coef_series(0) += a0 - 5.0 * t0 * adot0;
        return coef_series;
    }

    //return perturbation series for modes with l other than 1
    arma::cx_vec a_non1_series_first_order(const int max_order, const arma::cx_double a0, const arma::cx_double adot0, const double t0, const int l){
        assert(max_order >= 0);
        assert(t0 > 0);
        assert(l > 0);
        const double wl = aid::mode_frequency(l);
        arma::vec in_trigonometric_ln_series = 0.4 * wl * ln_1_minus_x(max_order);
        arma::vec sin_series = sin_of_series(in_trigonometric_ln_series);
        arma::vec cos_series = cos_of_series(in_trigonometric_ln_series);
        arma::vec R_025_series = R_over_R0_series(max_order, -0.25);

        arma::cx_double sin_coef = (0.25 * a0 - 2.5 * t0 * adot0) / wl;
        arma::cx_double cos_coef = a0;

        arma::cx_vec sin_part = arma::cx_vec(sin_coef.real() * sin_series, sin_coef.imag() * sin_series);
        arma::cx_vec cos_part = arma::cx_vec(cos_coef.real() * cos_series, cos_coef.imag() * cos_series);
        arma::cx_vec trig_part = sin_part + cos_part;

        arma::cx_vec coef_series = multiply_series(trig_part, R_025_series);
        return coef_series;
    }

    //returns a matrix containing series perturbations for a, up to order 4, in the format idx X order
    arma::cx_mat a_series_first_order(const int max_order, const arma::cx_vec &a0, const arma::cx_vec &adot0, const double t0, const int max_mode){
        assert(t0 > 0);
        assert(a0.n_rows == adot0.n_rows);
        const int num_of_modes = std::min(a0.n_rows, adot0.n_rows);
        const int num_of_modes_tofill = (max_mode + 1 < num_of_modes) ? max_mode + 1 : num_of_modes;
        arma::cx_mat a_series_coefs(num_of_modes, max_order + 1, arma::fill::zeros);
        for(int idx = 0; idx < num_of_modes_tofill; ++idx){
            int const l = aid::l_from_index(idx);
            if (l == 1) {
                a_series_coefs.row(idx) = a_1_series_first_order(max_order, a0(idx), adot0(idx), t0).st();
            } else {
                a_series_coefs.row(idx) = a_non1_series_first_order(max_order, a0(idx), adot0(idx), t0, l).st();
            }
        }
        return a_series_coefs;
    }

    //matrix of the l.h.s perturbation equations
    arma::mat a_first_order_perturbation_equation_matrix(const int max_order, const double t0, const int l){
        double der_scaling = 1.0 / t0;
        arma::mat der2 = derivative_matrix(2, max_order, der_scaling);
        arma::mat der1 = derivative_matrix(1, max_order, der_scaling);
        arma::mat part2 = (3.0 / t0) * multiplication_matrix(Rdot_t0_over_R_series(max_order, 1)) * der1;
        arma::mat part3 = (1.5 / (t0 * t0)) * double (l - 1) * multiplication_matrix(Rdot_t0_over_R_series(max_order, 2));
        return der2 + part2 + part3;        
    }

    //recieves the perturbation series for the first order equations via the matrix formalism.
    arma::cx_vec first_order_matrix_solution(const int max_order, const arma::cx_double a0, const arma::cx_double adot0, const double t0, const int l){
        arma::cx_vec res_coeficients(max_order + 1, arma::fill::zeros);
        arma::cx_vec temp_multiplied_coeficients(max_order + 1, arma::fill::zeros);
        res_coeficients(0) = a0;
        res_coeficients(1) = adot0 * t0;
        arma::mat zeros_mat(max_order + 1, max_order + 1, arma::fill::zeros);
        arma::cx_mat first_order_matrix(a_first_order_perturbation_equation_matrix(max_order, t0, l), zeros_mat);
        for(int order = 2; order <= max_order; ++order){
            temp_multiplied_coeficients = first_order_matrix * res_coeficients;
            res_coeficients(order) = -temp_multiplied_coeficients(order - 2) / first_order_matrix(order - 2, order); //since this is a second order equation, each element is given by the previous two (satisfing the [order-2] cond)
        }
        return res_coeficients;
    }

       //solves for coeficients with given non homogenic terms in the right hand side of the equation. solves up to the same order
    arma::cx_vec non_homogenic_order_matrix_solution(const arma::cx_vec &rhs, const arma::cx_double a0, const arma::cx_double adot0, const double t0, const int l){
        const int max_order = rhs.n_rows - 1;
        arma::cx_vec res_coeficients(max_order + 1, arma::fill::zeros);
        arma::cx_vec temp_multiplied_coeficients(max_order + 1, arma::fill::zeros);
        res_coeficients(0) = a0;
        res_coeficients(1) = adot0 * t0;
        arma::mat zeros_mat(max_order + 1, max_order + 1, arma::fill::zeros);
        arma::cx_mat first_order_matrix(a_first_order_perturbation_equation_matrix(max_order, t0, l), zeros_mat);
        for(int order = 2; order <= max_order; ++order){
            temp_multiplied_coeficients = first_order_matrix * res_coeficients;
            res_coeficients(order) = (rhs(order - 2) - temp_multiplied_coeficients(order - 2)) / first_order_matrix(order - 2, order); //since this is a second order equation, each element is given by the previous two (satisfieng the [order-2] cond)
            
        }
        return res_coeficients;
    }

    //returns components of the r.h.s
    arma::cx_mat get_rhs(const std::vector<arma::sp_cx_mat> &Q_vec, const std::vector<arma::sp_cx_mat> &Z_vec, const std::vector<arma::sp_cx_mat> &D_vec, const arma::cx_mat &a_coef_mat, const double t0, const double R0, const int max_mode){
        assert(Q_vec.size() == a_coef_mat.n_rows);
        assert(Z_vec.size() == a_coef_mat.n_rows);
        assert(D_vec.size() == a_coef_mat.n_rows);

        assert(R0 > 0);
        assert(t0 > 0);

        const int num_of_modes = a_coef_mat.n_rows;
        const int num_of_modes_tofill = (max_mode + 1 <num_of_modes) ? max_mode + 1 : num_of_modes;
        const int max_order = a_coef_mat.n_cols - 1;
        arma::cx_mat rhs(num_of_modes, max_order + 1, arma::fill::zeros); //matrix for right hand side components

        //derivative coeficients
        arma::cx_mat adot_coef_mat_transpose(max_order + 1, num_of_modes, arma::fill::zeros);
        arma::mat der_mat = seriesappx::first_derivative_matrix(max_order, 1.0 / t0);
        arma::cx_vec temp_row_multiplication;
        for(int mode = 0; mode < num_of_modes_tofill; ++mode){
            adot_coef_mat_transpose.col(mode) = der_mat * a_coef_mat.row(mode).st();
        }
        arma::cx_mat adot_coef_mat = adot_coef_mat_transpose.st();

        //Coeficients for matrix multiplication
        arma::vec one_over_R_series_real_temp = seriesappx::R_over_R0_series(max_order, -1.0) * (1.0 / R0);
        arma::vec zeros_vec(max_order + 1, arma::fill::zeros);
        arma::cx_vec one_over_R_series(one_over_R_series_real_temp, zeros_vec);
        arma::cx_vec Q_coef = (1.0 / (t0 * t0)) * seriesappx::multiply_series(one_over_R_series, seriesappx::Rdot_t0_over_R_series(max_order, 2.0));
        arma::cx_vec Z_coef = (1.0 / t0) * seriesappx::multiply_series(one_over_R_series, seriesappx::Rdot_t0_over_R_series(max_order, 1.0));
        arma::cx_vec D_coef = one_over_R_series;

        for(int order1 = 0; order1 <= max_order; ++order1){ //going over orders of the first vector
            arma::cx_vec a_order1 = a_coef_mat.col(order1);
            arma::cx_vec adot_order1 = adot_coef_mat.col(order1);
 
            for(int order2 = 0; order2 <= max_order - order1; ++order2){ //going over orders of the second vector
                arma::cx_vec a_order2 = a_coef_mat.col(order2);
                arma::cx_vec adot_order2 = adot_coef_mat.col(order2);

                for(int order3 = 0; order3 <= max_order - order1 - order2; ++order3){ //going over orders of the R, Rdot coef

                    for(int mode = 0; mode < num_of_modes; ++mode){
                        rhs(mode, order1 + order2 + order3) += Q_coef.at(order3) * arma::dot(a_order1, Q_vec.at(mode) * a_order2);
                        rhs(mode, order1 + order2 + order3) += Z_coef.at(order3) * arma::dot(adot_order1, Z_vec.at(mode) * a_order2);
                        rhs(mode, order1 + order2 + order3) += D_coef.at(order3) * arma::dot(adot_order1, D_vec.at(mode) * adot_order2);
                    }
                }
            }
        }
        return rhs;
    }

    //recursive solution for a coeficients
    arma::cx_mat a_second_order_iterative_solution(const std::vector<arma::sp_cx_mat> &Q_vec, const std::vector<arma::sp_cx_mat> &Z_vec, const std::vector<arma::sp_cx_mat> &D_vec, 
        const arma::cx_vec &a0, const arma::cx_vec &adot0, const double t0, const double R0, const int max_order, const int max_depth, const double required_accuracy, const int max_mode){

        assert(Q_vec.size() == Z_vec.size());
        assert(Z_vec.size() == D_vec.size());
        assert(D_vec.size() == a0.n_rows);
        assert(adot0.n_rows == a0.n_rows);
        assert(max_depth >= 0);
        assert(required_accuracy > 0);
        
        const int num_of_modes = std::min(a0.n_rows, adot0.n_rows);
        const int num_of_modes_tofill = (max_mode + 1 < num_of_modes) ? max_mode + 1 : num_of_modes; 
        arma::cx_mat a_coef_mat = seriesappx::a_series_first_order(max_order, a0, adot0, t0, max_mode);
        arma::cx_mat iteration_diff(num_of_modes, max_order + 1, arma::fill::zeros);
        arma::cx_mat rhs;

        for(int iteration = 0; iteration < max_depth; ++iteration){
            rhs = seriesappx::get_rhs(Q_vec, Z_vec, D_vec, a_coef_mat, t0, R0, max_mode);

            //std::cout << "rhs created seccuessfully in order 2 comp" << std::endl;
            for(int mode = 0; mode < num_of_modes_tofill; ++mode){
                int const l = aid::l_from_index(mode);
                iteration_diff.row(mode) = seriesappx::non_homogenic_order_matrix_solution(rhs.row(mode).st(), a0(mode), adot0(mode), t0, l).st() - a_coef_mat.row(mode);
            }

            a_coef_mat += iteration_diff;
            if(arma::norm(iteration_diff,2) <= required_accuracy){
                std::cout << "second order series coef computation: ended at iteration " << iteration << " after reaching sufficient accuracy" << std::endl;
                iteration = num_of_modes; //end loop
            }

        }
        
        if(arma::norm(iteration_diff,2) > required_accuracy){
            std::cout << "second order series coef computation: ended at accuracy " << arma::norm(iteration_diff,2) << " when max iteration was reached" << std::endl;
        }

        return a_coef_mat;
    }


    
}