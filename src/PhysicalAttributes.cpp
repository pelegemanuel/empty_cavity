#include "PhysicalAttributes.hpp"
#include <cassert>
#include <iostream>

double P0_calculator(double const P_out_var, double alpha_var, double const R0_var){
    if(R0_var > 0){
        return P_out_var + 2.0 * alpha_var / R0_var;
    } else {
        std::cout << "illegal equilibrium radius. Setting inner pressure to be zero" << std::endl;
        return 0.0;
    }
}

PhysicalAttributes::PhysicalAttributes(double const init_alpha, 
                                       double const init_R0_equilibrium, 
                                       double const init_P_out) :
alpha(init_alpha),
R0_equilibrium(init_R0_equilibrium),
P_out(init_P_out),
P0(P0_calculator(P_out, alpha, R0_equilibrium))
{
    std::cout << "PhysicalAttributes object created (oscillation constructor)" << std::endl;
    print_physical_parameters();
}

PhysicalAttributes::PhysicalAttributes(double const init_alpha) :
alpha(init_alpha),
R0_equilibrium(0.0),
P_out(0.0),
P0(0.0)
{
    std::cout << "PhysicalAttributes object created (collapse constructor)" << std::endl;
    print_physical_parameters();
}

PhysicalAttributes::PhysicalAttributes(PhysicalAttributes const &initPhysicalAttributes) :
alpha(initPhysicalAttributes.get_alpha()),
R0_equilibrium(initPhysicalAttributes.get_R0_equilibrium()),
P_out(initPhysicalAttributes.get_P_out()),
P0(initPhysicalAttributes.get_P0())
{
    std::cout << "PhysicalAttributes object copied" << std::endl;
    print_physical_parameters();
}