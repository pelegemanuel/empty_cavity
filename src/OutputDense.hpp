#ifndef OUTPUT_DENSE_H
#define OUTPUT_DENSE_H

#include "Output.hpp"

class OutputDense: public Output
{
    private:
        int const index_to_save;
        std::ofstream save_file;

    public:
        OutputDense(int const num_of_modes, std::string const &run_folder, int init_index_to_save) :
        Output(num_of_modes, run_folder), index_to_save(init_index_to_save), save_file(save_path + filenames::dense_saves) {}

        inline void push_data(SystemState const &state, int const step, double const dt){
            outputAid::write_main_state_terms(save_file, state);
            save_file << outputAid::complex_to_string(state.get_mode_amps_at_index(index_to_save)) << " "
                        << outputAid::complex_to_string(state.get_mode_vels_at_index(index_to_save)) << std::endl;
        }

        inline void close_all(){ save_file.close(); }
};

#endif