#include "artificialCoefficients.hpp"
#include "aid.hpp"
#include "aid_lm_indexing.hpp"
#include <cassert>

//Artificial dissipation according to "Weak Turbulent Kolmogorov Spectrum for Surface Gravity Waves" - Dyachenko, Korotkevich, Zakharov
//https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.92.134501
arma::cx_vec disp_coefs_Dyachenko_Korotkevich_Zakahrov_2003(int const num_of_modes, int const l1, int const l2, double const gamma1, double const gamma2){
    assert(num_of_modes > 0);
    assert(l1 > 0);
    assert(l2 > l1);

    arma::cx_vec disp_coefs(num_of_modes, arma::fill::zeros);
    int const upper_bound_for_gamma1_disp = aid::index_from_lm(l1, l1);
    int const lower_bound_for_gamma2_disp = aid::index_from_lm(l2, -l2);
    double const k2 = aid::get_k_from_l(l2);

    for(int mode_index = 0; mode_index <= upper_bound_for_gamma1_disp; ++mode_index){
        disp_coefs[mode_index] = gamma1;
    }

    for(int mode_index = lower_bound_for_gamma2_disp; mode_index < num_of_modes; ++mode_index){
        double const k = aid::get_k_from_index(mode_index);
        disp_coefs[mode_index] = gamma2 * (k - k2) * (k - k2);
    }

    return disp_coefs;
}

//Artificial dissipation according to "Turbulence of Capillary Waves" - Pushkarev, Zakharov
//https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.76.3320
arma::cx_vec disp_coefs_Pushkarev_Zakharov_1996(int const num_of_modes, int const l0, double const gamma){
    assert(num_of_modes > 0);
    assert(l0 > 0);

    arma::cx_vec disp_coefs(num_of_modes, arma::fill::zeros);
    int const lower_bound_for_dissipation = aid::index_from_lm(l0, -l0);

    for(int mode_index = lower_bound_for_dissipation; mode_index < num_of_modes; ++mode_index){
        double const k = aid::get_k_from_index(mode_index);
        disp_coefs[mode_index] = gamma * (k + k * k);
    }

    return disp_coefs;
}

//Artificial pumping according to "Turbulence of Capillary Waves" - Pushkarev, Zakharov
//https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.76.3320
arma::cx_vec pump_coefs_Pushkarev_Zakharov_1996(int const num_of_modes, double const k1, double const k2, double const f0){
    assert(num_of_modes > 0);
    assert(k1 > 0);
    assert(k2 > 0);

    arma::cx_vec pump_coefs(num_of_modes, arma::fill::zeros);

    for(int mode_index = 0; mode_index < num_of_modes; ++mode_index){
        double const k = aid::get_k_from_index(mode_index);
        pump_coefs[mode_index] = std::exp(-std::pow(k - k1, 4.0) / k2);
    }
    return f0 * pump_coefs;
}

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
double h_function_Landau_plasma_damping(double const x){
    if(x <= aid::EPS){
        return 0.0;
    } else if (x <= 1.0) {
        return (1.0 / 6.0) * std::exp(5.0 * (1.0 - std::pow(x, -2.0))) * std::pow(x, -5.0);
    } else {
        return 1.0 - (5.0 / 6.0) * exp(0.5 * (1.0 - x * x));
    }
}

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
arma::cx_vec disp_coefs_Landau_plasma_damping_Dyachenko(int const num_of_modes, double const kd){
    assert(num_of_modes > 0);
    assert(kd > 0.0);

    arma::cx_vec disp_coefs(num_of_modes, arma::fill::zeros);

    for(int mode_index = 0; mode_index < num_of_modes; ++mode_index){
        double const k = aid::get_k_from_index(mode_index);
        disp_coefs[mode_index] = 0.5 * k * k * h_function_Landau_plasma_damping(k / kd);
    }

    return disp_coefs;
}

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
arma::cx_vec disp_coefs_Dyachenko_large_wavelengths(int const num_of_modes, double const k_bound, double const coeficient){
    assert(num_of_modes > 0);
    assert(k_bound > 0.0);

    arma::cx_vec disp_coefs(num_of_modes, arma::fill::zeros);
    int const l_bound = int(std::ceil(aid::get_l_from_k(k_bound)));
    int const index_bound = aid::index_from_lm(l_bound, -l_bound);

    for(int mode_index = 0; mode_index < index_bound; ++mode_index){
        double const k = aid::get_k_from_index(mode_index);
        disp_coefs[mode_index] = std::pow(k / k_bound - 1.0, 2.0);
    }
    return coeficient * disp_coefs;
}

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
arma::cx_vec pumping_coefs_Dyachenko(int const num_of_modes, double const k_min, double const k_max, double const coeficient){
    assert(num_of_modes > 0);
    assert(k_min > 0.0);
    assert(k_max > k_min);

    int const l_min = int(std::ceil(aid::get_l_from_k(k_min)));
    int const l_max = int(std::floor(aid::get_l_from_k(k_max)));
    int const min_index = aid::min_index_for_l(l_min);
    int const max_index = std::min(num_of_modes - 1, aid::max_index_for_l(l_max));
    arma::cx_vec disp_coefs(num_of_modes, arma::fill::zeros);

    for(int mode_index = min_index; mode_index <= max_index; ++mode_index){
        double const k = aid::get_k_from_index(mode_index);
        disp_coefs[mode_index] = std::sqrt((k * k - k_min * k_min) * (k_max * k_max - k * k));
    }
    return coeficient * disp_coefs / arma::max(arma::abs(disp_coefs));
}