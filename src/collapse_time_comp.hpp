#ifndef COLLAPSE_TIME_H
#define COLLAPSE_TIME_H

//see collapse time computation for consts etc.
//in "surface tension file"
namespace collapsetime{

    static double const MAX_THETA_FOR_TAYLOR = 0.1;

    inline double get_theta_param(double const R0, double const vel0, double const alpha){return 2.0 * alpha / (R0 * vel0 * vel0); }

    inline double get_y_param(double const theta){ return theta / (1.0 + theta); }

    inline bool deriavtive_condition(double const chi, double const theta){ return (chi >= 2.0 / (2.0 * theta + 5.0)); }

    double get_time_numerical(double const theta, double const time_scale);

    double get_time_leading_taylor(double const theta, double const time_scale);

    double get_collapse_time(double const theta, double const time_scale);
}

#endif