#include "matrix_constructor.hpp"
#include "wigner_coefs.hpp"
#include "aid_lm_indexing.hpp"
#include <cassert>

//IGNORING INTERACTIONS OF THE (0,0) TERM

//total interaction matrix for given mode (l_base, m_base). Reduced by scaling if shouldReduce
arma::mat total_gaunt_mat(int const N_indexes, int const l_base, int const  m_base, bool const shouldReduce){
    assert(N_indexes > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    arma::mat A(N_indexes, N_indexes, arma::fill::zeros);

    for (int idx1 = 0; idx1 < N_indexes; ++idx1){
        auto const [l1, m1] = aid::lm_from_index(idx1);
    
        for (int idx2 = 0; idx2 <= idx1; ++idx2){
            auto const [l2, m2] = aid::lm_from_index(idx2);
            A(idx1, idx2) = coefs::gaunt_coef(l_base, l1 ,l2 , m_base, m1, m2, shouldReduce);
            A(idx2, idx1) = A(idx1, idx2);
        }
    }
    return A;
}

//interaction mat where components with different frequencies (different l's) have been canceled out
arma::mat total_gaunt_mat_same_l_only(int const N_indexes, int const l_base, int const m_base, bool const shouldReduce){
    assert(N_indexes > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    arma::mat A(N_indexes, N_indexes, arma::fill::zeros);

    for (int idx1 = 0; idx1 < N_indexes; ++idx1){
        auto const [l1, m1] = aid::lm_from_index(idx1);
    
        for (int idx2 = 0; idx2 <= idx1; ++idx2){
            auto const [l2, m2] = aid::lm_from_index(idx2);
            if (l1 == l2){
                A(idx1, idx2) = coefs::gaunt_coef(l_base, l1 ,l2 , m_base, m1, m2, shouldReduce);
                A(idx2, idx1) = A(idx1, idx2);
            }
        }
    }
    return A;

}

// C1, C2 matrix for l_base,m_base,m1,m2. 
// for proper definition of C1, C2: see gaunt_spectrum.lyx 
// basic l_indices of C are saved to base_indices pointer if given as input
arma::mat get_Ci_matrix(int const l_base, int const  m_base, int const m1, int const  m2, int const l_max, int const l1_parity, bool const shouldReduce, std::pair<int,int> &base_indices){
    assert(m1 + m2 + m_base == 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    //set l1,l2 boundaries
    int l1_min, l2_min, l1_max, l2_max;

    l1_max = l_max;
    l2_max = l_max;
    l1_min = std::max(std::abs(m1), 1);
    l2_min = std::max(std::abs(m2), 1);

    //max parity
    if (l1_max % 2 != l1_parity % 2){ --l1_max; }
    if (l2_max % 2 != (l_base+l1_parity) % 2){ --l2_max; }

    //min parity
    if (l1_min % 2 != l1_parity % 2){ ++l1_min; }
    if (l2_min % 2 != (l_base+l1_parity) % 2){ ++l2_min; }

    //making sure the left triangular condition is applied from the start
    //for all l_base > 0, the following condition is satisfied only if l1 > l2
    if (l1_min - l2_min > l_base){
        l2_min = l1_min - l_base; //"mod" condition automatically satisfied
    }
    
    //fill matrix
    arma::mat C_mat(1 + (l1_max - l1_min) / 2, 1 + (l2_max - l2_min) / 2, arma::fill::zeros);
    for(int l1 = l1_min; l1 <= l1_max; l1 += 2){
        for (int l2 = l2_min; l2 <= l2_max; l2 += 2){
            int l1_index = (l1 - l1_min) / 2;
            int l2_index = (l2 - l2_min) / 2;
            C_mat(l1_index, l2_index) = coefs::gaunt_coef(l_base, l1, l2, m_base, m1, m2, shouldReduce);
        } 
    }
    base_indices.first = l1_min;
    base_indices.second = l2_min;
    return C_mat;
}

arma::mat get_Ci_matrix(int const l_base, int const m_base, int const m1, int const m2, int const l_max, int const l1_parity, bool const shouldReduce){
    std::pair<int,int> base_indices;
    return get_Ci_matrix(l_base, m_base, m1, m2, l_max, l1_parity, shouldReduce, base_indices);
}

// C matrix for the symmetric m1=m2 case. Must have even m_base.
// for more information see gaunt_spectrum.lyx
arma::mat get_symmetric_C_mat(int const l_base, int const m_base, int const l_max, bool const shouldReduce){
    assert(m_base % 2 == 0);
    assert(l_max > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    const int m = -m_base / 2;
    const int l_min = std::max(abs(m), 1); //ignoring the (0,0) term
    const int len = l_max - l_min + 1;
    arma::mat C(len, len, arma::fill::zeros);
    for(int l1 = l_min; l1 <= l_max; ++l1){
        for(int l2 = l_min; l2 <= l1; ++l2){
            C(l1 - l_min, l2 - l_min) = coefs::gaunt_coef(l_base, l1, l2, m_base, m, m, shouldReduce);
            C(l2 - l_min, l1 - l_min) = C(l1 - l_min, l2 - l_min);
        }
    }
    return C;
}

// Returns matrix for second order interations. Matrix type is set by given function.
// for more information see empty_cavity.lyx
arma::mat get_sec_order_mat(int const N_indexes, int const  l_base, int const m_base, bool const shouldReduce, double (*f)(int,int,int,int,int,int,bool)){
    assert(N_indexes > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    arma::mat second_order_mat(N_indexes, N_indexes, arma::fill::zeros);

    for (int index1 = 0; index1 < N_indexes; ++index1){
        auto const [l1, m1] = aid::lm_from_index(index1);
    
        for (int index2 = 0; index2 < N_indexes; ++index2){
            auto const [l2, m2] = aid::lm_from_index(index2);
            second_order_mat(index1, index2) = f(l_base, l1 ,l2 , m_base, m1, m2, shouldReduce);
        }
    }
    return second_order_mat;
}

//same as previous, uses known sizes knowing that f is proportional to the Gaunt matrices of (-m_base, m1, m2)
arma::mat get_sec_order_mat_smart(int const N_indexes, int const  l_base, int const m_base, bool const shouldReduce, double (*f)(int,int,int,int,int,int,bool)){
    assert(N_indexes > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    arma::mat second_order_mat(N_indexes, N_indexes, arma::fill::zeros);

    //we go over pairs such that index1 >=index2 and so l1 >= l2
    const int min_l1 = (l_base + 1) / 2;
    const int min_index1 = aid::index_from_lm(min_l1, -min_l1);

    for (int index1 = min_index1; index1 < N_indexes; ++index1){
        auto const [l1, m1] = aid::lm_from_index(index1);
        int const m2 = m_base - m1;

        int min_l2 = std::max(std::max(1, l1 - l_base), std::abs(m2));
        if((min_l2 + l1 + l_base) % 2  == 1){
            ++min_l2;
        }

         //if m2 > m1, at l2=l1, l2 will have a greater index and shouldn't be considered.
        int const max_l2 = (m2 > m1) ? l1 - 1 : l1;
        
        for(int l2 = min_l2; l2 <= max_l2; l2 += 2){
            int const index2 = aid::index_from_lm(l2, m2);

            //std::cout << l1 << " " << m1 << " | " << l2 << " " << m2 << " " << index2 << std::endl;
            second_order_mat(index1, index2) = f(l_base, l1 ,l2 , m_base, m1, m2, shouldReduce);
            second_order_mat(index2, index1) = f(l_base, l2 ,l1 , m_base, m2, m1, shouldReduce);
        }
    }

    return second_order_mat;
}