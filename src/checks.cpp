#include <time.h>
#include <iostream>
#include <cassert>
#include "checks.hpp"
#include "aid.hpp"
#include "aid_lm_indexing.hpp"
#include "aid_spherical.hpp"
#include "aid_arma_templates.hpp"
#include "eigenvalues.hpp"
#include "wigner_coefs.hpp"
#include "matrix_constructor.hpp"
#include "series_appx_plesset.hpp"
#include "collapse_time_comp.hpp"
#include "PhysicalAttributes.hpp"
#include "Point.hpp"
#include "initial_conditions.hpp"
#include "point_function_gradient.hpp"
#include "liquid_hydrodynamics.hpp"

//runs all checks
void checks_main(){
    bool are_all_checks_ok = true;
    are_all_checks_ok = are_all_checks_ok && lm_index_correspondence(50);
    are_all_checks_ok = are_all_checks_ok && point_spherical_cartesian_checker();
    are_all_checks_ok = are_all_checks_ok && eigenvalue_algorithms(false);
    are_all_checks_ok = are_all_checks_ok && plesset_first_order_series_approximants();
    are_all_checks_ok = are_all_checks_ok && collapse_time_derivative_condition();
    are_all_checks_ok = are_all_checks_ok && second_order_matrix_comparison(1000, 100, false);
    are_all_checks_ok = are_all_checks_ok && linear_function_gradient_comparison();
    are_all_checks_ok = are_all_checks_ok && spherical_harmonic_gradient_comparison();
    are_all_checks_ok = are_all_checks_ok && liquid_velocity_calculations_comparison();
    liquid_velocity_calculations_time_comparison(500);
    if(are_all_checks_ok){
        std::cout << "All tests work :)" << std::endl;
    } else {
        std::cout << "Some tests failed :(" << std::endl;
    }
}

SystemState get_default_check_state(){
    int const num_of_modes = aid::mode_num(L_SH_CHECK);
    aid::set_scaling_beta(1.0);
    double const alpha = 1.0;
    double const sig = 0.1;
    double const R0 = 1.0;
    double const P_out = 0.0;
    double const init_R_velocity = 0.0;
    PhysicalAttributes const physAttributes(alpha, R0, P_out);
    int const base_l = 1;
    InitialConditionsData const initData(initcond::SURFACE_OSCILLATIONS_NORMAL, sig, physAttributes, init_R_velocity, base_l); //oscillations
    return SystemState(num_of_modes, physAttributes, initData);
}

//ensuring lm-index is one to one
bool lm_index_correspondence(int const max_index_to_check){
    bool is_all_good = true;
    for(int index_to_check; index_to_check <= max_index_to_check; ++index_to_check){
        auto const [l, m] = aid::lm_from_index(index_to_check);
        int const recreated_index = aid::index_from_lm(l, m);
        if (recreated_index != index_to_check){
            std::cout << "Error! incompattible index: " << index_to_check << " | " << recreated_index << std::endl;
            is_all_good = false;
        }
    }
    if(is_all_good){ 
        std::cout << "lm index correspondence - all good" << std::endl;
    } else {
        std::cout << "Problem with lm-index corrspondence" << std::endl;
    }
    return is_all_good;
}

bool point_spherical_cartesian_checker(int const num_of_trials){
    arma::mat cartesian_coordinates(3, num_of_trials, arma::fill::randu);
    cartesian_coordinates = 2.0 * (cartesian_coordinates - 0.5);
    bool is_all_good = true;

    for(int trial = 0; trial < num_of_trials; ++trial){
        Point testPoint(cartesian_coordinates.col(trial));

        arma::vec back_to_cartesian(3, arma::fill::zeros);
        back_to_cartesian(aidsph::X_INDEX) = testPoint.get_r() * std::sin(testPoint.get_theta()) * std::cos(testPoint.get_phi());
        back_to_cartesian(aidsph::Y_INDEX) = testPoint.get_r() * std::sin(testPoint.get_theta()) * std::sin(testPoint.get_phi());
        back_to_cartesian(aidsph::Z_INDEX) = testPoint.get_r() * std::cos(testPoint.get_theta());
        double const diff_norm = arma::norm(cartesian_coordinates.col(trial) - back_to_cartesian);
        if(diff_norm > aid::EPS){
            std::cout << "Coordinate change found! " << diff_norm << std::endl;
            is_all_good = false;
        }

        arma::vec r_dir = testPoint.get_r_hat();
        arma::vec theta_dir = testPoint.get_theta_hat();
        arma::vec phi_dir = testPoint.get_phi_hat();
        if(!aidsph::are_orthogonal(r_dir, theta_dir) || !aidsph::are_orthogonal(r_dir, phi_dir) || !aidsph::are_orthogonal(theta_dir, phi_dir)){
            is_all_good = false;
            std::cout << "Non orthogonal spherical coordinates!" << std::endl;
        }
    }
    if(is_all_good) { std::cout << "Spherical-Cartesean transformations in Point objects - all good" << std::endl; }
    return is_all_good;
}

//compare calculations of different algorithms fo eigenvalue calculation
bool eigenvalue_algorithms(bool shouldPrintTime){
    bool is_all_good = true;
    int const l = 5;
    int const m = 0;
    int const l_max = 40;
    double const bound = 0.001;
    int const num_of_indexes = aid::mode_num(l_max);
    bool const shouldReduce = true;
    clock_t clock_block, clock_direct;
    
    clock_block = clock();
    std::pair<std::vector<arma::vec>,std::vector<double>> const blockwise_pair = block_wise_eigenvectors(l, m, l_max, shouldReduce, bound);
    clock_block = clock() - clock_block;
    clock_direct = clock();
    std::pair<std::vector<arma::vec>,std::vector<double>> const direct_pair = direct_eigenvectors(l, m, l_max, shouldReduce, bound);
    clock_direct = clock() - clock_direct;

    std::vector<double>  const direct_eigs = direct_pair.second;
    std::vector<arma::vec> const direct_vecs = direct_pair.first;
    std::vector<double> const block_eigs = blockwise_pair.second;
    std::vector<arma::vec> const block_vecs = blockwise_pair.first;

    std::vector<double> const direct_eigs_only = direct_eigenvalues(l, m, l_max, shouldReduce, false);
    std::vector<double> const block_eigs_only = block_wise_eigenvalues(l, m, l_max, shouldReduce);

    arma::mat const T = total_gaunt_mat(num_of_indexes, l, m, shouldReduce);
    
    if(shouldPrintTime){
        std::cout << "direct time: " << clock_direct << std::endl;
        std::cout << "block time: " << clock_block << std::endl;
    }
    
    if((direct_eigs.size() != block_eigs.size()) || (block_eigs_only.size() != direct_eigs_only.size())) {
        std::cout << "Eigenvalue list sizes incompatible! " << std::endl;
        is_all_good = false;
    }

    for(int eig_index = 0; eig_index < block_vecs.size(); ++eig_index){
        arma::vec const current_eig_vec = block_vecs.at(eig_index);
        arma::vec const check_vec = T * current_eig_vec - block_eigs.at(eig_index) * current_eig_vec;
        if(arma::norm(check_vec) > aid::EPS){
            std::cout << "Incorrect eigvec! index: " << eig_index << std::endl;
            is_all_good = false;
        }
    }
    if(is_all_good) {std::cout << "eigenvalue algorithms - all good" << std::endl; }
    return is_all_good;
}

//check both taylor series functions yield the same value
bool plesset_first_order_series_approximants(){
    double const t0 = 0.4;
    arma::cx_double const a0(1.0, 0.5);
    arma::cx_double const adot0(0.3, -2.0);
    int const max_order = 30;
   
    arma::cx_vec const series_analytical_1 = seriesappx::a_1_series_first_order(max_order, a0, adot0, t0);
    arma::cx_vec const series_matrix_1 = seriesappx::first_order_matrix_solution(max_order, a0, adot0, t0, 1);
    arma::vec const diff_1 = arma::abs(series_analytical_1 - series_matrix_1);

    const int l = 10;
    arma::cx_vec const series_analytical_L = seriesappx::a_non1_series_first_order(max_order, a0, adot0, t0, l);
    arma::cx_vec const series_matrix_L = seriesappx::first_order_matrix_solution(max_order, a0, adot0, t0, l);
    arma::vec const diff_L = arma::abs(series_analytical_L - series_matrix_L);

    if (std::max(diff_1.max(), diff_L.max()) > aid::EPS){
        std::cout << "Difference in series approximation algorithms!" << std::endl;
        return false;
    } else {
        std::cout << "Series appx algorithms - all good" << std::endl;
        return true;
    }
}

//ensures the computed collapse times are decresing
bool collapse_time_derivative_condition(){
    int const N_alphas = 1000;
    double const delta_theta = 0.01;
    double const time_scale = 1.0; //so chi = t
    bool is_all_good = true;

    for(int check_iter = 0; check_iter < N_alphas; ++check_iter){
        double const theta = double(check_iter) * delta_theta;
        double const time_to_collapse = collapsetime::get_collapse_time(theta, time_scale);
        bool const check_res = collapsetime::deriavtive_condition(time_to_collapse, theta);
        if(!check_res){
            std::cout << "time bound not fulfilled for: " << theta << std::endl;
            is_all_good = false;
        }
    }

    if (is_all_good){
        std::cout << "Collapse time computations - all good" << std::endl;
    } else {
        std::cout << "Collapse times computated don't obey the derivative bound" << std::endl;
    }
    return is_all_good;
}

bool second_order_matrix_comparison(int const num_of_modes, int const base_index, bool const shouldPrintTime){
    assert(base_index < num_of_modes);

    clock_t clock_basic, clock_smart;
    auto const [l, m] = aid::lm_from_index(base_index);

    clock_basic = clock();
    arma::mat C_basic = get_sec_order_mat(num_of_modes, l, m, true, &coefs::C_mat_element);
    clock_basic = clock() - clock_basic;

    clock_smart = clock();
    arma::mat C_smart = get_sec_order_mat_smart(num_of_modes, l, m, true, &coefs::C_mat_element);
    clock_smart = clock() - clock_smart;

    double const diff_norm = arma::norm(C_basic - C_smart);

    if(shouldPrintTime){
        std::cout << "basic time: " << clock_basic << std::endl;
        std::cout << "smart time: " << clock_smart << std::endl;
    }
    

    if(diff_norm < aid::EPS){
        std::cout << "Second order matrix comparisons - all good" << std::endl;
        return true;
    }
    std::cout << "norm of diff mat between two comp methods: " << diff_norm << std::endl;
    return false;
}

double linear_function_for_gradient(Point const &location){return X_COEF * location.get_x() + Y_COEF * location.get_y() + Z_COEF * location.get_z(); }
bool linear_function_gradient_comparison(int const num_of_trials){
    double const radius = 2.0;
    arma::vec const expected_grad = aidsph::entries_to_vector(X_COEF, Y_COEF, Z_COEF);
    bool is_all_good = true;
    for(int trial = 0; trial < num_of_trials; ++trial){
        double const linear_function_gradient_diff = aid::relative_norm_difference(expected_grad, gradient(Point(radius), &linear_function_for_gradient));
        if(linear_function_gradient_diff > aid::EPS){ is_all_good = false; }
    }
    if(is_all_good) {
        std::cout << "Linear function gradient computation - all good" << std::endl; 
    } else {
        std::cout << "Found gradient mismatch for a linear function" << std::endl;
    }
    return is_all_good;
}

arma::cx_double check_spherical_harmonic(Point const &location){ return location.spherical_harmonic(L_SH_CHECK, M_SH_CHECK); }
bool spherical_harmonic_gradient_comparison(int const num_of_trials){
    double const radius = 1.5;
    bool is_all_good = true;
    for(int trial = 0; trial < num_of_trials; ++trial){
        Point const testPoint(radius);
        arma::cx_vec direct_sh_gradient = (1.0 / radius) * testPoint.spherical_harmonic_angular_gradient(L_SH_CHECK, M_SH_CHECK);
        arma::cx_vec numerical_sh_gradient = gradient(testPoint, &check_spherical_harmonic);
        double const relative_norm_diff = aid::relative_norm_difference(direct_sh_gradient, numerical_sh_gradient);
        if(relative_norm_diff > MAX_ALLOWED_GRAD_RELATIVE_DIFFERENCE){
            is_all_good = false;
            std::cout << "Different gradients computed! relative norm diff: " << relative_norm_diff << std::endl;
        }
    }
    if(is_all_good) {std::cout << "Spherical harmonic gradient computation - all good" << std::endl; }
    return is_all_good;
}

bool liquid_velocity_calculations_comparison(int const num_of_trials){
    SystemState state = get_default_check_state();
    double const point_radius = state.get_R() * 2.0;

    bool is_all_good = true;
    double direct_time = 0.0, grad_time = 0;
    for(int check_index = 0; check_index < num_of_trials; ++check_index){
        Point const point_to_check(point_radius);
        double const diff = aid::relative_norm_difference(hydro::get_liquid_local_velocity_direct(state, point_to_check), 
                                                            hydro::get_liquid_local_velocity_grad(state, point_to_check));
        if(diff > MAX_ALLOWED_GRAD_RELATIVE_DIFFERENCE) { 
            is_all_good = false;
            std::cout << "Vel difference: " << diff << std::endl;
        }
    }
    if(is_all_good) { std::cout << "Liquid velocity calculation comparison - all good" << std::endl; }
    return is_all_good;
}

void liquid_velocity_calculations_time_comparison(int const num_of_trials){
    SystemState state = get_default_check_state();
    double const point_radius = state.get_R() * 2.0;
    clock_t myClock;

    myClock = clock();
    for(int check_index = 0; check_index < num_of_trials; ++check_index){
        Point const point_to_check(point_radius);
        hydro::get_liquid_local_velocity_direct(state, point_to_check);
    }
    myClock = clock() - myClock;
    std::cout <<"Direct calculation: " << myClock << std::endl;

    myClock = clock();
    for(int check_index = 0; check_index < num_of_trials; ++check_index){
        Point const point_to_check(point_radius);
        hydro::get_liquid_local_velocity_grad(state, point_to_check);
    }
    myClock = clock() - myClock;
    std::cout <<"Grad calculation: " << myClock << std::endl;    
}
