#include "aid.hpp"
#include "aid_arma_templates.hpp"
#include <cassert>
#include <algorithm>

double aid::BETA_SCALING = 1.0;
double aid::BASE_SCALING = 0.0;
namespace aid
{
    //return generalized binomial coeficient alpha over k
    double generalized_binomial_coeficient(double const alpha, int const k){
        assert(k >= 0);
        if (k == 0){ return 1.0; }
        return (alpha / double(k)) * aid::generalized_binomial_coeficient(alpha - 1, k - 1);
    }

    //returns the factorial of n
    double factorial(int const n){
        assert (n >= 0);
        return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
    }

    arma::vec get_random_coordinates_on_unit_sphere(){
        double const phi = aid::uniform_random_vector(1, 0.0, 2.0 * aid::PI).at(0);
        double const cos_theta = aid::uniform_random_vector(1, -1.0, 1.0).at(0);
        double const sin_theta = std::sqrt(1.0 - cos_theta * cos_theta);
        return {sin_theta * std::cos(phi), sin_theta * std::sin(phi), cos_theta};
    }
}