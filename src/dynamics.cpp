#include "dynamics.hpp"

void run_dynamics(SystemState &state,  
                    TimeManager &timeManager, 
                    Output &output,
                    ParticleDynamics &particleDynamicsRunner,
                    ArtificialDynamics const &artificialDynamicsRunner){
    output.save_l_number_vector();
    double dt = timeManager.get_dt(state);
    while(timeManager.shouldContinue(state, dt)){
        output.push_data(state, timeManager.get_step(), dt);
        timeManager.printTimeData(state);
        double const artificial_acceleration = artificialDynamicsRunner.getArtificialAcceleration(state.get_R_velocity());
        state.update_all_physical_aid_quantities(artificial_acceleration, timeManager.shouldUpdateSecondOrder());
        state.step_half_implicit(dt, artificial_acceleration);
        state.perform_dissipationAndPumping(artificialDynamicsRunner, dt);
        particleDynamicsRunner.step_euler(state, dt);
        dt = timeManager.get_dt(state);
        timeManager.count_step();
    }
    output.close_all();
}