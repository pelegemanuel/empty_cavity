#ifndef AID_H
#define AID_H

#include <vector>
#include <math.h>
#include <armadillo>
#include <string>
#include "aid_lm_indexing.hpp"

namespace aid
{
    static const double PI = 3.14159265359;
    static const double SQRT_OF_2 = 1.41421356237;
    static const double EPS = 0.00000001;

    extern double BETA_SCALING;
    extern double BASE_SCALING;
    
    inline bool is_eff_zero(double const &x, double const eps = EPS){ return std::abs(x) < eps;}
    inline bool is_eff_real(arma::cx_double const z, double const eps = EPS){ 
        return (is_eff_zero(std::imag(z) / std::real(z), eps) || is_eff_zero(std::abs(z), eps));
    }
    inline bool is_eff_real(arma::cx_vec const &vec, double const eps = EPS){ 
        return (is_eff_zero(arma::norm(arma::imag(vec) / arma::norm(arma::real(vec))), eps) || is_eff_zero(arma::norm(vec), eps));
    }

    inline void set_scaling_beta(double const input) { BETA_SCALING = input; }
    inline double get_scaling_beta() { return BETA_SCALING; }

    inline void set_base_scaling(double const input) { BASE_SCALING = input; }
    inline double get_base_scaling() { return BASE_SCALING; }

    inline double mode_scaling(int const l){ return BASE_SCALING + std::pow(get_k_from_l(l), BETA_SCALING); }

    //return the mode frequency for a given mode. Assumes l > 1.
    inline double mode_frequency(int const l){ return 0.25 * std::sqrt(double(24 * l - 25)); }

    inline int sign(double const x){ return (x > 0) - (x < 0); }

    //return generalized binomial coeficient alpha over k
    double generalized_binomial_coeficient(double const alpha, int const k);

    //returns the factorial of n
    double factorial(int const n);

    arma::vec get_random_coordinates_on_unit_sphere();

    inline double sphere_volume(double const radius){ return (4.0 * aid::PI / 3.0) * std::pow(radius, 3.0); }
}

#endif