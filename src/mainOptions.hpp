#ifndef MAIN_OPTIONS_H
#define MAIN_OPTIONS_H

#include "aid_lm_indexing.hpp"
#include "OutputSparse.hpp"
#include "OutputDense.hpp"
#include "initial_conditions.hpp"
#include "time_manager.hpp"
#include "time_manager_collapse.hpp"
#include "artificialDynamicsIncludeAll.hpp"
#include "dynamics.hpp"
#include "checks.hpp"
#include <cstdlib>

void checks_runner(){
    checks_main();
}

void self_similar_runner(char *argv[]){
    std::string run_folder = argv[1];
    int const max_l = std::atoi(argv[2]);
    int const main_l = std::atoi(argv[3]);
    double const sig = std::atof(argv[4]);

    std::cout << "maximal l: " << max_l << std::endl;
    std::cout << "l to save: " << main_l << std::endl;
    std::cout << "sigma: " << sig << std::endl;
    
    int const num_of_modes = aid::mode_num(max_l);
    double const dt_to_time_scale_ratio = 0.01;
    int const base_l = 10;
    aid::set_scaling_beta(1.0);

    PhysicalAttributes const physAttributes(0.0);
    InitialConditionsData const initData(initcond::SURFACE_OSCILLATIONS_NORMAL, sig);
    SystemState state(num_of_modes, physAttributes, initData);

    double const max_amp_to_R_ratio = 0.1;
    double const max_norm_amps_to_R_ratio = 0.1;
    int const max_num_of_steps = int(std::pow(10, 5));
    double const max_simmulation_time = 0.5;
    TimeManagerCollapse timeManager(max_amp_to_R_ratio, max_norm_amps_to_R_ratio, dt_to_time_scale_ratio, max_simmulation_time, max_num_of_steps, state);
    OutputDense output(num_of_modes, run_folder, aid::index_from_lm(main_l, 0));
    state.set_shouldConsiderRadiusVelocityInSecondOrder(true);
    std::cout << "starting dynamics" << std::endl;
    run_dynamics(state, timeManager, output);
}

void simmulation_runner(char *argv[]){
    std::string run_folder = argv[1];
    int const max_l = std::atoi(argv[2]);
    double const dt_to_time_scale_ratio = std::atof(argv[3]);
    double const sig = std::atof(argv[4]);
    double const alpha = std::atof(argv[5]);
    double const beta = 2.1;
    int const base_l = 10;
    int const num_of_modes = aid::mode_num(max_l);

    std::cout << "maximal l: " << max_l << std::endl;
    std::cout << "dt/t_scale: " << dt_to_time_scale_ratio << std::endl;
    std::cout << "sigma: " << sig << std::endl;
    std::cout << "alpha: " << alpha << std::endl;
    std::cout << "beta: " << beta << std::endl;

    aid::set_scaling_beta(beta);

    int const l_base_disp = 50;
    double const k0 = aid::get_k_from_l(base_l);
    double const gamma_large_wavelength = 1000;
    double const gamma_small_wavelength = 0.1; 
    double const R_velocity_disp = -gamma_large_wavelength;
    arma::cx_vec const high_mode_dissipation = -disp_coefs_Pushkarev_Zakharov_1996(num_of_modes, l_base_disp, gamma_small_wavelength);
    arma::cx_vec const low_mode_dissipation = -disp_coefs_Dyachenko_large_wavelengths(num_of_modes, k0, gamma_large_wavelength);
    arma::cx_vec const total_dissipation = high_mode_dissipation + low_mode_dissipation;
    ArtificialDynamicsLinear artificialDissipation(total_dissipation, R_velocity_disp);

    double const k1_pump = aid::get_k_from_l(15);
    double const k2_pump = aid::get_k_from_l(6); 
    double const f0_pump = 0.0001;
    arma::cx_vec const pumping_coeficients_zakharov = pump_coefs_Pushkarev_Zakharov_1996(num_of_modes, k1_pump, k2_pump, f0_pump);
    double const frequency_modulation_std = 0.05;
    ArtificialDynamicsZakharovOscillator artificialZakharovPumping(pumping_coeficients_zakharov, frequency_modulation_std);

    ArtificialDynamicsMultiple const artificialTerms(&artificialDissipation, &artificialZakharovPumping);
    artificialTerms.printData();

    double const R0 = 1.0;
    double const P_out = 0.0;
    double const init_R_velocity = 0.0;
    PhysicalAttributes const physAttributes(alpha, R0, P_out);
    InitialConditionsData const initData(initcond::SURFACE_OSCILLATIONS_NORMAL, sig, physAttributes, init_R_velocity, base_l);
    SystemState state(num_of_modes, physAttributes, initData);

    double const max_amp_to_R_ratio = 0.1;
    double const max_norm_amps_to_R_ratio = 0.1;
    int const max_num_of_steps = int(std::pow(10, 8));
    double const max_simmulation_time = 500.0;
    TimeManager timeManager(max_l, max_amp_to_R_ratio, max_norm_amps_to_R_ratio, dt_to_time_scale_ratio, max_simmulation_time, max_num_of_steps);
    
    //data variables
    double const base_mode_timescale = 2.0 * aid::PI / std::sqrt(alpha * std::pow(k0, 3.0));
    int const max_steps_between_saves = int(std::pow(10, 6));
    double const max_time_between_saves = 20.0 * base_mode_timescale;
    OutputSparse output(num_of_modes, run_folder, max_steps_between_saves, max_time_between_saves);

    state.set_shouldConsiderRadiusVelocityInSecondOrder(false);
    std::cout << "starting dynamics" << std::endl;
    run_dynamics(state, timeManager, output, artificialTerms);
}

#endif