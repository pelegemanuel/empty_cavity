#include "collapse_time_comp.hpp"
#include <cmath>
#include <cassert>

namespace collapsetime{

    double get_time_numerical(double const theta, double const time_scale){
        double const g0 = 1.0;
        int const N_divisions = std::pow(10, 4);
        double const dg = g0 / double(N_divisions);

        double integral_part = 0;
        for(int division_index = 0; division_index < N_divisions; division_index++){
            double const g = (0.5 + double(division_index)) * dg; //midstep value
            double const integrand = 1.0 / std::sqrt(1.0 + theta * (1.0 - std::pow(g, 0.8)));
            integral_part += dg * integrand;
        }

        return 0.4 * time_scale * integral_part;
    }

    double get_time_leading_taylor(double const theta, double const time_scale){
        assert(theta >= 0);
        assert(time_scale >= 0);
        double const y = collapsetime::get_y_param(theta);
        double const series_2nd_order = 0.4 + (1.0 / 9.0) * y + (3.0 / 52.0) * y * y;
        return time_scale * series_2nd_order * std::pow(1.0 + theta, -0.5);
    }

    double get_collapse_time(double const theta, double const time_scale){
        assert(theta >= 0);
        assert(time_scale >= 0);
        if(theta <= collapsetime::MAX_THETA_FOR_TAYLOR){
            return collapsetime::get_time_leading_taylor(theta, time_scale);
        } else {
            return collapsetime::get_time_numerical(theta, time_scale);
        }
    }
}