#include "second_order_terms.hpp"
#include "matrix_constructor.hpp"
#include "aid_lm_indexing.hpp"
#include "wigner_coefs.hpp"


SecondOrder::SecondOrder(int const init_num_of_modes)
: num_of_modes(init_num_of_modes),
C_vec(num_of_modes),
K_vec(num_of_modes),
X_vec(num_of_modes),
Z_vec(num_of_modes),
D_vec(num_of_modes)
{
    arma::mat const zeros_mat(num_of_modes, num_of_modes, arma::fill::zeros);

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        auto const [l, m] = aid::lm_from_index(index);

        C_vec[index] = arma::sp_cx_mat(arma::cx_mat(get_sec_order_mat_smart(num_of_modes, l, m, false, &coefs::C_mat_element), zeros_mat));
        K_vec[index] = arma::sp_cx_mat(arma::cx_mat(get_sec_order_mat_smart(num_of_modes, l, m, false, &coefs::K_mat_element), zeros_mat));
        X_vec[index] = arma::sp_cx_mat(arma::cx_mat(get_sec_order_mat_smart(num_of_modes, l, m, false, &coefs::X_mat_element), zeros_mat));
        Z_vec[index] = arma::sp_cx_mat(arma::cx_mat(get_sec_order_mat_smart(num_of_modes, l, m, false, &coefs::Z_mat_element), zeros_mat));
        D_vec[index] = arma::sp_cx_mat(arma::cx_mat(get_sec_order_mat_smart(num_of_modes, l, m, false, &coefs::D_mat_element), zeros_mat));
    }

    std::cout << "SecondOrder object created" << std::endl;     
}

