#include "SecondOrderTermsCalculator.hpp"
#include "dynamic_state.hpp"
#include "second_order_coef_multipliers.hpp"

SecondOrderTermsCalculator::SecondOrderTermsCalculator(int const init_num_of_modes) :
num_of_modes(init_num_of_modes),
secondOrderCoefs(num_of_modes),
tempRightHandSideRetVec(num_of_modes, arma::fill::zeros),
last_C_multiplier(0.0), last_K_multiplier(0.0), last_X_multiplier(0.0),
ampAmp_nonlin_matrices(num_of_modes)
{
    arma::mat const temp_dense_real_mat(num_of_modes, num_of_modes, arma::fill::zeros);
    arma::sp_mat const temp_sparse_real_mat(temp_dense_real_mat);

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        ampAmp_nonlin_matrices[index] = arma::sp_cx_mat(temp_sparse_real_mat, temp_sparse_real_mat);
    }
    std::cout << "SecondOrderTermsCalculator object created" << std::endl;
}

arma::cx_vec SecondOrderTermsCalculator::getRightHandSide_Directly(SystemState const &state) const {
    update_ampAmp_nonlin_matrices(state);

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        tempRightHandSideRetVec(index) = arma::dot(state.get_mode_amps(), ampAmp_nonlin_matrices.at(index) * state.get_mode_amps());
                                        + state.get_R_velocity() * std::pow(state.get_R(), -2.0) * arma::dot(state.get_mode_vels(), secondOrderCoefs.get_Z(index) * state.get_mode_amps());
                                        + std::pow(state.get_R(), -1.0) * arma::dot(state.get_mode_vels(), secondOrderCoefs.get_D(index) * state.get_mode_vels());
    }
    return tempRightHandSideRetVec;
}

arma::cx_vec SecondOrderTermsCalculator::getRightHandSide_Directly_NoRadiusVelocity(SystemState const &state) const {
    update_ampAmp_nonlin_matrices_NoRadiusVelocity(state);

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        tempRightHandSideRetVec(index) = arma::dot(state.get_mode_amps(), ampAmp_nonlin_matrices.at(index) * state.get_mode_amps());
                                        + std::pow(state.get_R(), -1.0) * arma::dot(state.get_mode_vels(), secondOrderCoefs.get_D(index) * state.get_mode_vels());
    }
    return tempRightHandSideRetVec;
}

void SecondOrderTermsCalculator::update_ampAmp_nonlin_matrices(SystemState const &state) const {
    update_last_C_multiplier_and_AmpAmp_matrices(state);
    update_last_K_multiplier_and_AmpAmp_matrices(state);
    update_last_X_multiplier_and_AmpAmp_matrices(state);
}

void SecondOrderTermsCalculator::update_ampAmp_nonlin_matrices_NoRadiusVelocity(SystemState const &state) const {
    update_last_C_multiplier_and_AmpAmp_matrices(state);
    update_last_K_multiplier_and_AmpAmp_matrices(state);
}

void SecondOrderTermsCalculator::update_last_C_multiplier_and_AmpAmp_matrices(SystemState const &state) const {
    double const new_C_multiplier = get_C_multiplier(state);
    if(!has_multiplier_changed(new_C_multiplier, last_C_multiplier)){ return; }

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        ampAmp_nonlin_matrices[index] += (new_C_multiplier - last_C_multiplier) * secondOrderCoefs.get_C(index);    
    }
    last_C_multiplier = new_C_multiplier;
}

void SecondOrderTermsCalculator::update_last_K_multiplier_and_AmpAmp_matrices(SystemState const &state) const {
    double const new_K_multiplier = get_K_multiplier(state);
    if(!has_multiplier_changed(new_K_multiplier, last_K_multiplier)){ return; }

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        ampAmp_nonlin_matrices[index] += (new_K_multiplier - last_K_multiplier) * secondOrderCoefs.get_K(index);    
    }
    last_K_multiplier = new_K_multiplier;
}

void SecondOrderTermsCalculator::update_last_X_multiplier_and_AmpAmp_matrices(SystemState const &state) const {
    double const new_X_multiplier = get_X_multiplier(state);
    if(!has_multiplier_changed(new_X_multiplier, last_X_multiplier)){ return; }

    #pragma omp parallel for
    for(int index = 0; index < num_of_modes; ++index){
        ampAmp_nonlin_matrices[index] += (new_X_multiplier - last_X_multiplier) * secondOrderCoefs.get_X(index);    
    }
    last_X_multiplier = new_X_multiplier;
}