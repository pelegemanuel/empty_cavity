#ifndef FIRST_ORDER_H
#define FIRST_ORDER_H

#include <armadillo>
#include "wigner_coefs.hpp"
#include "aid_arma_templates.hpp"

class FirstOrderCoefficients{
    private:
        int const num_of_modes;
        double const alpha;

        arma::vec const acceleration_omega;
        arma::vec const surface_omega;

        inline static double acceleration_omega_func(int const l){ return double(l - 1); }

    public:
        FirstOrderCoefficients(int const init_num_of_modes, double const init_alpha)
        : num_of_modes(init_num_of_modes), 
        alpha(init_alpha),
        acceleration_omega(aid::vector_of_l_func(num_of_modes, &FirstOrderCoefficients::acceleration_omega_func)),
        surface_omega(-alpha * aid::vector_of_l_func(num_of_modes, &coefs::q_coef)) {}

        inline int get_num_of_modes() const { return num_of_modes; }

        inline arma::vec get_A(double const acceleration, double const R) const {
            return acceleration_omega * (acceleration / R) + surface_omega * std::pow(R, -3.0);
        }

        inline double get_A_at_index(double const acceleration, double const R, int const index) const {
            return acceleration_omega.at(index) * (acceleration / R) + surface_omega.at(index) * std::pow(R, -3.0);
        }

        inline double get_max_A(double const acceleration, double const R) const {
            return std::max(0.0, get_A_at_index(acceleration, R, num_of_modes - 1));
        }
};

#endif