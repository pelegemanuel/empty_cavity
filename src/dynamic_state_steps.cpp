#include "dynamic_state.hpp"
#include "stepsTemplateAid.hpp"

void SystemState::step_euler(double const dt){
    arma::cx_vec const mode_acceleration = SystemState::get_mode_acceleration();
    time = time + dt;
    R = R + dt * R_velocity;
    R_velocity = R_velocity + dt * R_acceleration;
    mode_amps = mode_amps + dt * mode_vels;
    mode_vels =  mode_vels + dt * mode_acceleration;
}

void SystemState::step_half_implicit_inner_recursive(double const dt, int const recursion_depth, double const artificial_acceleration){
    if(recursion_depth > SystemState::MAX_STEP_RECURSION_DEPTH){
        std::cout << "Oh No! Max step recursion depth reached. Carrying Euler" << std::endl;
        SystemState::step_euler(dt);
        return;
    }

    double const minimal_bottom_value = getDivisorForImplicitScheme(dt, get_max_A(), get_Rvel_over_R());
    if(minimal_bottom_value < SystemState::MIN_DIVISION_VAL_FOR_IMPLICIT_SCHEME){
        SystemState::step_half_implicit_inner_recursive(0.5 * dt, recursion_depth + 1);
        SystemState::step_half_implicit_inner_recursive(0.5 * dt, recursion_depth + 1);
    } else {
        double const R_acceleration_init = R_acceleration;
        arma::cx_vec const mode_vels_old = mode_vels;
        double const R_velocity_init = R_velocity;
        arma::vec const mode_vels_bottom = getDivisorForImplicitScheme(dt, get_A(), get_Rvel_over_R());
        
        mode_vels = (mode_vels % getVelocityMultiplierForImplicitScheme(mode_vels_bottom)
                    + dt * SystemState::get_A() % mode_amps 
                    + dt * nonlinPertTerms.getRightHandSideByExtrapolation(time + 0.5 * dt)) 
                    / mode_vels_bottom;
        mode_amps = mode_amps + 0.5 * dt * (mode_vels + mode_vels_old);

        update_R_acceleration(artificial_acceleration); //after perturbation change
        R_velocity = R_velocity + dt * 0.5 * (R_acceleration_init + R_acceleration);
        R = R + dt * 0.5 * (R_velocity_init + R_velocity);
        time = time + dt;
    }
}