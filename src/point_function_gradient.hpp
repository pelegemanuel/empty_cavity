#ifndef POINT_FUNCTION_GRADIENT_H
#define POINT_FUNCTION_GRADIENT_H

#include <armadillo>
#include "Point.hpp"

static double constexpr DELTA = std::pow(10.0, -7.0);

template<typename armaNumberType>
static arma::Col<armaNumberType> gradient(Point const &point, armaNumberType (*f)(Point const &)){
    armaNumberType const grad_x = (f(point.nudged_a_bit(DELTA, 0.0, 0.0)) - f(point.nudged_a_bit(-DELTA, 0.0, 0.0))) / (2.0 * DELTA);
    armaNumberType const grad_y = (f(point.nudged_a_bit(0.0, DELTA, 0.0)) - f(point.nudged_a_bit(0.0, -DELTA, 0.0))) / (2.0 * DELTA);
    armaNumberType const grad_z = (f(point.nudged_a_bit(0.0, 0.0, DELTA)) - f(point.nudged_a_bit(0.0, 0.0, -DELTA))) / (2.0 * DELTA);
    return aidsph::entries_to_vector(grad_x, grad_y, grad_z);
}

template<class MyObject, typename armaNumberType>
static arma::Col<armaNumberType> member_function_gradient(Point const &point, armaNumberType (MyObject::*f)(Point const &) const, MyObject const &object){
    armaNumberType const grad_x = ((object.*f)(point.nudged_a_bit(DELTA, 0.0, 0.0)) - (object.*f)(point.nudged_a_bit(-DELTA, 0.0, 0.0))) / (2.0 * DELTA);
    armaNumberType const grad_y = ((object.*f)(point.nudged_a_bit(0.0, DELTA, 0.0)) - (object.*f)(point.nudged_a_bit(0.0, -DELTA, 0.0))) / (2.0 * DELTA);
    armaNumberType const grad_z = ((object.*f)(point.nudged_a_bit(0.0, 0.0, DELTA)) - (object.*f)(point.nudged_a_bit(0.0, 0.0, -DELTA))) / (2.0 * DELTA);
    return aidsph::entries_to_vector(grad_x, grad_y, grad_z);
}

template<class MyObject, typename armaNumberType>
static arma::Col<armaNumberType> class_outer_function_gradient(Point const &point, armaNumberType (*f)(MyObject const &, Point const &), MyObject const &object){
    armaNumberType const grad_x = (f(object, point.nudged_a_bit(DELTA, 0.0, 0.0)) - f(object, point.nudged_a_bit(-DELTA, 0.0, 0.0))) / (2.0 * DELTA);
    armaNumberType const grad_y = (f(object, point.nudged_a_bit(0.0, DELTA, 0.0)) - f(object, point.nudged_a_bit(0.0, -DELTA, 0.0))) / (2.0 * DELTA);
    armaNumberType const grad_z = (f(object, point.nudged_a_bit(0.0, 0.0, DELTA)) - f(object, point.nudged_a_bit(0.0, 0.0, -DELTA))) / (2.0 * DELTA);
    return aidsph::entries_to_vector(grad_x, grad_y, grad_z);
}

#endif