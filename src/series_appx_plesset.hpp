#ifndef SAP_H
#define SAP_H

#include "series_appx_general.hpp"

namespace seriesappx
{
    //returns series for (R/R0)^p
    inline arma::vec R_over_R0_series(const int max_order, const double power = 1.0){ return one_minus_t_series(max_order, power * 0.4); } // since R~(1-t/t0)^(2/5)

    //return series for (t0 * Rdot / R)^p 
    inline arma::vec Rdot_t0_over_R_series(const int max_order, const double power = 1.0){ return std::pow(-0.4, power) * one_minus_t_series(max_order, -power); } // since Rdot / R = -(2/5) * (1-t/t0)^(-1)

    //return perturbation series for modes with l=1
    arma::cx_vec a_1_series_first_order(const int max_order, const arma::cx_double a0, const arma::cx_double adot0, const double t0);

    //return perturbation series for modes with l other than 1
    arma::cx_vec a_non1_series_first_order(const int max_order, const arma::cx_double a0, const arma::cx_double adot0, const double t0, const int l);

    //returns a matrix containing series perturbations for a, up to order 4, in the format idx X order
    arma::cx_mat a_series_first_order(const int max_order, const arma::cx_vec &a0, const arma::cx_vec &adot0, const double t0, const int max_mode);

    //matrix of the l.h.s perturbation equations
    arma::mat a_first_order_perturbation_equation_matrix(const int max_order, const double t0, const int l);

    //recieves the perturbation series for the first order equations via the matrix formalism.
    arma::cx_vec first_order_matrix_solution(const int max_order, const arma::cx_double a0, const arma::cx_double adot0, const double t0, const int l);

    //solves for coeficients with given non homogenic terms in the right hand side of the equation. solves up to the same order
    arma::cx_vec non_homogenic_order_matrix_solution(const arma::cx_vec &rhs, const arma::cx_double a0, const arma::cx_double adot0, const double t0, const int l);

    //returns components of the r.h.s
    arma::cx_mat get_rhs(const std::vector<arma::sp_cx_mat> &Q_vec, const std::vector<arma::sp_cx_mat> &Z_vec, const std::vector<arma::sp_cx_mat> &D_vec, const arma::cx_mat &a_coef_mat, const double t0, const double R0, const int max_mode);

    //recursive solution for a coeficients
    arma::cx_mat a_second_order_iterative_solution(const std::vector<arma::sp_cx_mat> &Q_vec, const std::vector<arma::sp_cx_mat> &Z_vec, const std::vector<arma::sp_cx_mat> &D_vec, 
        const arma::cx_vec &a0, const arma::cx_vec &adot0, const double t0, const double R0, const int max_order, const int max_depth, const double required_accuracy, const int max_mode);
}

#endif