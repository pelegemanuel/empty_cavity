#ifndef AID_ARMA_TEMPLATE_H
#define AID_ARMA_TEMPLATE_H

#include <armadillo>
#include "aid.hpp"
#include "aid_lm_indexing.hpp"

namespace aid{
    template<typename armaNumber>
    arma::Col<armaNumber> vector_of_l_func(int const num_of_modes, armaNumber (*f)(int)){
        arma::Col<armaNumber> func_vector(num_of_modes, arma::fill::zeros);

        #pragma omp parallel for
        for(int mode_index = 0; mode_index < num_of_modes; ++mode_index){
            func_vector(mode_index) = f(aid::l_from_index(mode_index));
        }
        return func_vector;
    }

    template<typename armaNumber>
    void sum_quantity_for_l(arma::Col<armaNumber> const &original_vec, arma::Col<armaNumber> &summed_vec){
        int const num_of_modes = original_vec.n_rows;
        int const l_max = aid::maximal_l(num_of_modes);
        summed_vec.fill(0.0);

        #pragma omp parallel for
        for(int l = 1; l <= l_max; ++l){
            for(int mode_index = aid::min_index_for_l(l); mode_index <= aid::effective_max_index_for_l(l, num_of_modes); ++mode_index){
                summed_vec(l - 1) += original_vec(mode_index);
            }
        }
    }

    template<typename armaNumber>
    void zero_vec_to_index(arma::Col<armaNumber> &vec, int const first_nonzero_index){
        #pragma omp parallel for
        for(int mode_index = 0; mode_index < first_nonzero_index; ++mode_index){
            vec(mode_index) = 0.0;
        }
    }

    template<typename ArmaDataForm> ArmaDataForm reduce_arma(ArmaDataForm const &data){
        int const num_of_rows = data.n_rows;
        ArmaDataForm data_reduced(data.n_rows, data.n_cols, arma::fill::zeros);
        #pragma omp parallel for
        for(int row_index = 0; row_index < num_of_rows; ++row_index){
            data_reduced.row(row_index) = data.row(row_index) / aid::mode_scaling(aid::l_from_index(row_index));
        }
        return data_reduced;
    }

    template<typename ArmaDataForm> void limit_arma_vector_entries(ArmaDataForm &vector, double const limit){
        #pragma omp parallel for
        for(int index = 0; index < vector.n_rows; ++index){
            if(std::abs(vector(index)) > limit) {
                vector(index) = limit * vector(index) / (std::abs(vector(index)) + EPS);
            }
        }
    }

    template<typename ArmaDataForm>
    ArmaDataForm normal_random_vector_with_limited_std(int const num_of_entries, double const max_num_of_std){
        arma::arma_rng::set_seed_random();
        ArmaDataForm vector(num_of_entries, arma::fill::randn);
        limit_arma_vector_entries(vector, max_num_of_std);
        return vector;
    }

    inline arma::vec uniform_random_vector(int const num_of_entries, double const minimal_val, double const maximal_val){
        arma::arma_rng::set_seed_random();
        arma::vec result_vec(num_of_entries, arma::fill::randu);
        return minimal_val + (maximal_val - minimal_val) * result_vec;
    }

    template<typename ArmaDataForm>
    inline double relative_norm_difference(ArmaDataForm const &object1, ArmaDataForm const &object2){ 
        return 2.0 * arma::norm(object2 - object1) / (arma::norm(object1) + arma::norm(object2) + aid::EPS);
    }

    template<typename armaNumber>
    void printVectorOfL(arma::Col<armaNumber> const &vector){
        for(int l = 1; l <= aid::maximal_l(vector.n_rows); ++l){
            std::cout 
                    << "mode l=" << l << ": " 
                    << vector.at(aid::min_index_for_l(l)) 
                    << std::endl;
        }
    }
}

#endif