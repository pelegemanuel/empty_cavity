#include "dynamic_state.hpp"
#include "initial_conditions.hpp"
#include "wigner_coefs.hpp"
#include "aid_lm_indexing.hpp"
#include "aid_arma_templates.hpp"
#include <cassert>

SystemState::SystemState(int const init_num_of_modes,
                        PhysicalAttributes const &initPhysicalAttributes,
                        InitialConditionsData const &initCondData) :
num_of_modes(init_num_of_modes),
physicalAttributes(initPhysicalAttributes),
firstOrderCoeffs(num_of_modes, physicalAttributes.get_alpha()),
nonlinPertTerms(num_of_modes),
radiusCoefs(num_of_modes),
time(0.0), 
R(initCondData.get_R0()), 
R_velocity(initCondData.get_R_velocity_0()),
mode_amps(num_of_modes, arma::fill::zeros), 
mode_vels(num_of_modes, arma::fill::zeros),
R_acceleration(0.0),
shouldKeepRadiusConstant(false),
aid_zeros(num_of_modes, arma::fill::zeros)
{
    update_R_acceleration();
    auto initial_conditions_pair = initcond::get_initial_perturbations(initCondData.get_initial_perturbation_type_key(), *this);
    mode_amps = initial_conditions_pair.first;
    mode_vels = initial_conditions_pair.second;

    int const minimal_l = initCondData.get_minimal_l();
    int const first_non_zero_index = aid::index_from_lm(minimal_l, -minimal_l);
    aid::zero_vec_to_index(mode_amps, first_non_zero_index);
    aid::zero_vec_to_index(mode_vels, first_non_zero_index);

    mode_amps = initCondData.get_sig() * aid::reduce_arma(mode_amps);
    mode_vels = initCondData.get_sig() * aid::reduce_arma(mode_vels);
    
    set_prerun_physical_quantities();
    std::cout << "SystemState object created" << std::endl;
}

void SystemState::setRadiusConstant(){
    shouldKeepRadiusConstant = true;
    nonlinPertTerms.set_shouldConsiderRadiusVelocity(false);
    R_velocity = 0.0;
}

void SystemState::setRadiusMoving(){
    shouldKeepRadiusConstant = false;
    nonlinPertTerms.set_shouldConsiderRadiusVelocity(true);
}
    