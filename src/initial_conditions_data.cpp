#include "initial_conditions_data.hpp"
#include <iostream>

//collapse constructor
InitialConditionsData::InitialConditionsData(int const init_initial_perturbation_type_key, 
                                             double const init_sig, 
                                             int const init_minimal_l) :
initial_perturbation_type_key(init_initial_perturbation_type_key),
sig(init_sig),
R0(1.0),
R_velocity_0(-1.0),
minimal_l(init_minimal_l)
{
    std::cout << "InitialConditionsData object created - collapse mode" << std::endl;
}

//stable constructor
InitialConditionsData::InitialConditionsData(int const init_initial_perturbation_type_key, 
                                            double const init_sig, 
                                            PhysicalAttributes const &physAttributes,
                                            double const R_velocity_init,
                                            int const init_minimal_l) :
initial_perturbation_type_key(init_initial_perturbation_type_key),
sig(init_sig),
R0(physAttributes.get_R0_equilibrium()),
R_velocity_0(R_velocity_init),
minimal_l(init_minimal_l)
{
    std::cout << "InitialConditionsData object created - stable mode" << std::endl;
}