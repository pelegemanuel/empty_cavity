#ifndef PARTICLE_H
#define PARTICLE_H

#include "Point.hpp"

class Particle{
    private:
        Point location;
        Point velocity;

    public:
        Particle(Point const &location, Point const &velocity) : location(location), velocity(velocity) {}
        Particle(Point const &location) : location(location), velocity() {}
        Particle(double const radius) : location(radius), velocity() {}
        Particle(double const radius, double const initial_velocity) : location(radius), velocity(initial_velocity) {}
        Particle() : location(), velocity() {}

        inline Point get_location() const { return location; }
        inline Point get_velocity() const { return velocity; }

        inline void step_euler(double const dt, arma::vec const acceleration){
            location.move(dt * velocity.get_coordinates());
            velocity.move(dt * acceleration);
        }
};

#endif