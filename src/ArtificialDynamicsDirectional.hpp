#ifndef ARTIFICIAL_DYNAMICS_DIRECTIONAL_H
#define ARTIFICIAL_DYNAMICS_DIRECTIONAL_H

#include <armadillo>
#include "ArtificialDynamics.hpp"
#include "dynamic_state.hpp"
#include "aid_arma_templates.hpp"
#include "aid.hpp"

inline arma::cx_vec get_mode_directions(arma::cx_vec const &input_mode_velocities){
    return input_mode_velocities / (arma::abs(input_mode_velocities) + aid::EPS);
}

class ArtificialDynamicsDirectional: public ArtificialDynamics
{
    private:
        arma::cx_vec const modeVelocityCoeffs;

    public:
        ArtificialDynamicsDirectional(ArtificialDynamicsDirectional const &origin)
        : ArtificialDynamics(origin.get_num_of_modes()), 
        modeVelocityCoeffs(origin.get_modeVelocityCoeffs()) {}

        ArtificialDynamicsDirectional(arma::cx_vec const &init_modeVelocityCoeffs)
        : ArtificialDynamics(init_modeVelocityCoeffs.n_rows), 
        modeVelocityCoeffs(init_modeVelocityCoeffs) {}

        inline arma::cx_vec pumpAndDissipateModeVelocities(SystemState const &state, double const dt) const {
            return state.get_mode_vels() + dt * get_mode_directions(state.get_mode_vels()) % modeVelocityCoeffs;
        }
        inline double pumpAndDissipateRadiusVelocity(double const input_R_velocity, double const dt) const {
             return input_R_velocity; 
        }

        inline void printData() const {
            std::cout << "Directional pumping / dissipation coefs: " << std::endl;
            aid::printVectorOfL(modeVelocityCoeffs);
        }

        inline arma::cx_vec get_modeVelocityCoeffs() const { return modeVelocityCoeffs; }
};

#endif