#ifndef OUTPUT_FILENAMES_H
#define OUTPUT_FILENAMES_H

#include <string>

namespace filenames{
    static std::string fin = ".csv";
    static std::string mode_amps = "perturbationAmplitudes" + fin;
    static std::string mode_vels = "perturbationVelocities" + fin;
    static std::string sparse_saves = "sparseSaves" + fin;
    static std::string dense_saves = "denseSaves" + fin;
    static std::string elevation_squared_avg = "elevationSquaredAvg" + fin;
    static std::string l_number = "modeAngmomentum" + fin;
}

#endif