#include "mainOptions.hpp"

int main(int argc, char *argv[]){
    if(argc >= 6){
        std::cout << "Running full non-linear simmulation" << std::endl;
        simmulation_runner(argv);
    } else if(argc >= 5) {
        std::cout << "Tracking a single mode on the Hunter Bubble (Self Similar)" << std::endl;
        self_similar_runner(argv);
    } else {
        std::cout << "Running checks" << std::endl;
        checks_runner();
    }
    return 0;
}