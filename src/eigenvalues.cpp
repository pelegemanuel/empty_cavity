#include "eigenvalues.hpp"
#include <iostream>
#include <time.h>
#include <string>
#include <cassert>
#include "aid.hpp"
#include "aid_lm_indexing.hpp"
#include "std_vector_aid.hpp"
#include "matrix_constructor.hpp"

//direct computation of eigenvalues from the full gaunt matrix
std::vector<double> direct_eigenvalues(int const l_base, int const m_base, int const l_max, bool const shouldReduce, bool const useSparse = false){
    assert(l_base > 0);
    assert(l_max > 0);
    assert(std::abs(m_base) <= l_base);

    int const num_of_modes = aid::mode_num(l_max);
    std::vector<double> eigenval_vec; //std vector to be returned
    arma::vec eigenvals; //arma vector for output
    arma::mat A = total_gaunt_mat(num_of_modes, l_base, m_base, shouldReduce);

    if (useSparse){
        arma::sp_mat Asp = arma::sp_mat(A);
        arma::eigs_sym(eigenvals, Asp, 100);
    } else {
        arma::eig_sym(eigenvals, A);
    }

    eigenval_vec.insert(eigenval_vec.end(), eigenvals.begin(), eigenvals.end());
    stdVecAid::clean_and_sort(eigenval_vec);
    return eigenval_vec;
}

//direct computation of eigenvalues& eigenvectors from the full gaunt matrix
std::pair<std::vector<arma::vec>,std::vector<double>> direct_eigenvectors(int const l_base, int const m_base, int const l_max, bool const shouldReduce, double const lower_bound){
    assert(l_base > 0);
    assert(l_max > 0);
    assert(std::abs(m_base) <= l_base);

    int const num_of_modes = aid::mode_num(l_max);
    std::vector<arma::vec> eigenvec_vec; //values to be returned
    std::vector<double> eigenval_vec;

    arma::vec eigenvals;
    arma::mat eigenvectors;
    arma::mat A = total_gaunt_mat(num_of_modes, l_base, m_base, shouldReduce);
    arma::eig_sym(eigenvals, eigenvectors, A);

    // taking only significant terms 
    for (int eig_index = 0; eig_index < num_of_modes; ++eig_index){
        if (std::abs(eigenvals(eig_index)) > lower_bound){
            eigenvec_vec.push_back(eigenvectors.col(eig_index));
            eigenval_vec.push_back(eigenvals(eig_index));
        }
    }
    return std::pair<std::vector<arma::vec>,std::vector<double>>(eigenvec_vec, eigenval_vec);
}

//Computation of the guant eigenvalues using blockwise seperation. For full algorithm see gaunt_spectrum.lyx
//Notations from the file (such as CtC) are used
std::vector<double> block_wise_eigenvalues(int const l_base, int const m_base, int const l_max, bool const shouldReduce){
    assert(l_base > 0);
    assert(l_max > 0);
    assert(std::abs(m_base) <= l_base);

    arma::vec block_eigenvalues;
    std::vector<double> eigenval_vec;

    for(int m = -l_max; m <= l_max; ++m){
        int m1 = m;
        int m2 = -m_base - m1;

        if (m1 == m2){
            arma::mat C_mat = get_symmetric_C_mat(l_base, m_base, l_max, shouldReduce);
            arma::eig_sym(block_eigenvalues, C_mat);
            eigenval_vec.insert(eigenval_vec.end(), block_eigenvalues.begin(), block_eigenvalues.end());
        } else if(abs(m1) > abs(m2) || ((abs(m1) == abs(m2) && m1 > m2))){ //avoiding repetitions of the same (m1,m2)
            for(int parity_index = 0; parity_index <=1 ; ++parity_index){
                arma::mat C_mat = get_Ci_matrix(l_base, m_base, m1, m2, l_max, parity_index, shouldReduce);
                arma::mat CtC = C_mat.t() * C_mat;

                //The following steps are explained in gaunt_spectrum.lyx
                //CtC is semi-positive definite, negative values are ~~0, so we can take absolute value
                //CtC has the square eigenvalues of the original matrix.
                arma::eig_sym(block_eigenvalues, CtC); 
                block_eigenvalues = arma::sqrt(abs(block_eigenvalues));
                eigenval_vec.insert(eigenval_vec.end(), block_eigenvalues.begin(), block_eigenvalues.end());
                block_eigenvalues = -block_eigenvalues;
                eigenval_vec.insert(eigenval_vec.end(), block_eigenvalues.begin(), block_eigenvalues.end());
            }
        }   
    }

    stdVecAid::clean_and_sort(eigenval_vec);
    return eigenval_vec;
}

//turns of eigenvectors of the CtC mat (represented in that basis) to that of the full mat
//full_vec is filled with values of base_vec
void fill_Ci_eigenvector(arma::vec &full_vec, arma::vec const &base_vec, int const base_l, int const m){
    for (int vec_index = 0; vec_index < base_vec.size(); ++vec_index){
        int const l = base_l + 2 * vec_index;
        int const full_vec_index = aid::index_from_lm(l, m);
        full_vec(full_vec_index) = base_vec(vec_index);
    }   
}

//turns eigenvalues of B mat: Block(0,C,Ct,0) to the full basis
// v is an eigenvector of CtC, and Cv_lam = C*v / lambda with lambda its eigenvalue
arma::vec Ci_eigvec(int const mode_num, arma::vec const &v, arma::vec const &Cv_lam, int const l_v_base, int const m_v, int const l_Cv_base, int const m_Cv){
    arma::vec full_vec(mode_num);
    full_vec.zeros();
    fill_Ci_eigenvector(full_vec, v, l_v_base, m_v);
    fill_Ci_eigenvector(full_vec, Cv_lam, l_Cv_base, m_Cv);
    return full_vec / aid::SQRT_OF_2;
}

//turns of the eigenvectors of the symmetric C mat (represented in that basis) to that of the full mat
//full vec is filled with values of base_vec
void fill_Csym_eigenvector(arma::vec &full_vec, arma::vec const &base_vec, int const m){
    int l_min = std::max(std::abs(m), 1); //ignoring the (0,0) mode
    int l, full_vec_index;
    for (int vec_index = 0; vec_index < base_vec.size(); ++vec_index){
        l = l_min + vec_index;
        full_vec_index = aid::index_from_lm(l, m);
        full_vec(full_vec_index) = base_vec(vec_index);
    }
}

//Computation of the guant eigenvectors and eigenvalues using blockwise seperation. For full algorithm see gaunt_spectrum.lyx
//Notations from the file (such as CtC) are used
std::pair<std::vector<arma::vec>,std::vector<double>> block_wise_eigenvectors(int const l_base, int const m_base, int const l_max, bool const shouldReduce, double const lower_bound){
    assert(l_base > 0);
    assert(l_max > 0);
    assert(std::abs(m_base) <= l_base);

    arma::vec block_eigenvalues;
    arma::mat block_eigenvectors;
    std::vector<double> eigenval_vec;
    std::vector<arma::vec> eigenvec_vec;

    int const num_of_modes = aid::mode_num(l_max);

    for(int m = -l_max; m <=l_max; ++m){
        int const m1 = m;
        int const m2 = -m_base - m1;

        if (m1 == m2){
            arma::mat C_mat = get_symmetric_C_mat(l_base, m_base, l_max, shouldReduce);
            arma::eig_sym(block_eigenvalues, block_eigenvectors, C_mat);

            for (int eig_index = 0; eig_index < block_eigenvalues.size(); ++eig_index){
                 if(std::abs(block_eigenvalues(eig_index)) > lower_bound){ //taking only significant eigenvalues
                     arma::vec full_eigenvector(num_of_modes);
                     full_eigenvector.zeros();
                     arma::vec base_eigenvector = block_eigenvectors.col(eig_index);
                     base_eigenvector.clean(aid::EPS);
                     fill_Csym_eigenvector(full_eigenvector,base_eigenvector,m1);
                     eigenvec_vec.push_back(full_eigenvector);
                     eigenval_vec.push_back(block_eigenvalues(eig_index));
                 }
            }
        } 
        
        else if(abs(m1) > abs(m2) || ((abs(m1) == abs(m2) && m1 > m2))){ //avoiding repetitions of the same (m1,m2)
            for(int parity_index = 0; parity_index <=1; ++parity_index){
                std::pair<int,int> base_indices;
                arma::mat C_mat = get_Ci_matrix(l_base, m_base, m1, m2, l_max, parity_index, shouldReduce, base_indices);
                arma::mat CtC = C_mat.t() * C_mat;
                int l1_min = base_indices.first;
                int l2_min = base_indices.second;

                //The following steps are explained in gaunt_spectrum.lyx
                //CtC is semi-positive definite, negative values are ~~0, so we can take absolute value
                //CtC has the square eigenvalues of the original matrix.

                arma::eig_sym(block_eigenvalues, block_eigenvectors, CtC); 
                block_eigenvalues = arma::sqrt(abs(block_eigenvalues)); //eigenvalues of full matrix and not CtC
                for (int eig_index = 0; eig_index < block_eigenvalues.size(); ++eig_index){
                    if(block_eigenvalues(eig_index) > lower_bound){ //only non-zero eigenvalues
                        //v and Cv/lambda vecs in C subspace (see gaunt_spectrum.lyx)
                        arma::vec base_eigenvector = block_eigenvectors.col(eig_index);
                        arma::vec C_mul_base_eigenvector = C_mat * base_eigenvector / block_eigenvalues(eig_index);
                        base_eigenvector.clean(aid::EPS);
                        C_mul_base_eigenvector.clean(aid::EPS);

                        //vector with the positive eigenvalue
                        arma::vec full_eigenvector_pos = Ci_eigvec(num_of_modes, base_eigenvector, C_mul_base_eigenvector, l2_min, m2, l1_min, m1);
                        eigenvec_vec.push_back(full_eigenvector_pos);
                        eigenval_vec.push_back(block_eigenvalues(eig_index));

                        //vector with the negative eigenvalue
                        arma::vec full_eigenvector_neg = Ci_eigvec(num_of_modes, base_eigenvector, -C_mul_base_eigenvector, l2_min, m2, l1_min, m1);
                        eigenvec_vec.push_back(full_eigenvector_neg);
                        eigenval_vec.push_back(-block_eigenvalues(eig_index));
                    }
                }
            }
        }   
    }

    return std::pair<std::vector<arma::vec>,std::vector<double>>(eigenvec_vec, eigenval_vec);
}

//standard deviation for uncorrelated modes
double uncorrelated_std(int const l, int const m, int const l_max){
    assert(l > 0);
    assert(l_max > 0);
    assert(std::abs(m) <= l);

    double std = 0;
    std::vector<double> const eigenvals = block_wise_eigenvalues(l, m, l_max, true);
    for (auto const &eig :eigenvals){
        std += 0.5 * eig * eig;
    }
    return std;
}

//returns maximal eigenvalue for mode (l,0) in absolute value
//assumes given vector is ordered
std::vector<double> maximal_eigenvalue(int const l_max, int const l_max_tensor, bool const shouldReduce){
    assert(l_max > 0);
    assert(l_max_tensor > 0);

    std::vector<double> max_eigenval_vec(l_max);
    std::vector<double> temp_eigenval_vec;
    double current_max_eigenval = 0;
    for(int l = 1; l <= l_max; ++l){
        temp_eigenval_vec = block_wise_eigenvalues(l, 0, l_max_tensor, shouldReduce);
        //as the eigenval vector is ordered, the maximum (of one of the values) must be on one of the edges
        current_max_eigenval = std::max(std::abs(temp_eigenval_vec.front()), std::abs(temp_eigenval_vec.back()));
        max_eigenval_vec.at(l - 1) = current_max_eigenval;
    }
    return max_eigenval_vec;
}

//timing of 3 methods for computation of eigenvalues
void method_comparison(int const l_base, int const m_base, int const l_max, bool const shouldPrint){
    assert(l_max > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    clock_t direct_clock = clock();
    std::vector<double> direct = direct_eigenvalues(l_base, m_base, l_max, false);
    direct_clock = clock() - direct_clock;

    clock_t sparse_clock = clock();
    std::vector<double> sparse_direct = direct_eigenvalues(l_base, m_base, l_max, false, true);
    sparse_clock = clock() - sparse_clock;

    clock_t blockwise_clock = clock();
    std::vector<double> blockwise = block_wise_eigenvalues(l_base, m_base, l_max, false);
    blockwise_clock = clock() - blockwise_clock;
    
    std::cout << "direct computation - " << direct.size() << std::endl;
    std::cout << "regular time: " << direct_clock << std::endl;
    std::cout << "sparse time: " << sparse_clock;
    if (shouldPrint){stdVecAid::print_vector(direct);}
    std::cout<<"\n ### \n";
    std::cout << "blockwise computation - " << blockwise.size() << std::endl;
    std::cout << "time: " << blockwise_clock << std::endl; 
    if (shouldPrint){stdVecAid::print_vector(blockwise);}
}

//save give mode eigenvalues to file
void spectrum_to_file(int const l_base, int const m_base, int const l_max, bool const shouldReduce){
    assert(l_max > 0);
    assert(l_base > 0);
    assert(std::abs(m_base) <= l_base);

    std::string file_path = "/mnt/c/Users/peleg/Documents/turbfftw/sh_eignevalues/results/";
    int const num_of_modes = aid::mode_num(l_max);

    std::vector<double> eigenvals = block_wise_eigenvalues(l_base, m_base, l_max, shouldReduce);
    std::cout << "#Zero eigenvalues: " << num_of_modes - eigenvals.size() << std::endl;

    std::stringstream ss;
    ss << file_path << l_base << "_" << m_base << "_" << l_max << "_" << shouldReduce << ".txt";
    std::string filename = ss.str();

    stdVecAid::vector_to_file(eigenvals, filename);
}