#include "time_manager_collapse.hpp"
#include "collapse_time_comp.hpp"
#include "aid_lm_indexing.hpp"

TimeManagerCollapse::TimeManagerCollapse(double const init_max_amp_to_R, 
                                         double const init_norm_amp_to_R, 
                                         double const init_dt_over_typical_time, 
                                         double const init_max_time,
                                         int const init_max_num_steps,
                                         SystemState const &initial_state) :
TimeManager(aid::maximal_l(initial_state.get_num_of_modes()), init_max_amp_to_R, init_norm_amp_to_R, init_dt_over_typical_time, init_max_time, init_max_num_steps),
theta_param(collapsetime::get_theta_param(initial_state.get_R(), initial_state.get_R_velocity(), initial_state.get_alpha())),
t_collapse_appx(collapsetime::get_collapse_time(theta_param, std::abs(initial_state.get_R() / initial_state.get_R_velocity()))),
t_final_appx(t_collapse_appx * (1.0 - std::pow(arma::max(arma::abs(initial_state.get_mode_amps())) / max_amp_to_R, 2.0)))
{
    std::cout << "Appx. collapse time: " << t_collapse_appx << std::endl;
}

void TimeManagerCollapse::finishPrintMessage(SystemState const &state) const {
    std::cout << step << " steps taken" << std::endl;
    std::cout << "t/tf [%]: " << 100.0 * state.get_time() / t_collapse_appx << std::endl;
    std::cout << "R: " << state.get_R() << std::endl;
}