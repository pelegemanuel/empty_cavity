#ifndef SECOND_ORDER_COEF_MULTIPLIERS_H
#define SECOND_ORDER_COEF_MULTIPLIERS_H

#include "dynamic_state.hpp"

inline double get_C_multiplier(SystemState const &state) { return state.get_alpha() * std::pow(state.get_R(), -4.0); }
inline double get_K_multiplier(SystemState const &state) { return state.get_R_acceleration() * std::pow(state.get_R(), -2.0); }
inline double get_X_multiplier(SystemState const &state) { return std::pow(state.get_R_velocity(), 2.0) * std::pow(state.get_R(), -3.0); }

#endif