#include <armadillo>
#include "NonLinearTermsExtrapolator.hpp"
#include "dynamic_state.hpp"

NonLinearTermsExtrapolator::NonLinearTermsExtrapolator(int const init_num_of_modes) : 
num_of_modes(init_num_of_modes),
shouldConsiderRadiusVelocity(true),
rightHandSideSaved(num_of_modes, RHS_INTERPOLATION_POINT_NUM, arma::fill::zeros),
rhsSaveTimes(RHS_INTERPOLATION_POINT_NUM, arma::fill::zeros),
secondOrderCalc(num_of_modes)
{
    std::cout << "NonLinearTermsExtrapolator object created" << std::endl;
}

void NonLinearTermsExtrapolator::set_initial_extrapolation_points(SystemState const &state){
    rightHandSideSaved.col(0) = secondOrderCalc.getRightHandSide_Directly(state);
    for(int rhs_saving_index = 0; rhs_saving_index < RHS_INTERPOLATION_POINT_NUM; ++rhs_saving_index){
        rhsSaveTimes(rhs_saving_index) = -1.0 * double(rhs_saving_index + 1); //negative times: assuming same rhs measurement before dynamics start
        rightHandSideSaved.col(rhs_saving_index) = rightHandSideSaved.col(0);
    }
}

void NonLinearTermsExtrapolator::update_rightHandSide(SystemState const &state){
    //step last measurements backward
    for(int rhs_saving_index = RHS_INTERPOLATION_POINT_NUM - 1; rhs_saving_index >= 1; --rhs_saving_index){
        rhsSaveTimes(rhs_saving_index) = rhsSaveTimes(rhs_saving_index - 1);
        rightHandSideSaved.col(rhs_saving_index) = rightHandSideSaved.col(rhs_saving_index - 1);
    }

    if(shouldConsiderRadiusVelocity){
        rightHandSideSaved.col(0) = secondOrderCalc.getRightHandSide_Directly(state);
    } else {
        rightHandSideSaved.col(0) = secondOrderCalc.getRightHandSide_Directly_NoRadiusVelocity(state);
    }
    rhsSaveTimes(0) = state.get_time();
}

arma::cx_vec NonLinearTermsExtrapolator::getRightHandSideByExtrapolation(double const current_time) const {

    arma::vec measurement_weight(RHS_INTERPOLATION_POINT_NUM, arma::fill::ones);
    for(int first_measure_index = 0; first_measure_index < RHS_INTERPOLATION_POINT_NUM; ++first_measure_index){
        for(int second_measure_index = 0; second_measure_index < first_measure_index; ++second_measure_index){
            measurement_weight(first_measure_index) *= (current_time - rhsSaveTimes(second_measure_index)) / (rhsSaveTimes(first_measure_index) - rhsSaveTimes(second_measure_index));
            measurement_weight(second_measure_index) *= (current_time - rhsSaveTimes(first_measure_index)) / (rhsSaveTimes(second_measure_index) - rhsSaveTimes(first_measure_index));
        }
    }

    arma::cx_vec rhs_by_extrapolation(num_of_modes, arma::fill::zeros);
    for(int measure_index = 0; measure_index < RHS_INTERPOLATION_POINT_NUM; ++measure_index){
        rhs_by_extrapolation += measurement_weight(measure_index) * rightHandSideSaved.col(measure_index);
    }
    return rhs_by_extrapolation;
}