#ifndef PARTICLE_FORCE_H
#define PARTICLE_FORCE_H

#include "Particle.hpp"
#include "aid_arma_templates.hpp"
#include "liquid_hydrodynamics.hpp"

class ParticleForce{
    public:
        ParticleForce() {}
        virtual arma::vec get_acceleration(Particle const &particle, SystemState const &state) const = 0;

};

class ParticleForceViscosity: public ParticleForce{
    private:
        double const viscosity_coef;
    public:
        ParticleForceViscosity(double const viscosity_coef) : ParticleForce(), viscosity_coef() {}
        inline double get_viscosity_coef() { return viscosity_coef; }

        inline arma::vec get_acceleration(Particle const &particle, SystemState const &state) const { 
            return viscosity_coef * (hydro::get_liquid_local_velocity_grad(state, particle.get_location()) 
                                    - particle.get_velocity().get_coordinates()); 
        }
};

class ParticleForceRandom: public ParticleForce{
    private:
        double const standard_deviation;
    public:
        double static constexpr STD_NUM_LIMIT = 4.0;
        ParticleForceRandom(double const standard_deviation) : ParticleForce(), standard_deviation(standard_deviation) {}
        inline double get_standard_deviation() { return standard_deviation; }

        inline arma::vec get_acceleration(Particle const &particle, SystemState const &state) const { 
            return standard_deviation * aid::normal_random_vector_with_limited_std<arma::vec>(3, STD_NUM_LIMIT);
        }
};

#endif