#ifndef OUTPUT_SPARSE_H
#define OUTPUT_SPARSE_H

#include "Output.hpp"
#include "OutputSparseManager.hpp"

class OutputSparse: public Output
{
    private:
        OutputSparseManager manager;
        std::ofstream mode_amps_sparse;
        std::ofstream mode_vels_sparse;
        std::ofstream sparse_save_file;
        std::ofstream average_elevation_save_file;

        double averaging_time;
        arma::vec elevation_sq_sum;

        void push_moment_data(SystemState const &state, bool const shouldPushSave);
        void push_averaging_data(SystemState const &state, double const dt, bool const shouldPushSave);

    public:
        OutputSparse(int const init_num_of_modes, 
                    std::string const &init_run_folder, 
                    int const max_steps_between_saves, 
                    double const max_time_between_saves);

        inline void push_data(SystemState const &state, int const step, double const dt){
            bool const shouldPushSave = manager.shouldSave(step, state.get_time());
            push_moment_data(state, shouldPushSave);
            push_averaging_data(state, dt, shouldPushSave);
        }

        inline void close_all(){
            mode_amps_sparse.close();
            mode_vels_sparse.close();
            sparse_save_file.close();
            average_elevation_save_file.close();
        }
};

#endif