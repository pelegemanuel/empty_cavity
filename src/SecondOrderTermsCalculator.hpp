#ifndef NONLIN_PERTURBATION_DYNAMICS_H
#define NONLIN_PERTURBATION_DYNAMICS_H

#include <armadillo>
#include "second_order_terms.hpp"
class SystemState;

class SecondOrderTermsCalculator
{
    private:
        int const num_of_modes;
        SecondOrder const secondOrderCoefs;

        arma::cx_vec mutable tempRightHandSideRetVec; //temporary for calculations
        std::vector<arma::sp_cx_mat> mutable ampAmp_nonlin_matrices;
        double mutable last_C_multiplier, last_K_multiplier, last_X_multiplier;

        inline double getMultiplierScale() const {
            return std::sqrt(std::pow(last_C_multiplier, 2.0) + std::pow(last_K_multiplier, 2.0) + std::pow(last_X_multiplier, 2.0));
        }
        
        inline bool has_multiplier_changed(double const value1, double const value2) const { 
            return (std::abs(value1 - value2) > RATIO_TO_CONSIDER_CHANGE * getMultiplierScale());
        }

        void update_ampAmp_nonlin_matrices(SystemState const &state) const ;
        void update_ampAmp_nonlin_matrices_NoRadiusVelocity(SystemState const &state) const ;
        void update_last_C_multiplier_and_AmpAmp_matrices(SystemState const &state) const ;
        void update_last_K_multiplier_and_AmpAmp_matrices(SystemState const &state) const ;
        void update_last_X_multiplier_and_AmpAmp_matrices(SystemState const &state) const ;

    public:
        static double constexpr RATIO_TO_CONSIDER_CHANGE = 0.01;
        SecondOrderTermsCalculator(int const init_num_of_modes);

        arma::cx_vec getRightHandSide_Directly(SystemState const &state) const ;
        arma::cx_vec getRightHandSide_Directly_NoRadiusVelocity(SystemState const &state) const;
};

#endif