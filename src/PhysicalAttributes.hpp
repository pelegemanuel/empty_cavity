#ifndef PHYS_ATTR
#define PHYS_ATTR

#include "aid.hpp"

class PhysicalAttributes{
    private:
        double const alpha;
        double const R0_equilibrium;
        double const P_out;
        double const P0;

    public:
        //oscillations constructor
        PhysicalAttributes(double const init_alpha, double const init_R0_equilibrium, double const init_P_out);

        //collapse (no pressure) constructor
        PhysicalAttributes(double const init_alpha);

        //copt constructor
        PhysicalAttributes(PhysicalAttributes const &initPhysicalAttributes);

        inline double get_alpha() const {return alpha; }
        inline double get_R0_equilibrium() const {return R0_equilibrium; }
        inline double get_P0() const {return P0; }
        inline double get_P_out() const {return P_out; } 

        inline double get_Xi_from_radius(const double R) const {return P_out - P0 * std::pow(R0_equilibrium / R, 3.0); }
        inline double get_Xi_from_volume(const double V) const {return P_out - P0 * get_V0() / V; }
        inline double get_V0() const {return (4.0 * aid::PI / 3.0) * std::pow(R0_equilibrium, 3.0); }

        inline void print_physical_parameters() const {
            std::cout << "P0 = " << P0 << " | P_out = " << P_out << " | alpha = " << alpha << " | R0 = " << R0_equilibrium << std::endl;
        }
};
#endif