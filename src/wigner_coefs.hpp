#ifndef WIGNER_MODIFIED 
#define WIGNER_MODIFIED

#include "wigner/gaunt.hpp"
#include "aid.hpp"

namespace coefs{

    inline double gaunt_coef(const int l1, const int l2, const int l3, const int m1, const int m2, const int m3){
        return wigner::gaunt<double>(l1,l2,l3,m1,m2,m3);
    }

    inline double reduced_gaunt_coef(const int l_base, const int l2, const int l3, const int m_base, const int m2, const int m3){
        return coefs::gaunt_coef(l_base, l2, l3, m_base, m2, m3) * aid::mode_scaling(l_base) / (aid::mode_scaling(l2) * aid::mode_scaling(l3));
    }

    inline double angular_velocity_coef(const int l_base, const int l1, const int l2){
        return 0.5 * double(l1 * (l1 + 1) + l2 * (l2 + 1) - l_base*(l_base + 1));
    }

    inline double angular_velocity_coef_renormed(const int l_base, const int l1, const int l2){
        return double(l_base + 1) * angular_velocity_coef(l_base, l1, l2) / double((l1 + 1) * (l2 + 1));
    }

    inline double g_coef(const int l_base, const int l1, const int l2){
        return double(l1 + 2) - coefs::angular_velocity_coef(l_base, l1, l2) / double(l1 + 1);
    }

    inline double q_coef(const int l){ return double((l + 2) * (l + 1) * (l - 1));}

    double gaunt_coef(const int l1, const int l2, const int l3, const int m1, const int m2, const int m3, const bool shouldReduce);

    double K_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce);
    double C_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce);
    double X_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce);
    double Z_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce);
    double D_mat_element(const int l, const int l1, const int l2, const int m, const int m1, const int m2, const bool shouldReduce);

    //get the overlap of four spherical harmonics
    double four_spherical_harmonics_overlap(const int l1, const int l2, const int l3, const int l4, const int m1, const int m2, const int m3, const int m4);

}

#endif