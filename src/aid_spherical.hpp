#ifndef AID_SPHERICAL_H
#define AID_SPHERICAL_H

#include <armadillo>
#include "aid.hpp"

namespace aidsph{
    static const int X_INDEX = 0;
    static const int Y_INDEX = 1;
    static const int Z_INDEX = 2;

    inline arma::vec x_hat() {arma::vec res(3, arma::fill::zeros); res(X_INDEX) = 1.0; return res;}
    inline arma::vec y_hat() {arma::vec res(3, arma::fill::zeros); res(Y_INDEX) = 1.0; return res;}
    inline arma::vec z_hat() {arma::vec res(3, arma::fill::zeros); res(Z_INDEX) = 1.0; return res;}
    inline arma::vec r_hat(double const theta, double const phi){ return std::cos(theta) * z_hat() + std::sin(theta) * (std::cos(phi) * x_hat() + std::sin(phi) * y_hat()); }
    inline arma::vec theta_hat(double const theta, double const phi){ return -std::sin(theta) * z_hat() + std::cos(theta) * (std::cos(phi) * x_hat() + std::sin(phi) * y_hat()); }
    inline arma::vec phi_hat(double const phi){ return -std::sin(phi) * x_hat() + std::cos(phi) * y_hat(); }

    inline bool are_orthogonal(arma::vec vec1, arma::vec vec2){ return (std::abs(arma::dot(vec1, vec2)) < aid::EPS);}

    template<typename armaNumber> 
    inline arma::Col<armaNumber> entries_to_vector(armaNumber const x_entry, armaNumber const y_entry, armaNumber const z_entry){
        return x_entry * x_hat() + y_entry * y_hat() + z_entry * z_hat();
    }

    inline arma::cx_double exp_imag(double const phi) {return std::cos(phi) + 1.0j * std::sin(phi); }
    inline arma::cx_double spherical_harmonic(int const l, int const m, double const theta, double const phi){
        return 0.5 * (1.0 + aid::sign(m) + (1.0 - aid::sign(m)) * std::pow(-1, m)) * std::sph_legendre(l, std::abs(m), theta) * exp_imag(m * phi);   
    }

    inline arma::cx_double spherical_harmonic_angular_gradient_in_phi(int const l, int const m, double const theta, double const phi){
        return arma::cx_double(0.0, double(m)) * aidsph::spherical_harmonic(l, m, theta, phi) / std::sin(theta);
    }

    inline arma::cx_double spherical_harmonic_angular_gradient_in_theta(int const l, int const m, double const theta, double const phi){
        return 0.5 * std::sqrt(double((l + m + 1) * (l - m))) * aidsph::exp_imag(-phi) * aidsph::spherical_harmonic(l, m + 1, theta, phi)
                - 0.5 * std::sqrt(double((l + m) * (l - m + 1))) * aidsph::exp_imag(phi) * aidsph::spherical_harmonic(l, m - 1, theta, phi);
    }

    inline arma::cx_vec spherical_harmonic_angular_gradient(int const l, int const m, double const theta, double const phi){
        return spherical_harmonic_angular_gradient_in_phi(l, m, theta, phi) * phi_hat(phi) 
                + spherical_harmonic_angular_gradient_in_theta(l, m, theta, phi) * theta_hat(theta, phi);
    }

    template<typename armaNumber>
    arma::cx_double evaluate_vector_in_physical_space(arma::Col<armaNumber> const &vector, double const theta, double const phi){
        int const num_of_modes = vector.n_rows;
        armaNumber evaluation = 0.0;
        for(int mode_index = 0; mode_index < num_of_modes; ++mode_index){
            int const l = aid::l_from_index(mode_index);
            int const m = aid::m_from_indexAndL(mode_index, l);
            evaluation += vector(mode_index) * spherical_harmonic(l, m, theta, phi);
        }
        return evaluation; 
    }
}

#endif