#ifndef SECOND_ORDER_H
#define SECOND_ORDER_H

#include <armadillo>
#include <vector>

class SecondOrder{
    private:
        //info
        int num_of_modes;

        std::vector<arma::sp_cx_mat> C_vec;
        std::vector<arma::sp_cx_mat> K_vec;
        std::vector<arma::sp_cx_mat> X_vec;
        std::vector<arma::sp_cx_mat> Z_vec;
        std::vector<arma::sp_cx_mat> D_vec;        
    public:
        SecondOrder(int const init_num_of_modes);

        inline int get_num_of_modes() const { return num_of_modes; }

        inline arma::SpMat<arma::cx_double> get_C(int const index) const { return C_vec[index]; }
        inline arma::SpMat<arma::cx_double> get_K(int const index) const { return K_vec[index]; }
        inline arma::SpMat<arma::cx_double> get_X(int const index) const { return X_vec[index]; }
        inline arma::SpMat<arma::cx_double> get_Z(int const index) const { return Z_vec[index]; }
        inline arma::SpMat<arma::cx_double> get_D(int const index) const { return D_vec[index]; }
};

#endif