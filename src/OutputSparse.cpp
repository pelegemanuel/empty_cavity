#include "OutputSparse.hpp"

OutputSparse::OutputSparse(int const init_num_of_modes, 
                            std::string const &init_run_folder, 
                            int const max_steps_between_saves, 
                            double const max_time_between_saves) : 
Output(init_num_of_modes, init_run_folder), 
manager(max_steps_between_saves, max_time_between_saves),
mode_amps_sparse(save_path + filenames::mode_amps),
mode_vels_sparse(save_path + filenames::mode_vels),
sparse_save_file(save_path + filenames::sparse_saves),
average_elevation_save_file(save_path + filenames::elevation_squared_avg),
averaging_time(0.0),
elevation_sq_sum(num_of_modes, arma::fill::zeros) {}

void OutputSparse::push_moment_data(SystemState const &state, bool const shouldPushSave){
    if(shouldPushSave){
        outputAid::write_complex_line(mode_amps_sparse, state.get_mode_amps());
        outputAid::write_complex_line(mode_vels_sparse, state.get_mode_vels());
        outputAid::write_main_state_terms(sparse_save_file, state);
        sparse_save_file << std::endl << std::flush;
    }
}

void OutputSparse::push_averaging_data(SystemState const &state, double const dt, bool const shouldPushSave){
    averaging_time += dt;
    elevation_sq_sum += dt * arma::pow(arma::abs(state.get_mode_amps()), 2.0);

    if(shouldPushSave){
        arma::vec const elevation_sq_avg = elevation_sq_sum / (averaging_time + aid::EPS);
        averaging_time = 0.0;
        elevation_sq_sum.fill(0.0);
        outputAid::write_real_line(average_elevation_save_file, elevation_sq_avg);
    }
}