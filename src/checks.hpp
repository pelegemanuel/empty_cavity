#ifndef CODE_CHECKS_H
#define CODE_CHECKS_H

#include <math.h>
#include "dynamic_state.hpp"

static int constexpr L_SH_CHECK = 30;
static int constexpr M_SH_CHECK = 7;
static int constexpr DEFAULT_NUM_OF_TRIALS = 100;
static bool constexpr DEFAULT_TIME_PRINT = false;
static double constexpr MAX_ALLOWED_GRAD_RELATIVE_DIFFERENCE = std::pow(10.0, -6.0);

static double constexpr X_COEF = 1.0, Y_COEF = 2.0, Z_COEF = 3.0;

//runs all checks
void checks_main();

SystemState get_default_check_state();

//ensuring lm-index is one to one
bool lm_index_correspondence(int const max_index_to_check);

bool point_spherical_cartesian_checker(int const num_of_trials = DEFAULT_NUM_OF_TRIALS);

//compare calculations of different algorithms fo eigenvalue calculation
bool eigenvalue_algorithms(bool const shouldPrintTime = DEFAULT_TIME_PRINT);

//check both taylor series functions yield the same value
bool plesset_first_order_series_approximants();

//ensures the computed collapse times are decreasing
bool collapse_time_derivative_condition();

bool second_order_matrix_comparison(int const num_of_modes, int const base_index, bool const shouldPrintTime = DEFAULT_TIME_PRINT);

bool linear_function_gradient_comparison(int const num_of_trials = DEFAULT_NUM_OF_TRIALS);

bool spherical_harmonic_gradient_comparison(int const num_of_trials = DEFAULT_NUM_OF_TRIALS);

bool liquid_velocity_calculations_comparison(int const num_of_trials = DEFAULT_NUM_OF_TRIALS);

void liquid_velocity_calculations_time_comparison(int const num_of_trials = DEFAULT_NUM_OF_TRIALS);

#endif
