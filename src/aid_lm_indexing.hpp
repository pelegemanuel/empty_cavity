#ifndef AID_LM_INDEXING_H
#define AID_LM_INDEXING_H

#include <vector>
#include <math.h>

namespace aid
{
    inline int l_from_index(int const index){ return int(std::sqrt(double(index + 1))); }

    inline int m_from_indexAndL(int const index, int const l){ return index - l*l - l + 1; }

    inline int m_from_index(int const index){ return m_from_indexAndL(index, l_from_index(index)); }

    inline std::pair<int,int> lm_from_index(int const index){ return std::make_pair(l_from_index(index), m_from_index(index)); }

    inline int conjugate_index_from_indexAndM(int const index, int const m){ return index - 2 * m; }

    inline int conjugate_index(int const index){ return conjugate_index_from_indexAndM(index, m_from_index(index)); }

     //turn (l,m) pair to index to index
    //modes are indexed in a lexicographic order
    inline int index_from_lm(int const l, int const m){return l * l - 1 + l + m; }

    inline int max_index_for_l(int const l) {return index_from_lm(l, l); }

    inline int min_index_for_l(int const l) {return index_from_lm(l, -l); }

    inline int effective_max_index_for_l(int const l, int const num_of_modes){ return std::min(max_index_for_l(l), num_of_modes - 1); }

    inline int maximal_l(int const num_of_modes) {return l_from_index(num_of_modes - 1); }

    //number of modes when (0,0) is not taken into account
    inline int mode_num(int const max_l){ return 2 * max_l + max_l * max_l;}

    inline bool is_set_complete(int const num_of_modes){ return (num_of_modes == mode_num(maximal_l(num_of_modes))); }

    //the physical exact analogue of the Fourier transform
    inline double get_k_from_l(int const l){ return std::sqrt(double(l * l + l)); }

    inline double get_k_from_index(int const index){ return aid::get_k_from_l(aid::l_from_index(index)); }

    //the l num corresponding to a specific k value. Not necessarily int
    inline double get_l_from_k(double const k){ return std::sqrt(k * k + 0.25) - 0.5; }

    //return 1 for even numbers, -1 for odd. That is: (-1)^m
    inline int get_parity(int const m){ return -1 + 2 * int(m %2 == 0);}
}

#endif