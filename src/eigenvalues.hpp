#ifndef EIG_H
#define EIG_H

#include <armadillo>
#include <vector>

//direct computation of eigenvalues from the full gaunt matrix
std::vector<double> direct_eigenvalues(int const l_base, int const m_base, int const l_max, bool const shouldReduce, bool const useSparse);

//direct computation of eigenvalues& eigenvectors from the full gaunt matrix
std::pair<std::vector<arma::vec>,std::vector<double>> direct_eigenvectors(int const l_base, int const m_base, int const l_max, bool const shouldReduce, double const lower_bound);

//Computation of the guant eigenvalues using blockwise seperation. For full algorithm see gaunt_spectrum.lyx
//Notations from the file (such as CtC) are used
std::vector<double> block_wise_eigenvalues(int const l_base, int const m_base, int const l_max, bool const shouldReduce);

//turns of eigenvectors of the CtC mat (represented in that basis) to that of the full mat
//full_vec is filled with values of base_vec
void fill_Ci_eigenvector(arma::vec const &full_vec, arma::vec const &base_vec, int const base_l, int const m);

//turns eigenvalues of B mat: Block(0,C,Ct,0) to the full basis
// v is an eigenvector of CtC, and Cv_lam = C*v / lambda with lambda its eigenvalue
arma::vec Ci_eigvec(int const  mode_num, arma::vec const &v, arma::vec const &Cv_lam, int const l_v_base, int const m_v, int const l_Cv_base, int const m_Cv);

//turns of the eigenvectors of the symmetric C mat (represented in that basis) to that of the full mat
//full vec is filled with values of base_vec
void fill_Csym_eigenvector(arma::vec const &full_vec, arma::vec const &base_vec, int const m);

//Computation of the guant eigenvectors and eigenvalues using blockwise seperation. For full algorithm see gaunt_spectrum.lyx
//Notations from the file (such as CtC) are used
std::pair<std::vector<arma::vec>,std::vector<double>> block_wise_eigenvectors(int const l_base, int const m_base, int const l_max, bool const shouldReduce, double const  lower_bound);

//standard deviation for uncorrelated modes
double uncorrelated_std(int const l, int const m, int const l_max);

//returns maximal eigenvalue for mode (l,0) in absolute value
//assumes given vector is ordered
std::vector<double> maximal_eigenvalue(int const  l_max, int const l_max_tensor, bool const shouldReduce);

//timing of 3 methods for computation of eigenvalues
void method_comparison(int const l_base, int const m_base, int const l_max = 40, bool const shouldPrint = false);

//save give mode eigenvalues to file
void spectrum_to_file(int const l_base, int const m_base, int const l_max, bool const shouldReduce);

#endif