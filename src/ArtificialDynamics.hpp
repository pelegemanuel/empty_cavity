#ifndef ARTIFICIAL_DYNAMICS_H
#define ARTIFICIAL_DYNAMICS_H

#include <armadillo>

class SystemState;
class ArtificialDynamics {
    protected:
        int const num_of_modes;

    public:
        static double constexpr DT_FOR_ARTIFICIAL_ACCELERATION = 0.000001;

        ArtificialDynamics(int const init_num_of_modes) : num_of_modes(init_num_of_modes) {}
        ArtificialDynamics() : num_of_modes(0) {}

        inline int get_num_of_modes() const { return num_of_modes; }
        inline double getArtificialAcceleration(double const R_velocity) const {
            return (pumpAndDissipateRadiusVelocity(R_velocity, DT_FOR_ARTIFICIAL_ACCELERATION) - R_velocity) / DT_FOR_ARTIFICIAL_ACCELERATION;
        }

        virtual arma::cx_vec pumpAndDissipateModeVelocities(SystemState const &state, double const dt) const = 0;
        virtual double pumpAndDissipateRadiusVelocity(double const input_R_velocity, double const dt) const = 0;
        virtual void printData() const = 0;
};

#endif