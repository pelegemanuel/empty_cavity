#ifndef SAG_H
#define SAG_H

#include <armadillo>
#include <complex>
#include <cassert>

namespace seriesappx
{
    //returns series for (1 - t / t0)^p
    arma::vec one_minus_t_series(const int max_order, const double power);

    //return series coefs for ln(1-x) up to max order
    arma::vec ln_1_minus_x(const int max_order);

    //returns the series of the cos of a series, of the same order
    arma::vec cos_of_series(const arma::vec &input_series);

    //returns the series of the sin of a series, of the same order
    arma::vec sin_of_series(const arma::vec &input_series);

    //returns matrix creating first derivative of series of f^(n)
    arma::mat first_derivative_matrix(const int max_order, const double der_scaling);

    //returns matrix turning coeficients of series of f to that of f^(n).
    arma::mat derivative_matrix(const int n_der, const int max_order, const double der_scaling);
}

#endif