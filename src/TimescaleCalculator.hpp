#ifndef TIME_SCALE_CALCULATOR_H
#define TIME_SCALE_CALCULATOR_H

#include <armadillo>
#include "aid.hpp"
#include "dynamic_state.hpp"
#include "wigner_coefs.hpp"
#include "aid_arma_templates.hpp"

class TimescaleCalculator
{
    private:
        mutable arma::vec amplitude_abs_sum_for_l, velocity_abs_sum_for_l;

        inline double get_surface_oscillations_timescale(SystemState const &state) const {
            return 2.0 * aid::PI / (std::sqrt(std::abs(state.get_last_A())) + aid::EPS);
        }

        inline double get_perturbation_amplitude_timescale(SystemState const &state) const {
            aid::sum_quantity_for_l<double>(arma::abs(state.get_mode_amps()), amplitude_abs_sum_for_l);
            aid::sum_quantity_for_l<double>(arma::abs(state.get_mode_vels()), velocity_abs_sum_for_l);
            return arma::min((amplitude_abs_sum_for_l + aid::EPS) / (velocity_abs_sum_for_l + aid::EPS));
        }

        inline double get_R_timescale(SystemState const &state) const {
            return state.get_R() / (std::abs(state.get_R_velocity()) + aid::EPS);
        }

        inline double get_R_velocity_timescale(SystemState const &state) const {
            return std::abs(state.get_R_velocity()) / (std::abs(state.get_R_acceleration()) + aid::EPS);
        }

        inline double constant_radius_rescaling_time(bool const ignoreRadiusVelocity) const {
            return double(ignoreRadiusVelocity);
        }

        inline double get_radius_total_timescale(SystemState const &state) const {
            return std::min(get_R_timescale(state), get_R_velocity_timescale(state)) 
                    + constant_radius_rescaling_time(state.shouldIgnoreRadiusVelocity());
        }

        inline double get_perturbation_total_timescale(SystemState const &state) const {
            return std::min(get_surface_oscillations_timescale(state), get_perturbation_amplitude_timescale(state));
        }
    public:
        TimescaleCalculator(int const l_max) 
        : amplitude_abs_sum_for_l(l_max, arma::fill::zeros), velocity_abs_sum_for_l(l_max, arma::fill::zeros) {}
        
        inline double get_timescale(SystemState const &state) const {
            return std::min(get_perturbation_total_timescale(state), get_radius_total_timescale(state));
        }

};

#endif