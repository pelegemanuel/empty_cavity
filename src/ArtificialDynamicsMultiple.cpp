#include "ArtificialDynamicsMultiple.hpp"
#include "dynamic_state.hpp"
#include <boost/assign/list_of.hpp>
#include <cassert>

ArtificialDynamicsMultiple::ArtificialDynamicsMultiple(std::vector<ArtificialDynamics*> const &initSubArtificialDynamicsVector) :
ArtificialDynamics(ArtificialDynamicsMultiple::get_and_check_num_of_modes(initSubArtificialDynamicsVector)),
subArtificialDynamicsVector(initSubArtificialDynamicsVector)
{}

ArtificialDynamicsMultiple::ArtificialDynamicsMultiple(ArtificialDynamics* const firstArtificialDynamics, ArtificialDynamics* const secondArtificialDynamics) :
ArtificialDynamics(std::min(firstArtificialDynamics->get_num_of_modes(), secondArtificialDynamics->get_num_of_modes())),
subArtificialDynamicsVector(boost::assign::list_of(firstArtificialDynamics)(secondArtificialDynamics))
{
    assert(firstArtificialDynamics->get_num_of_modes() = secondArtificialDynamics->get_num_of_modes());
}

arma::cx_vec ArtificialDynamicsMultiple::pumpAndDissipateModeVelocities(SystemState const &state, double const dt) const {
    int effective_num_of_modes = (num_of_modes > 0) ? num_of_modes : state.get_num_of_modes();
    arma::cx_vec mode_velocities_diff(effective_num_of_modes, arma::fill::zeros);

    for(ArtificialDynamics* singleArtificialDynamics : subArtificialDynamicsVector){
        mode_velocities_diff += singleArtificialDynamics->pumpAndDissipateModeVelocities(state, dt) - state.get_mode_vels();
    }
    return state.get_mode_vels() + mode_velocities_diff;
}

double ArtificialDynamicsMultiple::pumpAndDissipateRadiusVelocity(double const input_R_velocity, double const dt) const {
    double R_velocity = input_R_velocity;
    for(ArtificialDynamics* singleArtificialDynamics : subArtificialDynamicsVector){
        R_velocity = singleArtificialDynamics->pumpAndDissipateRadiusVelocity(R_velocity, dt);
    }
    return R_velocity;
}

void ArtificialDynamicsMultiple::printData() const {
    if(subArtificialDynamicsVector.size() == 0){
        std::cout << "Empty artificial dynamics object (no dissipation / pumping)" << std::endl;
    } else {
        std::cout << "Multiple pumping / dissipation terms: " << std::endl; 
        for(ArtificialDynamics* singleArtificialDynamics : subArtificialDynamicsVector){
            singleArtificialDynamics->printData();
        }
    }    
}

int ArtificialDynamicsMultiple::get_and_check_num_of_modes(std::vector<ArtificialDynamics*> const &artificialDynamicsVector){
    bool first_checked = false;
    int num_of_modes = 0;

    for(ArtificialDynamics* artificialDynamicsPointer : artificialDynamicsVector){
        if(first_checked == false){
            num_of_modes = artificialDynamicsPointer->get_num_of_modes();
            first_checked = true;
        }
        if(first_checked && (num_of_modes != artificialDynamicsPointer->get_num_of_modes())){
            std::cout << "Objects with different num of modes given! " << std::endl;
            num_of_modes = std::min(num_of_modes, artificialDynamicsPointer->get_num_of_modes());
        }
    }
    return num_of_modes;
}
