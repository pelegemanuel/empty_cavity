#ifndef ARTIFICIAL_DYNAMICS_MULTIPLE_H
#define ARTIFICIAL_DYNAMICS_MULTIPLE_H

#include <armadillo>
#include "ArtificialDynamics.hpp"

class ArtificialDynamicsMultiple: public ArtificialDynamics
{
    private:
        std::vector<ArtificialDynamics*> const subArtificialDynamicsVector;

    public:
        ArtificialDynamicsMultiple(std::vector<ArtificialDynamics*> const &initSubArtificialDynamicsVector);
        ArtificialDynamicsMultiple(ArtificialDynamics* const firstArtificialDynamics, ArtificialDynamics* const secondArtificialDynamics);
        ArtificialDynamicsMultiple() : subArtificialDynamicsVector() {}
        

        arma::cx_vec pumpAndDissipateModeVelocities(SystemState const &state, double const dt) const;
        double pumpAndDissipateRadiusVelocity(double const input_R_velocity, double const dt) const;
        void printData() const;

        static int get_and_check_num_of_modes(std::vector<ArtificialDynamics*> const &artificialDynamicsVector);
};

#endif