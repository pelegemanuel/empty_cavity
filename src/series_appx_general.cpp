#include "series_appx_general.hpp"
#include "aid.hpp"
#include "aid_lm_indexing.hpp"
#include "series_appx_arma_template.cpp"

namespace seriesappx
{ 
    //returns series for (1 - t / t0)^p
    arma::vec one_minus_t_series(const int max_order, const double power){
        assert(max_order >= 0);
        arma::vec series_coefs(max_order + 1, arma::fill::zeros);
        for(int order = 0; order <= max_order; ++order){
            series_coefs(order) = double(aid::get_parity(order)) * aid::generalized_binomial_coeficient(power, order);
        }
        return series_coefs;
    }

    //return series coefs for ln(1-x) up to max order
    arma::vec ln_1_minus_x(const int max_order){
        assert(max_order >= 0);
        arma::vec coef_series(max_order + 1, arma::fill::zeros);
        coef_series(0) = 0.0;
        for(double order = 1.0; order <= max_order; ++order){
            coef_series(order) = -1.0 / order;
        }
        return coef_series;
    }

    //returns the series of the cos of a series, of the same order
    arma::vec cos_of_series(const arma::vec &input_series){
        int len = input_series.n_rows;
        int max_order = len - 1;
        arma::vec cos_series(len);
        arma::vec series_squared = series_power(input_series, 2);
        cos_series.zeros();
        cos_series(0) = 1.0;
        for(int half_order = 1; 2 * half_order <= max_order; ++half_order){
            cos_series += series_power(series_squared, half_order) / double(aid::get_parity(half_order) * aid::factorial(2 * half_order));
        }
        return cos_series;
    }

    //returns the series of the sin of a series, of the same order
    arma::vec sin_of_series(const arma::vec &input_series){
        int len = input_series.n_rows;
        int max_order = len - 1;
        arma::vec sin_series(len);
        arma::vec series_squared = series_power(input_series, 2);
        sin_series.zeros();
        sin_series(0) = 1.0;
        for(int half_order = 1; 2 * half_order + 1 <= max_order; ++half_order){
            sin_series += series_power(series_squared, half_order) / double(aid::get_parity(half_order) * aid::factorial(2 * half_order + 1));
        }
        sin_series = multiply_series(sin_series, input_series);
        return sin_series;
    }

    //returns matrix creating first derivative of series of f^(n)
    arma::mat first_derivative_matrix(const int max_order, const double der_scaling){
        assert(max_order >= 0);
        arma::mat der_mat(max_order + 1, max_order + 1, arma::fill::zeros);
        for(int idx = 0; idx < max_order; ++idx){
            der_mat(idx, idx + 1) = idx + 1; //output order in first index, input order in the second
        }
        return der_scaling * der_mat;
    }

    //returns matrix turning coeficients of series of f to that of f^(n).
    arma::mat derivative_matrix(const int n_der, const int max_order, const double der_scaling){
        assert(max_order >=0);
        assert(n_der >= 0);
        arma::mat der_mat = first_derivative_matrix(max_order, der_scaling);
        arma::mat res_mat(max_order + 1, max_order +1, arma::fill::eye);
        for(int mul = 0 ; mul < n_der; ++mul){
            res_mat = res_mat * der_mat;
        }
        return res_mat;
    }
}