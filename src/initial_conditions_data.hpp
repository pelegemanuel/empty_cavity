#ifndef INIT_COND_DATA
#define INIT_COND_DATA

#include "PhysicalAttributes.hpp"

class InitialConditionsData{
    private:
        int const initial_perturbation_type_key;
        double const sig;
        double const R0;
        double const R_velocity_0;
        int const minimal_l;

    public:
        //collapse constructor
        InitialConditionsData(int const init_initial_perturbation_type_key,
                              double const init_sig, 
                              int const init_minimal_l = 1);

        //stable constructor
        InitialConditionsData(int const init_initial_perturbation_type, 
                              double const init_sig, 
                              PhysicalAttributes const &physAttributes, 
                              double const R_velocity_init = 0.0,
                              int const init_minimal_l = 1);

        inline int get_initial_perturbation_type_key() const {return initial_perturbation_type_key; }
        inline double get_sig() const {return sig; }
        inline double get_R0() const {return R0; }
        inline double get_R_velocity_0() const {return R_velocity_0; }
        inline int get_minimal_l() const {return minimal_l; }
};

#endif
