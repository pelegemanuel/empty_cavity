#ifndef TIME_MANAGER
#define TIME_MANAGER

#include "dynamic_state.hpp"
#include "TimescaleCalculator.hpp"

class TimeManager{
    protected:
        const TimescaleCalculator timescaleCalc;
        double const max_amp_to_R;
        double const max_norm_to_R;
        double const dt_over_typical_time;
        double const max_time;
        int const max_num_of_steps;

        int step;

        inline void finishPrintMessage(SystemState const &state) const {
            std::cout << "steps taken: " << step << " | time: " << state.get_time() << std::endl;  
        }
        
    public:
        static int constexpr STEPS_BETWEEN_PRINTS = 5000;
        static int constexpr STEPS_BETWEEN_SECOND_ORDER_COMPUTATION = 25;
        static double constexpr DT_MIN = std::pow(10, -8.0);
        static double constexpr DT_MAX = std::pow(10.0, -4.0);

        TimeManager(int const l_max, double const init_max_amp_to_R, double const init_max_norm_to_R, double const init_dt_over_typical_time, double const init_max_time, int const init_max_num_steps);

        inline double get_dt(SystemState const &state) const {
            return std::min(DT_MAX, std::max(DT_MIN, dt_over_typical_time * timescaleCalc.get_timescale(state))); 
        }

        inline bool shouldUpdateSecondOrder() const { return (step % STEPS_BETWEEN_SECOND_ORDER_COMPUTATION == 0); }
        inline int get_step() const {return step; }
        inline void count_step() { ++step; }

        bool shouldContinue(SystemState const &state, double const dt) const;
        void printTimeData(SystemState const &state) const; 
};

#endif