#ifndef INITCOND_H
#define INITCOND_H

#include <armadillo>
#include "dynamic_state.hpp"

class SystemState;
namespace initcond{
    const static int SURFACE_OSCILLATIONS_NORMAL = 0;

    const static double NUM_OF_STANDARD_DEV_LIMITER = 3.0;

    std::pair<arma::cx_vec,arma::cx_vec> get_initial_perturbations(int const key, SystemState const &state);

    //see initial conditions file
    std::pair<arma::cx_vec,arma::cx_vec> surfaceWavesInitialConditions(SystemState const &state);

    arma::cx_vec get_modeAmpsMinus_from_modeAmpsPlus(arma::cx_vec const &modeAmpsPlus);
}

#endif
