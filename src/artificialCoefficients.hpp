#ifndef ARTIFICIAL_COEFFICIENTSS_H
#define ARTIFICIAL_COEFFICIENTSS_H

#include <armadillo>

//Artificial dissipation according to "Weak Turbulent Kolmogorov Spectrum for Surface Gravity Waves" - Dyachenko, Korotkevich, Zakharov
//https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.92.134501
arma::cx_vec disp_coefs_Dyachenko_Korotkevich_Zakahrov_2003(int const num_of_modes, int const l1, int const l2, double const gamma1, double const gamma2);

//Artificial dissipation according to "Turbulence of Capillary Waves" - Pushkarev, Zakharov
//https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.76.3320
arma::cx_vec disp_coefs_Pushkarev_Zakharov_1996(int const num_of_modes, int const l0, double const gamma);

//Artificial pumping according to "Turbulence of Capillary Waves" - Pushkarev, Zakharov
//https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.76.3320
arma::cx_vec pump_coefs_Pushkarev_Zakharov_1996(int const num_of_modes, double const k1, double const k2, double const f0);

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
double h_function_Landau_plasma_damping(double const x);
arma::cx_vec disp_coefs_Landau_plasma_damping_Dyachenko(int const num_of_modes, double const kd);

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
arma::cx_vec disp_coefs_Dyachenko_large_wavelengths(int const num_of_modes, double const k_bound, double const coeficient);

//See part 6 of "Optical Turbulence: weak turbulence, condensates and collapsing filaments in the non linear Schrodinger equation" - Dyachenko, Newell, Pushkarev, Zakharov
//https://www.sciencedirect.com/science/article/pii/016727899290090A
arma::cx_vec pumping_coefs_Dyachenko(int const num_of_modes, double const k_min, double const k_max, double const coeficient);

#endif