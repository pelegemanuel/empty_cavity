#ifndef ARTIFICIAL_DYNAMICS_INCLUDE_ALL_H
#define ARTIFICIAL_DYNAMICS_INCLUDE_ALL_H

#include "artificialCoefficients.hpp"
#include "ArtificialDynamics.hpp"
#include "ArtificialDynamicsLinear.hpp"
#include "ArtificialDynamicsDirectional.hpp"
#include "ArtificialDynamicsMultiple.hpp"
#include "ArtificialDynamicsZakharovOscillator.hpp"

#endif