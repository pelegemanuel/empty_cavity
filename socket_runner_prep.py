### Creates and runs jobs using a python interface ###
import os
import shutil
import build_aid
import analysis

CHECKS_DIRECTORY_NAME = "checks"
PY_FIN = ".py"

def create_run_file_and_directories(directoryName, data_input):
    outname = "out_" + directoryName + ".txt"
    errname = "err_" + directoryName + ".txt"
    filename = "python_auto_run.slurm"
    data_input = " " + directoryName + " " + data_input
    thisResultsDirectoryPath = build_aid.forceCreateDirectory(build_aid.RESULTS_DIRECTORY, directoryName)
    shutil.copy(build_aid.BUILD_DIRECTORY + "a.out", thisResultsDirectoryPath)
    profilerResultsFile = os.path.join(thisResultsDirectoryPath, build_aid.PROFILER_RESULTS)
    
    f = open(filename, "a")
    f.write("#!/bin/sh" + "\n")
    f.write("#" + "\n")
    f.write("\n")
    f.write("#SBATCH --job-name=socket_run" +" \n")
    f.write("#SBATCH --output=" + thisResultsDirectoryPath + outname + "\n")
    f.write("#SBATCH --error=" + thisResultsDirectoryPath + errname + "\n")
    f.write("##SBATCH --sockets-per-node=2" + "\n") 
    f.write("##SBATCH --partition=socket" + "\n")
    f.write("##SBATCH --ntasks=8" + "\n")
    f.write("#SBATCH --ntasks=9" + "\n")
    f.write("#SBATCH --sockets-per-node=1" + "\n\n")
    f.write("cd " + thisResultsDirectoryPath + "\n") 
    f.write("./a.out" + data_input + "\n")
    f.write("gprof " + "a.out " + "gmon.out > " + build_aid.PROJECT_PATH + profilerResultsFile + "\n")
    f.close
    return filename

def create_checks_file():
    data_input = " "
    return create_run_file_and_directories(CHECKS_DIRECTORY_NAME, data_input)

def create_simmulation_file(max_l, dt_to_t_ratio, sig, alpha):
    save_name = str(max_l) + "_"  + str(sig) + "_" + str(alpha)
    directoryName = save_name
    data_input = str(max_l) + " " + str(dt_to_t_ratio) + " " + str(sig) + " " + str(alpha)
    return create_run_file_and_directories(directoryName, data_input)

def create_self_similar_hunter_file(max_l, main_l, sig):
    directoryName = "hunter" + str(max_l) + "_" + str(main_l) + "_" +str(sig)
    data_input = str(max_l) + " " + str(main_l) + " " + str(sig)
    hunterFileName = directoryName + PY_FIN
    pythonFile = open(build_aid.ANALYSIS_DIRECTORY + hunterFileName, "a")
    pythonFile.write("import singleModePlot as smp\n")
    pythonFile.write("smp.singleAmplitudePlotter(\"" + directoryName +"\")\n")
    pythonFile.close

    filename = create_run_file_and_directories(directoryName, data_input)
    f = open(filename, "a")
    f.write("cd .. \n")
    f.write("cd .. \n")
    f.write("cd " + build_aid.ANALYSIS_DIRECTORY_NAME + "\n")
    f.write("python3 " + hunterFileName + "\n")
    f.write("rm " + hunterFileName + "\n")
    f.close()
    return filename

def checks_runner():
    filename = create_checks_file()
    build_aid.run_job(filename)
    build_aid.del_file(filename)

def seker_runner(option_list):
    for param_list in option_list:
        if(len(param_list) == 4):
            max_l = int(param_list[0])
            dt_to_t_ratio = float(param_list[1])
            sig = float(param_list[2])
            alpha = float(param_list[3])

            filename = create_simmulation_file(max_l, dt_to_t_ratio, sig, alpha)
            build_aid.run_job(filename)
            build_aid.del_file(filename)
        else:
            print("Error! not exact amount of params")

def self_similar_hunter_runner(max_l, main_l, sig):
    filename = create_self_similar_hunter_file(max_l, main_l, sig)
    build_aid.run_job(filename)
    build_aid.del_file(filename)

def option_list_creator(max_l_list, dt_to_t_ratio_list, sig_list, alpha_list):
    options_list = []
    for max_l in max_l_list:
        for dt_to_t_ratio in dt_to_t_ratio_list:
            for sig in sig_list:
                for alpha in alpha_list:
                    options_list.append([max_l, dt_to_t_ratio, sig, alpha])
    return options_list
