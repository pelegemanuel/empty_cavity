from sys import modules
from matplotlib import cm
import numpy as np
import analysisAid as aid
import coordinatesAid as coord
from scipy import special as sp
import mapsOnSpheres as maps
import string

def getElevationMatrix(amplitudes, thetaVec, phiVec):
    elevationMat = np.zeros((len(phiVec), len(thetaVec)), dtype = complex)
    lArray, mArray = aid.getLmArrays(len(amplitudes))

    for phiIndex in range(len(phiVec)):
        for thetaIndex in range(len(thetaVec)):
            elevationMat[phiIndex, thetaIndex] = np.dot(amplitudes, 
                                                        sp.sph_harm(mArray, lArray, phiVec[phiIndex], thetaVec[thetaIndex]))
    return np.real(elevationMat)

def elevationFigureFromAmps(amplitudes, saveName):
    thetaVec, phiVec = coord.getThetaAndPhiVectors()
    elevationMat = getElevationMatrix(amplitudes = amplitudes, thetaVec = thetaVec, phiVec = phiVec)
    colorMap = cm.jet
    maps.createAllMaps(saveName, thetaVec, phiVec, elevationMat, colorMap)

def elevationFigureFromFile(name):
    filename = aid.RESULTS_PATH + name + "/perturbationAmplitudes.csv"
    last_full_line = ""
    with open(filename) as file:
        for line in file:
            if(len(line) >= len(last_full_line)):
                last_full_line = line
    last_full_line_list = last_full_line.split(" ")
    last_full_line_list = aid.removeWhitespaceStrings(last_full_line_list)

    last_full_line_array = np.zeros((len(last_full_line_list)), dtype = complex)
    for index, strElement in enumerate(last_full_line_list):
        strEdited = ''.join([x for x in strElement if x in string.printable])
        last_full_line_array[index] = aid.csv_string_to_complex(strEdited) 

    last_full_line_array = np.array(last_full_line_array)
    elevationFigureFromAmps(last_full_line_array, name)