import numpy as np
from pathlib import Path
import os, sys, inspect

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import build_aid

PROJECT_PATH = str(Path().absolute().parent) + "/"
RESULTS_PATH = PROJECT_PATH + build_aid.RESULTS_DIRECTORY
ANALYZED_RESULTS_PATH = PROJECT_PATH + build_aid.ANALYZED_RESULTS_DIRECTORY

PIC_FIN = ".jpeg"
TXT_FIN = ".txt"
GRAPH_FONTSIZE = 11
GRAPH_FONT = {'fontsize': GRAPH_FONTSIZE}


# save file indices
TIME_INDEX = 0
RADIUS_INDEX = 1
RADIUS_VEL_INDEX = 2
NORM_INDEX = 3
AMP_INDEX = 4
AMP_VEL_INDEX = 5

THEORETICAL_POWER_LAW = -4.75

def removeWhitespaceStrings(stringList):
    for str in stringList:
        if(str.isspace()):
            stringList.remove(str)
    return stringList

def csv_string_to_complex(elem):
    elem1 = elem.replace('i', 'j', 2)
    elem2 = elem1.replace('+-', '-', 2)
    return np.complex(elem2)

def vec_csv_complex_reader(name):
    vec_str = np.genfromtxt(name, dtype = str)
    res_vec = np.zeros(len(vec_str), dtype = complex)
    for idx, elem in enumerate(vec_str):
        res_vec[idx] = csv_string_to_complex(elem)
    return res_vec

def mat_csv_complex_reader(name):
    vec_str = np.genfromtxt(name, dtype = str)
    list_lists_str = [1] * len(vec_str)
    for idx in range(len(vec_str)):
        row_unedited = vec_str[idx]
        row = [0] * len(row_unedited)
        for in_row_idx in range(len(row_unedited)):
            row[in_row_idx] = csv_string_to_complex(row_unedited[in_row_idx])
        list_lists_str[idx] = np.array(row)
    return np.array(list_lists_str)

def avg_param_per_l(l_list, param_list):
    avg_param_list = []
    l_reduced_list = []
    for idx in range(len(l_list)):
        if not (l_list[idx] == l_list[idx - 1]):
            param_for_l = param_list[l_list == l_list[idx]] 
            avg_param_list.append(np.mean(param_for_l))
            l_reduced_list.append(l_list[idx])
    return l_reduced_list, avg_param_list

def lm_from_index(index):
    l = int(np.sqrt(float(index + 1)))
    m = index - l*l - l + 1
    return l, m

def getLmArrays(numOfModes):
    lArray = np.zeros(numOfModes, dtype = int)
    mArray = np.zeros(numOfModes, dtype = int)
    for mode_index in range(numOfModes):
        lArray[mode_index], mArray[mode_index] = lm_from_index(mode_index)
    return lArray, mArray

def power_law(x, a, b):
    return b * np.power(x, a)

def getCutter(data, bounds):
    upperBound = max(bounds)
    lowerBound = min(bounds)
    cutter = np.array([data >= lowerBound]) * np.array([data <= upperBound])
    return cutter
