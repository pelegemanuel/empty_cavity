import numpy as np
import math

THETA_NUM = 300
PHI_NUM = 600
THETA_EDGE_NUM = THETA_NUM // 10
THETA_EDGE_ANGLE = 0.1

def get3Dcoordinates(phiVec, thetaVec):
    XX = np.outer(np.cos(phiVec), np.sin(thetaVec))
    YY = np.outer(np.sin(phiVec), np.sin(thetaVec))
    ZZ = np.outer(np.ones(len(phiVec)), np.cos(thetaVec))
    return XX, YY, ZZ

def getThetaAndPhiVectors():
    linearEdgeZero = np.linspace(start = 0.0, stop = THETA_EDGE_ANGLE, 
                                    num = THETA_EDGE_NUM, endpoint = False)
    cosThetaVecMiddle = np.linspace(start = np.cos(THETA_EDGE_ANGLE), stop = np.cos(math.pi - THETA_EDGE_ANGLE),
                                    num = THETA_NUM - 2 * THETA_EDGE_NUM, endpoint = False)
    linearEdgePi = np.linspace(start = math.pi - THETA_EDGE_ANGLE, stop = math.pi, 
                                num = THETA_EDGE_NUM, endpoint = True)
    thetaVec = np.concatenate((linearEdgeZero, np.arccos(cosThetaVecMiddle), linearEdgePi))
    phiVec = np.linspace(0, 2 * math.pi, num = PHI_NUM, endpoint=True)
    return thetaVec, phiVec