import numpy as np
import analysisAid as aid
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib.ticker import ScalarFormatter

GRADIENT_FOR_FIT_SAVENAME = aid.ANALYZED_RESULTS_PATH + "gradientForFit_"
ELEVATION_SQUARED_SAVENAME = aid.ANALYZED_RESULTS_PATH + "avgElevation_"

MEASUREMENT_FORMAT = '.b'
THEORY_FORMAT = '--g'
NUM_OF_POINTS_TO_DISCARD = 5

def read_and_average_elevation(name):
    load_file = open(aid.RESULTS_PATH + name + "/elevationSquaredAvg.csv")
    
    analyzed_lines = []
    mode = np.genfromtxt(aid.RESULTS_PATH + name + "/modeAngmomentum.csv", dtype = float)
    line_count = 0

    for line in load_file:
        line_count = line_count + 1
        line_list = line.split(" ")
        del line_list[-1]

        try:
            line_array = np.array(line_list, dtype = float)
            mode_red, elevationRed = aid.avg_param_per_l(mode, line_array)
            elevationRed = np.array(elevationRed)
            analyzed_lines.append(elevationRed)
        except:
            print("line format unknonwn")

    mode_red = np.array(mode_red)
    return mode_red, analyzed_lines

def spectrumAxesSettings(ax, shouldLogPlot, plotRange = None, shouldGrid = True):
    if(shouldLogPlot):
        ax.set_xscale("log")
        ax.set_yscale("log")
    
    ax.set_ylabel("${<|a|>}^2 $", rotation = 0, **aid.GRAPH_FONT)
    ax.set_xlabel("$\sqrt{l^2 + l}$", **aid.GRAPH_FONT)
    xtick_list = np.array([1, 5, 10, 20, 30, 40, 50, 60, 65])
    plotCutter = aid.getCutter(data = xtick_list, bounds = plotRange)
    xtick_list = xtick_list[tuple(plotCutter)]
    ax.set_xticks(xtick_list)
    ax.xaxis.set_major_formatter(ScalarFormatter())
    if(shouldGrid):
        ax.grid()

def fitAccordingToKey(angMomentumForFit, elevationRedForFit, elevationRedErrorForFit, fitKey):
    if(fitKey in ["lin", "linear", "polyfit"]):
        sigma = np.divide(elevationRedErrorForFit, elevationRedForFit, dtype = float)
        weights = 1.0 / sigma
        [fit_gradient, lnCoef], covmat = np.polyfit(np.log(angMomentumForFit), 
                                                    np.log(elevationRedForFit), 
                                                    deg = 1, 
                                                    full = False,
                                                    w = weights, 
                                                    cov = True)
        coef = np.exp(lnCoef)
        coef_err = np.sqrt(covmat[1][1]) * coef
    else:
        initial_guess = [aid.THEORETICAL_POWER_LAW, elevationRedForFit[0]]
        bounds = (-np.inf, np.inf)

        [fit_gradient, coef], covmat = curve_fit(f = aid.power_law, 
                                                xdata = angMomentumForFit, 
                                                ydata = elevationRedForFit,
                                                sigma = elevationRedErrorForFit,
                                                p0 = initial_guess,
                                                bounds = bounds)
        coef_err = np.sqrt(covmat[1][1])
    fit_gradient_err = np.sqrt(covmat[0][0])
    return [fit_gradient, coef], [fit_gradient_err, coef_err]

def fit_average_elevation(name, fitRange, fitKey, avgNum = 1, plotRange = None, shouldLogPlot = True, shouldGrid = True):
    mode_red, analyzed_lines = read_and_average_elevation(name)
    
    fitCutter = aid.getCutter(data = mode_red, bounds = fitRange)
    angMomentum = np.sqrt(mode_red ** 2.0 + mode_red)
    angMomentumForFit = angMomentum[tuple(fitCutter)]
    
    gradient_list = []
    gradient_err_list = []
    print("Num of points: " + str(len(analyzed_lines)))

    for lineIndex in range(len(analyzed_lines) - NUM_OF_POINTS_TO_DISCARD):
        numOfPoints = min(lineIndex + 1, avgNum)
        minIndex = lineIndex - numOfPoints + 1
        if(numOfPoints > 1):
            elevationRed = np.mean(analyzed_lines[minIndex:lineIndex + 1], axis = 0, dtype = float)
            elevationRedError = np.std(analyzed_lines[minIndex:lineIndex + 1], axis = 0, dtype = float)
        else:
            elevationRed = analyzed_lines[lineIndex]
            elevationRedError = np.divide(elevationRed, 2.0 * mode_red + 1.0)

        elevationRedForFit, elevationRedErrorForFit = elevationRed[tuple(fitCutter)], elevationRedError[tuple(fitCutter)]

        [fit_gradient, coef], [fit_gradient_err, coef_err] = fitAccordingToKey(angMomentumForFit, 
                                                                                elevationRedForFit, 
                                                                                elevationRedErrorForFit,
                                                                                fitKey)
        print(name + 
            ": exponent = " + str(fit_gradient) + " +- " + str(fit_gradient_err) 
            + " | coef = " + str(coef) + " +- " + str(coef_err)
            + " | num of outputs: " + str(numOfPoints))

        gradient_list.append(fit_gradient)
        gradient_err_list.append(fit_gradient_err)

    spectrumFigure, spectrumAxes = plt.subplots()
    if(plotRange is None): #all by default
        plotRange = [min(mode_red) - 1.0, max(mode_red) + 1.0]
    plotCutter = aid.getCutter(data = mode_red, bounds = plotRange)
    angMomentumForPlot = angMomentum[tuple(plotCutter)]
    elevationRedForPlot, elevationRedErrorForPlot = elevationRed[tuple(plotCutter)], elevationRedError[tuple(plotCutter)]
    spectrumFit = coef * np.power(angMomentumForPlot, fit_gradient)
    spectrumAxes.errorbar(angMomentumForPlot, elevationRedForPlot, yerr = elevationRedErrorForPlot, fmt = MEASUREMENT_FORMAT)
    spectrumAxes.plot(angMomentumForPlot, spectrumFit, THEORY_FORMAT)

    spectrumAxesSettings(spectrumAxes, shouldLogPlot, plotRange, shouldGrid)
    plt.setp(spectrumAxes.get_xticklabels(), **aid.GRAPH_FONT)
    plt.setp(spectrumAxes.get_yticklabels(), **aid.GRAPH_FONT)
    plt.tight_layout()
    spectrumFigure.savefig(ELEVATION_SQUARED_SAVENAME + name + aid.PIC_FIN, bbox_inches = 'tight')

    #gradient list for known times
    sparse_saves = np.genfromtxt(aid.RESULTS_PATH + name + "/sparseSaves.csv", dtype = float)
    times = np.real(sparse_saves[:,aid.TIME_INDEX])
    times = times[0:len(gradient_list)]
    grad_theory = [aid.THEORETICAL_POWER_LAW] * len(times) 
    plt.figure()
    plt.plot(times, np.array(gradient_list), MEASUREMENT_FORMAT, markersize = 5)
    plt.plot(times, np.array(grad_theory), THEORY_FORMAT)
    plt.xlabel(r'time $\left[ \sqrt{{R_0}^3 / \alpha} \right]$', **aid.GRAPH_FONT)
    plt.ylabel("fit gradient", **aid.GRAPH_FONT)
    plt.xticks(**aid.GRAPH_FONT), plt.yticks(**aid.GRAPH_FONT)
    plt.tight_layout()
    if(shouldGrid):
        plt.grid()
    plt.savefig(GRADIENT_FOR_FIT_SAVENAME + name + aid.PIC_FIN)

    return fit_gradient, fit_gradient_err