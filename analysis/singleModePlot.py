import numpy as np
import matplotlib.pyplot as plt
import analysisAid as aid

THEORETICAL_COLLAPSE_TIME = 0.4

def firstOrderFrequency(l):
    if(l == 1.0):
        return 0.0
    return 0.25 * np.sqrt(24.0 * l - 25.0)

def getLNumberFromName(name):
    nameList = name.split("_")
    return int(nameList[1])

def resimmulateEquation(initialAmplitude, initialVelocity, l_num, finalTime = THEORETICAL_COLLAPSE_TIME):
    N = 5000
    dt = finalTime / N
    t_vec = np.linspace(0, finalTime, N, endpoint=False)
    a_vec = np.zeros(N, dtype = complex)
    a_dot_vec = np.zeros(N, dtype = complex)

    a_vec[0] = initialAmplitude
    a_dot_vec[0] = initialVelocity

    for step in range(N-1):
        a_vec[step + 1] = a_vec[step] + dt * a_dot_vec[step]
        Rdot_over_R = -0.4 / (finalTime - t_vec[step]) 
        a_dot_vec[step + 1] = a_dot_vec[step] + dt * (-3.0 * Rdot_over_R * a_dot_vec[step] - 1.5*(l_num - 1.0) * Rdot_over_R**2.0 * a_vec[step])
    
    return t_vec, a_vec

def theoreticalHunterSolution(initialAmplitude, initialVelocity, radiusVec, initialRadiusVelocity, l_num):
    Wl = firstOrderFrequency(l_num)
    phase_array = Wl * np.log(radiusVec / radiusVec[0])
    sinCoef = (0.25 * initialAmplitude + radiusVec[0] * initialVelocity / initialRadiusVelocity) / Wl
    return (np.power(radiusVec / radiusVec[0], -0.25)) * (initialAmplitude * np.cos(phase_array) + sinCoef * np.sin(phase_array))

def singleAmplitudePlotter(name, shouldGrid = True):
    return singleAmplitudePlotterExplicitL(name, getLNumberFromName(name), shouldGrid)

def singleAmplitudePlotterExplicitL(name, l_num, shouldGrid):
    res_mat = aid.mat_csv_complex_reader(aid.RESULTS_PATH + name + "/denseSaves.csv")
    time = np.real(res_mat[:,aid.TIME_INDEX])
    R = np.real(res_mat[:,aid.RADIUS_INDEX])
    R_velocity = np.real(res_mat[:,aid.RADIUS_VEL_INDEX])
    a = res_mat[:,aid.AMP_INDEX]
    a_dot = res_mat[:,aid.AMP_VEL_INDEX]

    theoryAmplitude = theoreticalHunterSolution(a[0], a_dot[0], R, R_velocity[0], l_num)
    oneOvertimeFromCollapse = 1.0 / (THEORETICAL_COLLAPSE_TIME - time)

    theory_plot_real, = plt.plot(oneOvertimeFromCollapse, np.real(theoryAmplitude), '-b')
    exp_plot_real, = plt.plot(oneOvertimeFromCollapse, np.real(a), '--m')
    plot_list = [theory_plot_real, exp_plot_real]
    name_list = ["theory", "simulation"]

    plt.legend(plot_list, name_list, **aid.GRAPH_FONT)
    plt.xscale("log")
    plt.ylabel("mode amplitude", **aid.GRAPH_FONT)
    plt.xlabel(r"$\left(t_c - t\right)^{-1}$", **aid.GRAPH_FONT)
    plt.xticks(**aid.GRAPH_FONT), plt.yticks(**aid.GRAPH_FONT)
    plt.ticklabel_format(style = 'sci', axis = 'y', scilimits=(0,0))
    plt.tight_layout()

    if(shouldGrid):
        plt.grid()

    plt.savefig(aid.ANALYZED_RESULTS_PATH + name +"_dynamics" + aid.PIC_FIN)
    plt.close()

