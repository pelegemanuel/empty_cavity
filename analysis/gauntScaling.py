import numpy as np
from sympy.physics.wigner import gaunt
import matplotlib.pyplot as plt
import analysisAid as aid

SAVENAME = aid.ANALYZED_RESULTS_PATH + "gauntScaling" + aid.PIC_FIN
L_LIST_SAVENAME = aid.ANALYZED_RESULTS_PATH + "lList" + aid.TXT_FIN
GAUNT_LIST_SAVENAME = aid.ANALYZED_RESULTS_PATH + "gauntCoefList" + aid.TXT_FIN


def nonZeroGauntVsDiff(l_max, precision):
    l_max = int(l_max)

    l_list = []
    gaunt_coef_list = []

    for l1 in range(1, l_max + 1):
        print(l1)
        for l2 in range(int(l1 / 2) + 1, l1 + 1):
            for m1 in range(-l1, l1 + 1):
                for m2 in range(-l2, l2 + 1):
                    m3 = - m1 - m2
                    min_l3 = max(abs(m3), l1 - l2, 1)
                    min_l3 = min_l3 + ((min_l3 + l1 + l2) % 2)
                    for l3 in range(min_l3, l2 + 1, 2):
                        coef = gaunt(l1, l2, l3, m1, m2, m3, prec = precision)
                        l_list.append([l1, l2, l3])
                        gaunt_coef_list.append(coef)

    np.savetxt(L_LIST_SAVENAME, np.array(l_list))
    np.savetxt(GAUNT_LIST_SAVENAME, np.array(gaunt_coef_list))

def plotGauntCoefs():
    l_mat = np.loadtxt(L_LIST_SAVENAME)
    l_diff = np.abs(l_mat[:, 0] - l_mat[:, 1] - l_mat[:, 2])
    gaunt_coef_list = np.loadtxt(GAUNT_LIST_SAVENAME)

    fig, ax = plt.subplots()
    ax.plot(l_diff, gaunt_coef_list, '.')
    plt.grid()
    plt.xlabel(r'$|l_1 - l_2 - l_3|$')
    plt.ylabel("Gaunt coef")
    plt.savefig(SAVENAME)

nonZeroGauntVsDiff(40, 5)

