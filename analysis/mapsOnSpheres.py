import numpy as np
import coordinatesAid as coord
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm, colors
import analysisAid as aid

ELEVATION_MAP_NAME = aid.ANALYZED_RESULTS_PATH + "elevationMap_"
ELEVATION_GLOBE_NAME = aid.ANALYZED_RESULTS_PATH + "elevationGlobe_"
ELEVATION_HEMISPHERE_NAME = aid.ANALYZED_RESULTS_PATH + "elevationHemispheres_"

NUM_OF_LEVELS = 100
AXIS_DEFAULT_LIMIT = 0.8

def createFlatMap(saveName, thetaCoordinates, phiCoordinates, elevationMat, colorMap):
    figFlat = plt.figure()
    plt.contourf(thetaCoordinates, phiCoordinates, elevationMat, levels = NUM_OF_LEVELS, cmap = colorMap)
    plt.colorbar()
    plt.xlabel(r"$\theta$")
    plt.ylabel(r"$\phi$", rotation = 0)
    plt.savefig(ELEVATION_MAP_NAME + saveName + aid.PIC_FIN)

def setAxisLimits(ax):
    ax.set_xlim(-AXIS_DEFAULT_LIMIT, AXIS_DEFAULT_LIMIT)
    ax.set_ylim(-AXIS_DEFAULT_LIMIT, AXIS_DEFAULT_LIMIT)
    ax.set_zlim(-AXIS_DEFAULT_LIMIT, AXIS_DEFAULT_LIMIT)

def create3DimensionalGlobes(saveName, thetaVec, phiVec, elevationMat, colorMap):
    colorNorm = colors.Normalize()
    fig, (ax1, ax2) = plt.subplots(ncols=2, subplot_kw={'projection': '3d'})
    XX, YY, ZZ = coord.get3Dcoordinates(phiVec = phiVec, thetaVec = thetaVec)
    facecolors = colorMap(colorNorm(elevationMat))
    create3DSingleGlobeOnAx(ax1, XX, YY, ZZ, facecolors)
    create3DSingleGlobeOnAx(ax2, -XX, -YY, -ZZ, facecolors)
    plt.tight_layout()
    plt.savefig(ELEVATION_GLOBE_NAME + saveName + aid.PIC_FIN, bbox_inches = 'tight')

def create3DSingleGlobeOnAx(ax, XX, YY, ZZ, facecolors):
    ax.plot_surface(XX, YY, ZZ, cstride = 1, rstride = 1, facecolors = facecolors, shade = False)
    ax.set_axis_off()
    setAxisLimits(ax)

def create2DimensionalHemispherePlots(saveName, thetaCoordinates, phiCoordinates, elevationMat, colorMap):
    [northHemisphereElevation, southHemisphereElevation] = np.split(elevationMat, 2, axis = 1)
    [northPhi, southPhi] = np.split(phiCoordinates, 2, axis = 1)
    [northTheta, southTheta] = np.split(thetaCoordinates, 2, axis = 1)
    northRadius, southRadius = np.sin(northTheta), np.sin(southTheta)
    
    fig, (axNorth, axSouth) = plt.subplots(ncols=2, subplot_kw={'projection': "polar"})
    axNorth.set_yticklabels([])
    axNorth.set_xticklabels([])
    axSouth.set_yticklabels([])
    axSouth.set_xticklabels([])
    axNorth.contourf(northPhi, northRadius, northHemisphereElevation, levels = NUM_OF_LEVELS, cmap = colorMap)
    axSouth.contourf(southPhi, southRadius, southHemisphereElevation, levels = NUM_OF_LEVELS, cmap = colorMap)
    plt.savefig(ELEVATION_HEMISPHERE_NAME + saveName + aid.PIC_FIN)

def createAllMaps(saveName, thetaVec, phiVec, elevationMat, colorMap):
    thetaCoordinates, phiCoordinates = np.meshgrid(thetaVec, phiVec)

    create2DimensionalHemispherePlots(saveName = saveName,
                                        thetaCoordinates = thetaCoordinates,
                                        phiCoordinates = phiCoordinates,
                                        elevationMat = elevationMat,
                                        colorMap = colorMap)

    createFlatMap(saveName = saveName,
                    thetaCoordinates = thetaCoordinates,
                    phiCoordinates = phiCoordinates,
                    elevationMat = elevationMat,
                    colorMap = colorMap)

    create3DimensionalGlobes(saveName = saveName,
                            thetaVec = thetaVec,
                            phiVec = phiVec,
                            elevationMat = elevationMat,
                            colorMap = colorMap)