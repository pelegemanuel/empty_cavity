import build_aid

def initialDirectoryCreator():
    build_aid.forceCreateDirectory(build_aid.PROJECT_PATH, build_aid.RESULTS_DIRECTORY_NAME)
    build_aid.forceCreateDirectory(build_aid.PROJECT_PATH, build_aid.BUILD_DIRECTORY_NAME)
    build_aid.forceCreateDirectory(build_aid.PROJECT_PATH, build_aid.ANALYZED_RESULTS_DIRECTORY_NAME)

initialDirectoryCreator()