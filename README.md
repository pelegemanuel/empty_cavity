### Empty Cavity: Nonlinear Bubble Dynamics Simulator

This program contains a numerical solution of the second order perturbation equations of motion for a bubble surface between two incompressible fluids, as well as some analysis tools. Equations and additional information appear in "[Turbulence and capillary waves on bubbles](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.104.025108)".

---

### License

Copyright 2021 Peleg Emanuel

Released under the [MIT license](https://opensource.org/licenses/mit-license.php)

---

### Citation Details

If you use this software in your research, please cite one of the following works, for which this code was developed.

1. [Turbulence and capillary waves on bubbles](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.104.025108)

---

### Requirements

1. Armadillo (fast C++ library for linear algebra & scientific computing)
    + [Git Repository](https://gitlab.com/conradsnicta/armadillo-code)
    + [Site](http://arma.sourceforge.net/)
2. Wigner-cpp (C++ templated library for Wigner 3*j*, 6*j*, 9*j* symbols and Gaunt coefficients)
    + [Git Repository](https://github.com/hydeik/wigner-cpp)
3. SuperLU [OPTIONAL] (Sparse Linear Algebra Library)
    + [Git Repository](https://github.com/xiaoyeli/superlu)
    + Note: Armadillo should be compiled with this library


